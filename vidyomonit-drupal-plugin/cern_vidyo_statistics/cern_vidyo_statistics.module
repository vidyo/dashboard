<?php
/**
    This file is part of the CERN Dashboards and Monitoring for Vidyo
    Copyright (C) 2014 European Organization for Nuclear Research (CERN)
    CERN Dashboards and Monitoring for Vidyo is free software: you can redistribute it and/or
    modify it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.
    CERN Dashboards and Monitoring for Vidyo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU Affero General Public License
    along with the CERN Dashboards and Monitoring for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
 */


require_once 'cvs_background_worker.php';
require_once 'cvs_data_display.php';


/*******************************************************************************************************
 * MODULE Permissions for the module and for each page
 *******************************************************************************************************/

/**
 * Implements hook_permission().
 */
function cern_vidyo_statistics_permission(){
    return array(
        'access cern_vidyo_statistics content' => array(
            'title' => t('View the dashboard'),
            'description' => t('Select who can view the dashboard page'),
        ),
        'access cern_vidyo_statistics monitoring content' => array(
            'title' => t('View the monitoring dashboard'),
            'description' => t('Select who can view the monitoring dashboard page'),
        ),
        'administer cern_vidyo_statistics configuration' => array(
            'title' => t('Administer the configuration of the dashboard'),
            'description' => t('Perform changes to the configuration of the module, connections to the db and API'),
        )
    );
}

/*******************************************************************************************************
 * MODULE Creation of the node to display the dashboard
 *******************************************************************************************************/

function _cvs_display_statisitics() {
    $vs = new CVS_background_worker();

    // add js for this page
    // add vars to the settings in javascript
    $js_variables = array();
    $has_monitoring_access = user_access('access cern_vidyo_statistics monitoring content');
    $js_variables['has_monitoring_access'] = $has_monitoring_access;
    if ($has_monitoring_access) {
        $js_variables['observium_url'] = variable_get('cvs_observium_url');
        $js_variables['observium_username'] = variable_get('cvs_observium_username');
        $js_variables['observium_password'] = variable_get('cvs_observium_password');
    }
    $settings = array('cern_vidyo_statistics_config' => $js_variables);

    drupal_add_js($settings, 'setting');
    drupal_add_library('system', 'ui.tabs');
    drupal_add_library('system', 'ui.button');
    drupal_add_library('system', 'ui.dialog');
    drupal_add_js('https://www.google.com/jsapi', 'external');
    drupal_add_js('https://oss.maxcdn.com/respond/1.4.2/respond.min.js', 'external');
    drupal_add_js('https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js', 'external');
//    drupal_add_js(drupal_get_path('module', 'cern_vidyo_statistics') . '/js/vidyo-dashboard-bundle.min.js');
    drupal_add_js(drupal_get_path('module', 'cern_vidyo_statistics') . '/js/cvs_helpers.js');
    drupal_add_js(drupal_get_path('module', 'cern_vidyo_statistics') . '/js/cvs_charts_data_builder.js');
    drupal_add_js(drupal_get_path('module', 'cern_vidyo_statistics') . '/js/cvs_charts.js');
    drupal_add_js(drupal_get_path('module', 'cern_vidyo_statistics') . '/js/piwik_dash.js');


    // add css for this page
    drupal_add_css(drupal_get_path('module', 'cern_vidyo_statistics') . '/style/cvs_style.css', array('group' => CSS_DEFAULT));
    drupal_add_css(drupal_get_path('module', 'cern_vidyo_statistics') . '/style/vidyo-dashboard-style.css', array('group' => CSS_THEME, 'weight' => 999));

    $pathToVidyoDashboardBundle = drupal_get_path('module', 'cern_vidyo_statistics') . '/js/vidyo-dashboard-bundle.min.js';
    $pathToJQuery = drupal_get_path('module', 'cern_vidyo_statistics') . '/js/jquery.min.js';
    $cvsIndex = file_get_contents(drupal_get_path('module', 'cern_vidyo_statistics') . '/cvs_index.html');
    $markup = str_replace('#vidyoDashboardBundlePath#', $pathToVidyoDashboardBundle, $cvsIndex);
    $markup = str_replace('#jQueryPath#', $pathToJQuery, $markup);
    $page_array['cern_vidyo_statistics_arguments'] = array(
        '#title' => t('Dashboard'),
        '#markup' => $markup,
    );


    return $page_array;
}

function cvs_auth_monitoring(){
    $vs = new CVS_background_worker();
    $url = $vs->get_cvs_monitoring_proxy_server() .'login/';
    // Create map with request parameters
    $params = array('username'=>variable_get('cvs_monitoring_proxy_username'),
        'password'=>variable_get('cvs_monitoring_proxy_password')
    );

    // Build Http query using params
    $query = http_build_query ($params);

    // Create Http context details
    $contextData = array(
        'method' => 'POST',
        'header' => "Connection: close\r\n".
            "Content-type: application/x-www-form-urlencoded\r\n".
            "Content-Length: ".strlen($query)."\r\n",
        'content'=> $query );

    // Create context resource for our request
    $context = stream_context_create (array ( 'http' => $contextData ));

    // Read page rendered as result of your POST request
    $result = @file_get_contents($url, false, $context);

    if ($result === FALSE) {
        drupal_set_message(t('The monitoring system is offline.'), 'error');
        $result = '';
    }

    $_SESSION['auth_key'] = $result;
    return $result;
}

function _cvs_cross_domain_proxy(){
    $regexp = '/^[a-zA-Z0-9._\/-]*$/';
    preg_match($regexp, $_POST['url'], $match);
    if (count($match) < 1) {
        watchdog('CERN Vidyo statistics', 'XDomain request denied to url: ' . $_POST['url']);
        return;
    }

    $vs = new CVS_background_worker();
    $url = $vs->get_cvs_monitoring_proxy_server() . $_POST['url'];

    $params = $_POST;
    $query = http_build_query($params);

    $contextData = array(
        'method' => 'POST',
        'header' => "Connection: close\r\n".
            "Content-type: application/x-www-form-urlencoded\r\n".
            "Content-Length: ".strlen($query)."\r\n",
        'content'=> $query
    );

    $context = stream_context_create(array ( 'http' => $contextData ));
    $result = file_get_contents($url, false, $context);

    drupal_json_output(json_decode($result));
}

function _cvs_vidyomonit_proxy(){
    $url = _get_vidyomonit_api_url() . $_GET['path'];
    $params = drupal_http_build_query(drupal_get_query_parameters($_GET, array('path', 'q')));
    $username = variable_get('cvs_vidyomonit_api_username', '');
    $password = variable_get('cvs_vidyomonit_api_password', '');
    $authHeader = 'Basic ' . base64_encode($username . ':' . $password);
    $options = array('headers' => array('Authorization' => $authHeader));

    $request = drupal_http_request($url . '?' . $params, $options);

    if ($request->code != 200) {
        drupal_add_http_header('Status', $request->code.' '.$request->error);
        drupal_json_output(array('status'=>$request->code, 'msg'=>$request->data));
    } else {
        drupal_json_output(json_decode($request->data));
    }
}

function _get_vidyomonit_api_url() {
    $url = variable_get('cvs_vidyomonit_api_url', 'https://vidyomon.cern.ch/api/');
    $regexp = '/^(?:https?:\/+)?(.*)(?:\/)?$/';
    preg_match($regexp, $url, $match);

    if (count($match) == 2) {
        return 'https://' . $match[1] . '/';
    }

    return $url;
}


function _cvs_ajax() {
    $graphic = $_GET['graphic'];
    $display = new CSV_data_display();
    switch ($graphic) {
        case 'pwiki' :
            if(isset($_GET['method']) && $_GET['period'])
                $data = $display->cvs_get_pwiki($_GET['period'],$_GET['method']);
            else
                $data = $display->cvs_get_pwiki($_GET['period'], null);
            break;
        default:
            break;
    }
    // Return json
    drupal_json_output($data);
}



/*******************************************************************************************************
 * MODULE CONFIGURATION
 *******************************************************************************************************/

/**
 * Implements hook_menu().
 */
function cern_vidyo_statistics_menu() {
    $items = array();
    // add a new group in the admin/config panel of Drupal
    $items ['admin/config/cern'] = array(
        'title' => 'CERN',
        'description' => 'Configuration for CERN custom modules',
        'position' => 'right',
        'page callback' => 'system_admin_menu_block_page',
        'file' => 'system.admin.inc',
        'file path' => drupal_get_path( 'module', 'system' ),
        'access arguments' => array('administer site configuration'),
    );

    // add the vidyo statistics configuration menu
    $items['admin/config/cern/vidyo_statistics'] = array(
        'title' => 'Vidyo Statistics & Monitoring',
        'description' => 'Configuration for Vidyo Statistics module',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('cern_vidyo_statistics_form'),
        'access arguments' => array('administer cern_vidyo_statistics configuration'),
    );

    // links to the dashboard pages
    $items['Vidyo'] = array(
        'title' => 'Vidyo',
        'page callback' => '_cvs_display_statisitics',
        'access arguments' => array('access cern_vidyo_statistics content'),
        'type' => MENU_NORMAL_ITEM, //Will appear in Navigation menu.
        'menu_name' => 'main-menu', // show in the main menu
    );

    // ajax callback from client
    $items['Vidyo/ajax'] = array(
        'title' => t('Vidyo AJAX'),
        'type' => MENU_CALLBACK,
        'page callback' => '_cvs_ajax',
        'access arguments' => array('access cern_vidyo_statistics content'),
    );

    // Proxy requests between JS client and vidyomonit to allow access outside CERN network
    $items['Vidyo/monitproxy'] = array(
        'title' => t('Vidyomonit API proxy'),
        'type' => MENU_CALLBACK,
        'page callback' => '_cvs_vidyomonit_proxy',
        'access arguments' => array('access cern_vidyo_statistics content'),
    );

    return $items;
}

/**
 * Implements hook_help.
 *
 * Displays help and module information.
 *
 * @param path
 *   Which path of the site we're using to display help
 * @param arg
 *   Array that holds the current path as returned from arg() function
 */

function cern_vidyo_statistics_help($path, $arg) {
    switch ($path) {
        case "admin/help#cern_vidyo_statistics":
            return '<p>'. t("Displays charts to view statistics data from Vidyo") .'</p>';
            break;
    }
}

/**
 * Form function, called by drupal_get_form()
 * in cern_vidyo_statistics_menu().
 */
function cern_vidyo_statistics_form($form, &$form_state) {
    $form['cvs_vidyomonit_api_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Url to Vidyomonit API'),
        '#size' => 50,
        '#maxlength' => 255,
        '#description' => t('Url to Vidyomonit API.'),
        '#required' => TRUE,
    );
    $form['cvs_vidyomonit_api_username'] = array(
        '#type' => 'textfield',
        '#title' => t('Username to Vidyomonit API'),
        '#size' => 50,
        '#maxlength' => 255,
        '#description' => t('Username to Vidyomonit API.'),
        '#required' => TRUE,
    );
    $form['cvs_vidyomonit_api_password'] = array(
        '#type' => 'password',
        '#title' => t('Password to Vidyomonit API'),
        '#size' => 50,
        '#maxlength' => 255,
        '#description' => t('Password to Vidyomonit API.'),
        '#required' => TRUE,
    );
    $form['cvs_piwik_apikey'] = array(
        '#type' => 'password',
        '#title' => t('Piwik API key'),
        '#size' => 50,
        '#description' => t('Piwik API key.'),
        '#required' => TRUE
    );

    $form['cvs_observium_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Observium URL'),
        '#size' => 50,
        '#maxlength' => 255,
        '#description' => t('Observium URL (e.g. https://myobservium.cern.ch)'),
        '#required' => TRUE,
    );
    $form['cvs_observium_username'] = array(
        '#type' => 'textfield',
        '#title' => t('Username to Observium'),
        '#size' => 50,
        '#maxlength' => 255,
        '#description' => t('Username to Observium.'),
        '#required' => TRUE,
    );
    $form['cvs_observium_password'] = array(
        '#type' => 'password',
        '#title' => t('Password to Observium'),
        '#size' => 50,
        '#maxlength' => 255,
        '#description' => t('Password to Observium.'),
        '#required' => TRUE,
    );

    return system_settings_form($form);
}
