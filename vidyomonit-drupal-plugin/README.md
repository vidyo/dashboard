    This file is part of the CERN Dashboards for Vidyo
    Copyright (C) 2014 European Organization for Nuclear Research (CERN)
    
    CERN Dashboards for Vidyo is free software: you can redistribute it and/or
    modify it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.
    
    CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU Affero General Public License
    along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.

# CERN Vidyo Dashboards Drupal plugin

A Drupal pluign for displaying Vidyo Dashboards

## Installation

### Important

Installation process may differ depending on your Drupal version. In case of problems follow instructions according to your version (https://www.drupal.org/documentation/install/modules-themes).

### Installation steps
Copy the three directories (**cern_vidyo_statistics**, **elysia_cron**, **user_permissions**) to your Drupal installation (normally into **sites/all/modules**).

Go to Modules administration page (**Administration -> Modules**)
 
Find the three following modules and enable them:
- **CERN Vidyo Statistics (cern_vidyo_statistics)**
- **Elysia Cron (elysia_cron)**
- **User Permissions (user_permissions)**
(remeber to click 'Save configuration' afterwards)

Next go to CERN Vidyo Statistics configuration page (**Administration -> Configuration -> Vidyo Statistics & Monitoring**) and fill all the fields.

- **Url to Vidyomonit API ** i.e. http://mysitevidyomon.com/api/
- **Username to Vidyomonit API / Password to Vidyomonit API **
- **Piwik API key** 
- **Observium URL/Username to Observium/Password to Observium**

If you are not using Autorization/Piwiki/Observium just pass any fake information

To access Vidyo Dashboards simply create a Basic page. Vidyo tab should become visible.

### Important

Make sure you have the latest **vidyo-dashboards-bundle.min.js** file (build for Drupal!) on the server along with **jquery.min.js** and **query.min.map**.
Instructions how co create Drupal vidyo-dashboards-bundle are available in **vidyomonit-dashboard-frontend/README.md** file.
    
After uploading new js or css files remember to clear Drupal's Cache (**Administration -> Configuration -> Performance -> Clear All Cache**).
    
## Known Issues

We strongly recommend disabling cache for anonymous users (**Administration -> Configuration -> Performance ->  Cache pages for anonymous users**) due to the problem with caching requests from vidyomonit.

If there are problems with javascript or styles try disabling aggregation of JS/CSS Files (**Administration -> Configuration -> Performance ->  Aggregate and compress CSS files./Aggregate JavaScript files.**)
    