    This file is part of the CERN Dashboards for Vidyo
    Copyright (C) 2014 European Organization for Nuclear Research (CERN)
    
    CERN Dashboards for Vidyo is free software: you can redistribute it and/or
    modify it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.
    
    CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU Affero General Public License
    along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.

# CERN Vidyo Dashboards
    
    CERN Vidyo Dashboards
    
    This software provides a set of charts and monitoring graphs to use against a deployed Vidyo videoconferencing infrastructure. The software is currently in production at:
    
    http://avc-dashboard.web.cern.ch/Vidyo
    
    (c) 2014 CERN - European Organization for the Nuclear Research
    
    under the GNU AFFERO GENERAL PUBLIC LICENSE
    
    vidyo-team@cern.ch, http://cern.ch/Vidyo
    
    If the terms of the license collide with your intentions to use the software please contact us for the possibility of having a dual license.

This repository contains all the source code used for vidyo monitoring system.
It consists of four applications:
- vidyomonit-backend: app responsible for reading data from DB and serving API
- vidyomonit-backend-admin-panel-frontend: source code for backend admin panel
- vidyomonit-dashboard-frontend: source code for vidyo dashboard
- vidyomonit-wordpress-plugin: wordpress module containing everything to use vidyo dashboard 

TODO:
- create grunt tasks for easy development: start server and dashboard
- copy js bundles to proper folder
- make sure all .gitignore are working properly

Any feedback is welcome. Please send it to vidyo-team@cern.ch