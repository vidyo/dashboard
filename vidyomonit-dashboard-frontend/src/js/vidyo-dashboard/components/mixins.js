/*!
 * This file is part of the CERN Dashboards for Vidyo
 * Copyright (C) 2014 European Organization for Nuclear Research (CERN)
 *
 * CERN Dashboards for Vidyo is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var $ = require('jquery');
var _ = require('underscore');


var SetInterval = {
    componentWillMount: function() {
        this.intervals = [];
    },
    setInterval: function() {
        this.intervals.push(setInterval.apply(null, arguments));
    },
    componentWillUnmount: function() {
        this.intervals.map(clearInterval);
    }
};

var Resize = {
    getInitialState: function() {
        return {
            width: $(window).width(),
            height: $(window).height(),
        };
    },
    updateDimensions: function() {
        this.setState({width: $(window).width(), height: $(window).height()});
    },

    // Throttling makes sure that the update only happens once per second
    throttledUpdateDimensions: _.throttle(function() {
        this.setState({width: $(window).width(), height: $(window).height()});
    }, 500, {leading: false}),

    componentWillMount: function() {
        this.updateDimensions();
    },
    componentDidMount: function() {
        $(window).on('resize', this.throttledUpdateDimensions);
    },
    componentWillUnmount: function() {
        $(window).off('resize', this.throttledUpdateDimensions);
    }
};

var getModesMixin = function(modes, initialSelection) {
    return {
        getInitialState: function() {
            return {
                modes: modes.map(function(d) {
                    return {name: d, selected: d === initialSelection};
                })
            };
        },

        handleModeChange: function(newMode) {
            this.setState({modes: this.state.modes.map(function(d) {
                d.selected = d.name === newMode;
                return d;
            })});
        },

        getSelectedMode: function() {
            return _.find(this.state.modes, function(d) { return d.selected; }).name;
        }
    };
};

var getViewModesMixin = function(newViewMode, initialSelection) {
    return {
        getInitialState: function() {
            return {
                viewModes: newViewMode.map(function(d) {
                    return {name: d, selected: d === initialSelection};
                })
            };
        },

        handleViewModeChange: function(newMode) {
            this.setState({viewModes: this.state.viewModes.map(function(d) {
                d.selected = d.name === newMode;
                return d;
            })});
        },

        getSelectedViewMode: function() {
            return _.find(this.state.viewModes, function(d) { return d.selected; }).name;
        }
    };
};

module.exports = {
    SetInterval: SetInterval,
    Resize: Resize,
    getModesMixin: getModesMixin,
    getViewModesMixin: getViewModesMixin
};
