/*!
 * This file is part of the CERN Dashboards for Vidyo
 * Copyright (C) 2014 European Organization for Nuclear Research (CERN)
 *
 * CERN Dashboards for Vidyo is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';
/* global google */

var React = require('react');
var _ = require('underscore');

var colors = require('../colors');

var graphHeight = 300;


var GoogleChart = React.createClass({
    propTypes: {
        dataTable: React.PropTypes.object.isRequired,
        options: React.PropTypes.object.isRequired,
        chartType: function(props, propName) {
            if (!props[propName] || !(props[propName] in google.visualization)) {
                return new Error('Invalid chart type: ' + props[propName]);
            }
        },
        controlType: React.PropTypes.string,
        filterColumnIndex: React.PropTypes.number,
    },

    getInitialState: function() {
        return {
            target: null,
            control: null,
            chartId: _.uniqueId(),
            filterId: _.uniqueId(),
        };
    },

    getChartOptions: function() {
        var defaultOpts = {
            backgroundColor: {fill: 'transparent'},
            explorer: null,
            focusTarget: 'category',
            colors: colors.chartColors,
            chartArea: {width: '100%', height: '80%'},
            legend: {position: 'top'},
            height: graphHeight,
            isStacked: true,
        };

        for (var key in this.props.options) {
            defaultOpts[key] = this.props.options[key];
        }

        return defaultOpts;
    },

    updateChart: function() {
        var chart;
        if (this.props.controlType) {
            var dashboard = this.state.target;
            var control = this.state.control;

            if (!dashboard) {
                var container = this.refs.dashboard.getDOMNode();
                var chartId = this.refs.chart.getDOMNode().id;
                var filterId = this.refs.filter.getDOMNode().id;

                dashboard = new google.visualization.Dashboard(container);

                chart = new google.visualization.ChartWrapper({
                    chartType: this.props.chartType,
                    containerId: chartId,
                    options: this.getChartOptions(),
                });

                control = new google.visualization.ControlWrapper({
                    controlType: this.props.controlType,
                    containerId: filterId,
                    options: {
                        filterColumnIndex: this.props.filterColumnIndex || 0,
                    }
                });

                dashboard.bind(control, chart);

                this.setState({target: dashboard, control: control});
            }


            dashboard.draw(this.props.dataTable);

            if (control) {
                var controlState = control.getState();
                control.setState({
                    lowValue: controlState.lowValue,
                    highValue: controlState.highValue,
                });
            }
        } else {
            chart = this.state.target;

            if (!chart) {
                chart = new google.visualization[this.props.chartType](this.getDOMNode());
                this.setState({target: chart});
            }

            chart.draw(this.props.dataTable, this.getChartOptions());
        }
    },

    componentDidMount: function() {
        this.updateChart();
    },

    componentDidUpdate: function() {
        this.updateChart();
    },

    render: function() {
        return (
            <div ref='dashboard'>
                <div ref='chart' id={this.state.chartId} />
                <div ref='filter' id={this.state.filterId} />
            </div>
        );
    }
});


module.exports = {
    GoogleChart: GoogleChart
};
