/*!
 * This file is part of the CERN Dashboards for Vidyo
 * Copyright (C) 2014 European Organization for Nuclear Research (CERN)
 *
 * CERN Dashboards for Vidyo is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var $ = require('jquery');
var _ = require('underscore');


var today = new Date();
var fromDate = new Date(today.getFullYear() - 2, today.getMonth(), 1);
var fromParam = fromDate.toISOString();

/**
 * Monitoring API client.
 */
var getClient = function(apiLocation, tenantName) {
    var _getAPIURL = function(path, data) {
        if (_.isString(apiLocation)) {
            return apiLocation + path + (_.isEmpty(data) ? '' : '?' + $.param(data));
        } else if (_.isFunction(apiLocation)) {
            return apiLocation(path, data);
        }
    };

    var _ajax = function(path, data, excludeTenantParam) {
        data = data || {};
        if (!excludeTenantParam) data = $.extend(data, {tenant: tenantName});

        return $.ajax({
            url: _getAPIURL(path, data),
            crossDomain: true,
            tryCount: 0,
            retryLimit: 3,
            dataType: 'json',
            error: function(xhr, textStatus) {
                console.log('_ajax error:' + textStatus, xhr);
                $('.main-alert-box').css('display', 'block');
                if (textStatus === 'timeout') {
                    this.tryCount++;
                    if (this.tryCount <= this.retryLimit) {
                        console.log('retrying...');
                        $.ajax(this);
                        return;
                    }
                    return;
                }
            },
            success: function() {
                $('.main-alert-box').css('display', 'none');
            }
        });
    };

    return {
        snapshot: function(params) {
            return _ajax('connections/snapshot', params, true);
        },

        dailyConnections: function() {
            return _ajax('connections/daily', {from: fromParam});
        },

        dailyMeetings: function() {
            return _ajax('meetings/daily', {from: fromParam});
        },

        dailyRegistrations: function() {
            return _ajax('registrations/daily', {from: fromParam});
        },

        activeConnections: function() {
            return _ajax('connections/active');
        },

        installs: function() {
            return _ajax('installs/daily', {from: fromParam});
        },

        tenants: function() {
            return _ajax('tenants', {}, true);
        },

        monthlyUniqueUsers: function() {
            return _ajax('users/monthly');
        },

        maxSimultaneousConnections: function() {
            return _ajax('connections/maxSimConnections');
        },

        allActiveConnections: function() {
            return _ajax('connections/active', {}, true);
        },

        gateways: function() {
            return _ajax('gateways', {}, true);
        },
    };
};

module.exports = {
    getClient: getClient
};
