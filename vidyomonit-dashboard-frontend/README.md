    This file is part of the CERN Dashboards for Vidyo
    Copyright (C) 2014 European Organization for Nuclear Research (CERN)
    
    CERN Dashboards for Vidyo is free software: you can redistribute it and/or
    modify it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.
    
    CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU Affero General Public License
    along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.

# Vidyo dashboard

A Javascript front-end for the Vidyomonit API.

## Deploy

You can either build the Javascript bundle yourself (see below), or just use the readily built vidyo-dashboard-bundle.min.js. Also you'll need the vidyo-dashboard-style.css file, which is basically a stripped down version of Bootstrap, encapsulated under a namespace to prevent it from messing with the outer scope. Both files are can be found in the src/ folder.

Make sure you fetch the charting libraries before instantiating the dashboard:


    var vidyoDashboard;

    google.load('visualization', '1.0', {'packages': ['corechart', 'controls']});
    google.setOnLoadCallback(function() {

        // Config options are explained in src/js/vidyo-dashboard/vidyo-dashboard.js
        var config = {
            targetId: 'vidyo-dashboard-container',
            url: 'https://vidyomonit-backend.com/api/',
            defaultTenant: 'CERN',
        };

        vidyoDashboard = new VidyoDashboard(config);
    });

    // vidyoDashboard.unmount(); <-- can be used to delete dashboard from DOM

For a more complete deployment example, see src/index.html.


## Develop

1. Install NPM
2. Navigate to the folder with package.json file
3. Create a config.js file using the provided config.js.example (for details about parameters see src/js/vidyo-dashboard/vidyo-dashboard.js
4. Run
> npm install

Now you can run the following build scripts:

> npm run watch

to watch for changes in files and continuously build the bundle, and

> npm run build

to build a minified bundle ready for deployment as a standalone application.

> npm run build-drupal

to build a minified bundle ready for deployment in Drupal enviroment. It does not contain jQuery plugin, which has to 
be added manually. Using bundle with jQuery included will cause Drupal having conflicts with it's core jQuery plugin.

Aleksi Pekkala (aleksi.v.a.pekkala@student.jyu.fi)
4.12.2014
