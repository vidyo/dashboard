"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

import argparse
import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from vidyomonit.app.app_factory import create_app
from vidyomonit import conf
from vidyomonit.app.tools import rdb_manager

app = create_app(conf)


def init_db(silent, application):
    rdb_manager.db_setup(application.config, silent)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run the vidyomonit backend server')
    parser.add_argument('-s', '--setup', dest='run_setup', action='store_true', help='Sets up database and tables')
    parser.add_argument('-r', '--restore', dest='restore', const=30, type=int, metavar='nb_of_days', nargs='?',
                        help='Restores missing data within given number of days (default: 30)')

    args = parser.parse_args()

    if args.run_setup:
        init_db(silent=False, application=app)
    elif args.restore:
        from app.tasks import daily_connections, monthly_unique_users, daily_meetings
        import datetime
        import pytz

        dt_to = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
        dt_from = dt_to - datetime.timedelta(days=args.restore)

        daily_connections.restore_data(dt_from=dt_from, dt_to=dt_to)
        monthly_unique_users.restore_data(dt_from=dt_from, dt_to=dt_to)
        daily_meetings.restore_data(dt_from=dt_from, dt_to=dt_to)
    else:
        app.run(debug=True, port=1234, host='0.0.0.0')
