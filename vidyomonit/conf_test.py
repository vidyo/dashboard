import os


TESTING = True
DEBUG = True

# Connection to RethinkDB
RDB_CONF = {
    'host': 'localhost',
    'port': 28015,
    'db': 'vidomonittesst'
}

# Connection to Vidyo Mysql database with CDRAccess
VIDYO_DBCDR = {
    'host': 'dbod-vidyotst.cern.ch',
    'port': 5503,
    'user': 'admin',  # Make sure user has sufficient read rights
    'passwd': 'testtest',
    'db': 'vidyomonit_test'
}

CURRENT_PATH = os.path.dirname(os.path.realpath(__file__))
TEST_SQL_FILENAME = 'vidyomonit_tests.sql'
TEST_SQL_FILE_PATH = os.path.join(CURRENT_PATH, TEST_SQL_FILENAME)


def PROCESS_CONFERENCE_NAME(conference_name):
    return conference_name