    This file is part of the CERN Dashboards for Vidyo
    Copyright (C) 2014 European Organization for Nuclear Research (CERN)
    
    CERN Dashboards for Vidyo is free software: you can redistribute it and/or
    modify it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.
    
    CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU Affero General Public License
    along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.

# Vidyomonit

An app for gathering monitoring data from the Vidyo Call Detail Records database. The app consists of three parts:
1. The monitoring daemon, that periodically fetches data from the CDR, aggregates it and stores it locally. The daemon runs tasks found in the tasks/ subfolder, by intervals defined in daemon.py.
2. A light API for vidyomon dashboard (to read information) and vidyomon dashboard admin (to manage devices)
3. View that uses bundle from vidyomonit-backend-admin-panel-frontend application.

## Install
Disclaimer: To keep your system clean and avoid problems with package dependencies we strongly encourage to use Virtualenv (http://docs.python-guide.org/en/latest/dev/virtualenvs/)

1. Setup RethinkDB
2. Install pip requirements:
> pip install -r requirements.txt
3. Create a conf.py file using the provided conf.py.example
4. Make sure that path where logs will be store is created (look at the conf.py LOG_DIR variable)
5. Install bower components
> bower install
6. Initialize the database:
> python application.py --setup
7. Start the daemon:
> python daemon.py
8. Setup your webserver of choice; repo contains a sample .wsgi file for mod_wsgi. For development you can use the Flask dev server:
> python application.py

Some tasks (e.g. daily_connections.py) take a date parameter, so you can use them to populate the database with historical data:

    # Fetch daily connection data for November 2014:
    from vidyomonit.tasks import daily_connections
    from vidyomonit.utils import utc

    dt = utc(2014, 11, 1)
    while dt < utc(2014,12,1):
        print daily_connections.run_task(dt)


## Tesing
To run test with coverage run:
>  nosetests --with-coverage --cover-html --cover-tests --cover-html-dir=coverage


### TODO

- Authentication, restricted access to different tenants

Aleksi Pekkala (aleksi.v.a.pekkala@student.jyu.fi)
4.12.2014

Przemyslaw Kwiatkowski
22.04.2015
