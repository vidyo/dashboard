"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

import time
import logging
from apscheduler.scheduler import Scheduler

from vidyomonit.app.tools import log_manager
from vidyomonit.app.tasks import active_connections
from vidyomonit.app.tasks import snapshot
from vidyomonit.app.tasks import daily_connections
from vidyomonit.app.tasks import daily_meetings
from vidyomonit.app.tasks import daily_installs
from vidyomonit.app.tasks import daily_registrations
from vidyomonit.app.tasks import monthly_unique_users
from vidyomonit.app.tasks import cleanup
from vidyomonit.app.tasks import tenants


log_manager.setup_log_tasks()
logger = logging.getLogger('app.tasks')


def start_scheduler():
    logger.info('Starting scheduler')
    scheduler = Scheduler()
    scheduler.start()

    @scheduler.cron_schedule(second='*/10')
    def ten_second_tasks():
        active_connections.run_task()

    @scheduler.cron_schedule(minute='*/1')
    def minute_tasks():
        snapshot.run_task()

    @scheduler.cron_schedule(minute='*/10')
    def ten_minute_tasks():
        daily_connections.run_task()
        daily_meetings.run_task()

    @scheduler.cron_schedule(hour='*')
    def hourly_tasks():
        daily_installs.run_task()
        daily_registrations.run_task()

    @scheduler.cron_schedule(day='*')
    def daily_tasks():
        monthly_unique_users.run_task()
        cleanup.run_task()
        tenants.run_task()


def main():
    start_scheduler()
    try:
        while True:
            time.sleep(60)
            logger.info('Daemon heartbeat')
    except KeyboardInterrupt:
        logger.info('Daemon stopped')


if __name__ == '__main__':
    main()
