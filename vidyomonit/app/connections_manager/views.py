"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from flask import Blueprint, g, render_template, abort, redirect, url_for, request, jsonify, flash

from vidyomonit.app.connections_manager.forms import process_device_family_add_form, process_device_family_delete_form, \
    process_device_family_edit, process_device_add, process_device_edit, process_device_delete
from vidyomonit.app.connections_manager.gateways.forms import process_gateway_add, process_gateway_delete, \
    process_gateway_edit
import rethinkdb as r

module = Blueprint('connections_manager', __name__)


@module.route('/operating-systems', methods=['GET', 'POST'])
def operating_systems():
    device_type = 'app_os'
    device_print_name = 'Operating System'

    device_families_forms = get_device_families_forms(device_type, device_print_name)
    device_forms = get_device_forms(device_type, device_print_name)

    if request.method == 'POST':
        redirect(url_for('connections_manager.operating_systems'))

    device_families = list(r.table('connections_groups').filter({'type': device_type}).order_by('name').run(g.rdb_conn))
    device_families_keys = [os_f['id'] for os_f in device_families]
    devices = list(r.table('connections_groups_devices').filter(
        lambda device:
        device['connections_group'] in device_families_keys and device['type'] == device_type
    ).order_by('name').merge(lambda device: {
        'device_family': r.table('connections_groups').get(device['connections_group'])
    }).run(g.rdb_conn))

    grouped_devices_names = get_grouped_devices_names_from_devices_list(devices)
    ungrouped_devices = get_ungrouped_devices(device_type, grouped_devices_names)

    return render_template('connections_manager/operating_systems.html', device_families_forms=device_families_forms,
                           device_forms=device_forms, devices=devices, device_families=device_families,
                           device_print_name=device_print_name, ungrouped_devices=ungrouped_devices)


@module.route('/application-versions', methods=['GET', 'POST'])
def application_versions():
    device_type = 'app_version'
    device_print_name = 'Application Version'

    device_families_forms = get_device_families_forms(device_type, device_print_name)
    device_forms = get_device_forms(device_type, device_print_name)

    if request.method == 'POST':
        redirect(url_for('.application_versions'))

    device_families = list(r.table('connections_groups').filter({'type': device_type}).order_by('name').run(g.rdb_conn))
    device_families_keys = [os_f['id'] for os_f in device_families]
    devices = list(r.table('connections_groups_devices').filter(
        lambda device:
        device['connections_group'] in device_families_keys and device['type'] == device_type
    ).order_by('name').merge(lambda device: {
        'device_family': r.table('connections_groups').get(device['connections_group'])
    }).run(g.rdb_conn))

    grouped_devices_names = get_grouped_devices_names_from_devices_list(devices)
    ungrouped_devices = get_ungrouped_devices(device_type, grouped_devices_names)

    return render_template('connections_manager/application_versions.html', device_families_forms=device_families_forms,
                           device_forms=device_forms, devices=devices, device_families=device_families,
                           device_print_name=device_print_name, ungrouped_devices=ungrouped_devices)


@module.route('/endpoint-types', methods=['GET', 'POST'])
def endpoint_types():
    device_type = 'endpoint_type'
    device_print_name = 'Endpoint Type'

    device_families_forms = get_device_families_forms(device_type, device_print_name)
    device_forms = get_device_forms(device_type, device_print_name)

    if request.method == 'POST':
        redirect(url_for('.endpoint_types'))

    device_families = list(r.table('connections_groups').filter({'type': device_type}).order_by('name').run(g.rdb_conn))
    device_families_keys = [os_f['id'] for os_f in device_families]
    devices = list(r.table('connections_groups_devices').filter(
        lambda device:
        device['connections_group'] in device_families_keys and device['type'] == device_type
    ).order_by('name').merge(lambda device: {
        'device_family': r.table('connections_groups').get(device['connections_group'])
    }).run(g.rdb_conn))

    grouped_devices_names = get_grouped_devices_names_from_devices_list(devices)
    ungrouped_devices = get_ungrouped_devices(device_type, grouped_devices_names)

    return render_template('connections_manager/endpoint_types.html', device_families_forms=device_families_forms,
                           device_forms=device_forms, devices=devices, device_families=device_families,
                           device_print_name=device_print_name, ungrouped_devices=ungrouped_devices)


@module.route('/gateway-names', methods=['GET', 'POST'])
def gateway_names():
    gateway_print_name = 'Gateway'
    gateway_forms = get_gateway_forms(gateway_print_name)

    if request.method == 'POST':
        redirect(url_for('.gateway_names'))

    gateways = list(r.table('gateways').order_by('name').run(g.rdb_conn))

    return render_template('connections_manager/gateways.html',
                           gateway_forms=gateway_forms,
                           gateways=gateways,
                           gateway_print_name=gateway_print_name
                           )


def get_device_families_forms(device_type='', device_print_name=''):
    return {
        'add': process_device_family_add_form(device_type=device_type,
                                              device_print_name=device_print_name),
        'delete': process_device_family_delete_form(device_print_name=device_print_name),
        'edit': process_device_family_edit(device_print_name=device_print_name)
    }


def get_device_forms(device_type='', device_print_name=''):
    return {
        'add': process_device_add(device_type=device_type, device_print_name=device_print_name),
        'edit': process_device_edit(device_print_name=device_print_name),
        'delete': process_device_delete(device_print_name=device_print_name)
    }


def get_gateway_forms(gateway_print_name=''):
    return {
        'add': process_gateway_add(gateway_print_name=gateway_print_name),
        'edit': process_gateway_edit(gateway_print_name=gateway_print_name),
        'delete': process_gateway_delete(gateway_print_name=gateway_print_name)
    }


def get_ungrouped_devices(device_type='', grouped_devices=[]):
    selection = r.table('connections_per_day').order_by('id').run(g.rdb_conn)
    devices = []
    for s in selection:
        for t in s['tenants']:
            connections = s['tenants'][t]['connections']
            devices_in_type = connections[device_type]
            if devices_in_type:
                for device_name in connections[device_type].keys():
                    if device_name not in devices:
                        devices.append(device_name)

    unique_devices_names_hashlist = sorted(set(devices) - set(grouped_devices))
    return unique_devices_names_hashlist


def get_grouped_devices_names_from_devices_list(devices_list):
    grouped_devices_names = []
    for device in devices_list:
        grouped_devices_names.append(device['name'])
    return grouped_devices_names
