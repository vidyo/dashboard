"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from flask.ext.wtf import Form
from wtforms import StringField, HiddenField
from wtforms.validators import DataRequired
from flask import request, flash, g, url_for, redirect
import rethinkdb as r


class AddDeviceFamilyForm(Form):
    name = StringField('Name', [DataRequired()])


class DeleteDeviceFamilyForm(Form):
    id = HiddenField('Device Family ID', [DataRequired()])


class EditDeviceFamilyForm(Form):
    id = HiddenField('Deviceem Family ID', [DataRequired()])
    name = StringField('Name', [DataRequired()])


class AddDeviceForm(Form):
    name = StringField('Name', [DataRequired()])
    device_family_id = StringField('Device Family Id', [DataRequired()])


class EditDeviceForm(Form):
    id = HiddenField('Device ID', [DataRequired()])
    name = StringField('Device Name', [DataRequired()])
    device_family_id = StringField('Device Family Id', [DataRequired()])


class DeleteDeviceForm(Form):
    id = HiddenField('Device Family ID', [DataRequired()])


def process_device_family_add_form(device_type, device_print_name, prefix='add_device_family_form'):
    add_device_family_form = AddDeviceFamilyForm(request.form, prefix=prefix)
    if add_device_family_form.validate_on_submit():
        device_family_name = add_device_family_form.name.data
        os = list(r.table('connections_groups').filter({'name': device_family_name, 'type': device_type}).run(g.rdb_conn))
        if len(os) == 0:
            res = r.table('connections_groups').insert({'name': device_family_name, 'type': device_type}).run(g.rdb_conn)
            if res['inserted'] == 1:
                flash(device_print_name + ' Family was successfully added')
            else:
                flash('An error occurred while adding new ' + device_print_name + ' Family')
        else:
            flash(device_print_name + ' Family with this name already exist')
            return device_family_name
    return add_device_family_form


def process_device_family_delete_form(device_print_name, prefix="delete_device_family_form"):
    delete_device_family_form = DeleteDeviceFamilyForm(request.form, prefix=prefix)
    if delete_device_family_form.validate_on_submit():
        device_id = delete_device_family_form.id.data

        os = list(r.table('connections_groups').get(device_id).run(g.rdb_conn))

        if len(os):
                res = r.table('connections_groups').get(device_id).delete().run(g.rdb_conn)
                if res['deleted'] == 1:
                    flash(device_print_name + ' Family was successfully deleted')
                    return True
                else:
                    flash('An error occurred while removing ' + device_print_name + ' Family')
        else:
            flash('Unable to delete ' + device_print_name + ' Family')
    return delete_device_family_form


def process_device_family_edit(device_print_name, prefix="edit_device_family_form"):
    edit_device_family_form = EditDeviceFamilyForm(request.form, prefix=prefix)
    if edit_device_family_form.validate_on_submit():
        device_id = edit_device_family_form.id.data
        device_name = edit_device_family_form.name.data
        os = list(r.table('connections_groups').get(device_id).run(g.rdb_conn))
        if len(os):
                res = r.table('connections_groups').get(device_id).update({'name': device_name}).run(g.rdb_conn)
                if res['replaced'] == 1:
                    flash(device_print_name + ' Family was successfully updated')
                    return True
                else:
                    flash('An error occurred while updating '+device_print_name + ' Family')
        else:
            flash(device_print_name + ' Family does not exit')
    return edit_device_family_form


def process_device_add(device_type, device_print_name):
    add_device_form = AddDeviceForm(request.form, prefix="add_device_form")
    if add_device_form.validate_on_submit():
        device_name = add_device_form.name.data
        device_family_id = add_device_form.device_family_id.data
        os = list(r.table('connections_groups_devices').filter({'name': device_name, }).run(g.rdb_conn))
        if len(os) != 0:
            flash(device_print_name+' with this name already exist')
        else:
            os_family = r.table('connections_groups').get(device_family_id).run(g.rdb_conn)
            if os_family:
                res = r.table('connections_groups_devices').\
                    insert({'name': device_name, 'connections_group': device_family_id, 'type': device_type}).run(g.rdb_conn)
                if res['inserted'] == 1:
                    flash(device_print_name + ' was successfully added')
                    # return True
                else:
                    flash('An error occurred while adding new '+device_print_name)
            else:
                flash('Invalid ' + device_print_name + ' Family was selected')
    return add_device_form


def process_device_edit(device_print_name):
    edit_device_form = EditDeviceForm(request.form, prefix="edit_device_form")
    if edit_device_form.validate_on_submit():
        device_name = edit_device_form.name.data
        device_id = edit_device_form.id.data
        device_family_id = edit_device_form.device_family_id.data
        os_family = r.table('connections_groups').get(device_family_id).run(g.rdb_conn)
        os = r.table('connections_groups_devices').get(device_id).run(g.rdb_conn)

        if not os_family:
            flash('Invalid ' + device_print_name + ' Family was selected')
            return edit_device_form
        if not os:
            flash('Invalid ' + device_print_name)
            return edit_device_form

        res = r.table('connections_groups_devices').get(device_id).\
            update({'name': device_name, 'connections_group': device_family_id}).run(g.rdb_conn)
        if res['replaced'] == 1:
            flash(device_print_name + ' was successfully updated')
            # return True
        else:
            flash('An error occurred while adding new ' + device_print_name)
    return edit_device_form


def process_device_delete(device_print_name):
    delete_device_form = DeleteDeviceForm(request.form, prefix="delete_device_form")
    if delete_device_form.validate_on_submit():
        device_id = delete_device_form.id.data

        os = list(r.table('connections_groups_devices').get(device_id).run(g.rdb_conn))
        if len(os):
                res = r.table('connections_groups_devices').get(device_id).delete().run(g.rdb_conn)
                if res['deleted'] == 1:
                    flash(device_print_name + ' Family was successfully deleted')
                    return True
                else:
                    flash('An error occurred while removing ' + device_print_name + ' Family')
        else:
            flash('Unable to delete ' + device_print_name)
    return delete_device_form



