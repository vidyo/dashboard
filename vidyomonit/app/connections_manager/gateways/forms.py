"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from flask.ext.wtf import Form
from wtforms import StringField, HiddenField
from wtforms.validators import DataRequired
from flask import request, flash, g, url_for, redirect
import rethinkdb as r


class AddGatewayForm(Form):
    name = StringField('Name', [DataRequired()])
    gwid = StringField('GWID', [DataRequired()])


class DeleteGatewayForm(Form):
    id = HiddenField('Gateway ID', [DataRequired()])


class EditGatewayForm(Form):
    id = HiddenField('Gateway ID', [DataRequired()])
    name = StringField('Gateway Name', [DataRequired()])
    gwid = StringField('Gateway Id', [DataRequired()])


def process_gateway_add(gateway_print_name):
    print("Process gateway add")
    add_gateway_form = AddGatewayForm(request.form, prefix="add_gateway_form")
    if add_gateway_form.validate_on_submit():
        print("Submit validated")
        gateway_name = add_gateway_form.name.data
        gwid = add_gateway_form.gwid.data
        gateways = list(r.table('gateways').filter({'name': gateway_name, }).run(g.rdb_conn))
        if len(gateways) != 0:
            flash(gateway_print_name+' with this name already exist')
        else:
            gateways = list(r.table('gateways').filter({'gwid': gwid, }).run(g.rdb_conn))
            if len(gateways) != 0:
                flash(gateway_print_name + ' with this GWID already exist')
            else:
                res = r.table('gateways').insert({'name': gateway_name, 'gwid': gwid}).run(g.rdb_conn)
                if res['inserted'] == 1:
                    flash(gateway_print_name + ' was successfully added')
                    # return True
                else:
                    flash('An error occurred while adding new '+ gateway_print_name)
    return add_gateway_form


def process_gateway_delete(gateway_print_name):
    delete_gateway_form = DeleteGatewayForm(request.form, prefix="delete_gateway_form")
    if delete_gateway_form.validate_on_submit():
        gateway_id = delete_gateway_form.id.data

        gateways = list(r.table('gateways').get(gateway_id).run(g.rdb_conn))
        if len(gateways):
                res = r.table('gateways').get(gateway_id).delete().run(g.rdb_conn)
                if res['deleted'] == 1:
                    flash(gateway_print_name + ' was successfully deleted')
                    return True
                else:
                    flash('An error occurred while removing ' + gateway_print_name + ' Gateway')
        else:
            flash('Unable to delete ' + gateway_print_name)
    return delete_gateway_form


def process_gateway_edit(gateway_print_name):
    edit_gateway_form = EditGatewayForm(request.form, prefix="edit_gateway_form")
    if edit_gateway_form.validate_on_submit():
        gateway_name = edit_gateway_form.name.data
        gateway_gwid = edit_gateway_form.gwid.data
        gateway_id = edit_gateway_form.id.data

        res = r.table('gateways').get(gateway_id).\
            update({'name': gateway_name, 'gwid': gateway_gwid}).run(g.rdb_conn)
        if res['replaced'] == 1:
            flash(gateway_print_name + ' was successfully updated')
            # return True
        else:
            flash('An error occurred while adding new ' + gateway_print_name)
    return edit_gateway_form
