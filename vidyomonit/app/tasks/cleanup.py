"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
Remove any data not meant for long-term storage (basically snapshot data).
"""

import rethinkdb as r
from datetime import timedelta
import sys
sys.path.append('..')

from vidyomonit.app.tools.rdb_manager import get_rdb_conn
from vidyomonit.app.utils import task_wrapper, utc_now


@task_wrapper('cleanup')
def run_task(config=None):
    # Only store snapshot data from the past 48 hours:
    yesterday = utc_now() - timedelta(days=2)

    conn = get_rdb_conn(config)
    res = r.table('snapshot').between(None, yesterday).delete().run(conn)

    conn.close()
    return res


if __name__ == '__main__':
    run_task()
