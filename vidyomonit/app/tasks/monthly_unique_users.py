"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
Fetch and insert monthly distinct users, grouped by various attributes.
"""

import rethinkdb as r
from collections import defaultdict
from dateutil.relativedelta import relativedelta
import re
import sys
sys.path.append('..')

import vidyomonit.app.plugins.cdr_connector as cdr
from vidyomonit.app.tools.rdb_manager import get_rdb_conn
from vidyomonit.app.utils import task_wrapper, utc_month

@task_wrapper('monthly unique users')
def run_task(start_dt=None, end_dt=None):
    start_dt = start_dt or utc_month()
    end_dt = end_dt or start_dt + relativedelta(months=1)

    conns = cdr.fetch_conns_in_range(start_dt, end_dt)
    months = [{'id': k, 'tenants': v} for k, v in _aggregate_monthly_unique_users(conns).iteritems()]

    conn = get_rdb_conn()
    res = r.table('unique_users_per_month').insert(months, conflict='replace').run(conn)

    conn.close()
    return res


def _aggregate_monthly_unique_users(conns):
    """
    Given an array of connections, aggregate monthly distinct users grouped by endpoint type.
    """
    months = defaultdict(lambda: defaultdict(lambda: {
        'guest': 0, 'h323': 0, 'phone': 0, 'user': 0
    }))

    distinct_callers = defaultdict(lambda: defaultdict(set))
    phone_re = re.compile(r'^[+]?([0-9]+)(@|#)*(.*)$')

    for c in conns:
        dur = c['LeaveTime'] - c['JoinTime']
        seconds = dur.seconds + dur.days * 24 * 3600
        if seconds < 60:
            continue

        month = utc_month(c['JoinTime'])
        conn_tenant = c['TenantName']

        if c['CallerName'] in distinct_callers[conn_tenant][month]:
            continue
        distinct_callers[conn_tenant][month].add(c['CallerName'])

        endpoint_type = None
        if c['EndpointType'] == 'D':
            endpoint_type = 'user'
        elif c['EndpointType'] == 'G':
            endpoint_type = 'guest'
        elif c['EndpointType'] == 'L':
            if phone_re.match(c['CallerName']):
                endpoint_type = 'phone'
            else:
                endpoint_type = 'h323'

        if endpoint_type:
            for tenant in ['total', conn_tenant]:
                months[month][tenant][endpoint_type] += 1
            if conn_tenant == 'GATEWAY':
                if 'switch.ch' in c['ConferenceName']:
                    months[month]['CERN'][endpoint_type] += 1
                else:
                    months[month]['SWITCH'][endpoint_type] += 1

    return months


def restore_data(dt_from, dt_to):
    from datetime import timedelta
    day_count = (dt_to - dt_from).days + 1
    fetched_months = []
    for single_date in (dt_from + timedelta(n) for n in range(day_count)):
        if not utc_month(single_date) in fetched_months:
            fetched_months.append(utc_month(single_date))
            run_task(utc_month(single_date))
            print 'Added missing monthly unique users for: ' + single_date.strftime('%Y-%m-%d')
    return


def month_year_iter( start_month, start_year, end_month, end_year ):
    ym_start= 12*start_year + start_month - 1
    ym_end= 12*end_year + end_month - 1
    for ym in range( ym_start, ym_end ):
        y, m = divmod( ym, 12 )
        yield y, m+1

if __name__ == '__main__':
    run_task()
