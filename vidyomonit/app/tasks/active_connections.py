"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
Fetch and store a list of currently active connections.
"""

import rethinkdb as r
import sys
sys.path.append('..')

import vidyomonit.app.plugins.cdr_connector as cdr
from vidyomonit.app.tools.rdb_manager import get_rdb_conn
from vidyomonit.app.utils import task_wrapper

@task_wrapper('active conns')
def run_task():
    tenants = cdr.fetch_active_conns_data()

    conn = get_rdb_conn()
    res = r.table('active_connections').insert({
        'tenants': tenants,
        'updated': r.now(),
        'id': 0
    }, conflict='replace').run(conn)

    conn.close()

    return res


if __name__ == '__main__':
    run_task()
