"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
Update the tenants table.
"""

import rethinkdb as r
import sys
sys.path.append('..')

from vidyomonit.app.plugins import cdr_connector as cdr
from vidyomonit.app.tools.rdb_manager import get_rdb_conn
from vidyomonit.app.utils import task_wrapper


@task_wrapper('tenants')
def run_task():
    tenants = [{'id': t['tenantID'], 'name': t['tenantName']} for t in cdr.fetch_tenants()]

    conn = get_rdb_conn()
    res = r.table('tenants').insert(tenants, conflict='replace').run(conn)

    conn.close()
    return res


if __name__ == '__main__':
    run_task()
