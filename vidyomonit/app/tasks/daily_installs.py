"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
Fetch and insert daily installations by date.
"""

import rethinkdb as r
from collections import defaultdict
import sys
sys.path.append('..')

import app.plugins.cdr_connector as cdr
import conf
from vidyomonit.app.tools.rdb_manager import get_rdb_conn
from vidyomonit.app.utils import task_wrapper, utc, utc_from_timestamp


get_meeting_category = getattr(conf, 'GET_MEETING_CATEGORY', None)
if not get_meeting_category:
    raise Exception('conf.GET_MEETING_CATEGORY function is undefined!')


@task_wrapper('daily installs')
def run_task():
    installs_by_date = _aggregate_daily_installs(cdr.fetch_installs())
    dates = [{'id': k, 'tenants': v} for k, v in installs_by_date.iteritems()]

    conn = get_rdb_conn()
    res = r.table('installs_per_day').insert(dates, conflict='replace').run(conn)
    conn.close()

    return res


def _aggregate_daily_installs(rows):
    categories = ['users', 'devices', 'guests', 'total']
    dates = defaultdict(lambda: defaultdict(lambda: dict((k, {'total': 0, 'today': 0}) for k in categories)))
    tenant_totals = defaultdict(lambda: dict((k, 0) for k in categories))
    users_by_tenant = defaultdict(set)

    for row in rows:
        tenant, user, display, dt = row['tenantName'], row['userName'], row['displayName'], utc_from_timestamp(row['dt'])
        date = utc(dt.year, dt.month, dt.day)
        if tenant.lower() in [None, 'default', '']:
            tenant = conf.DEFAULT_TENANT

        is_guest = user == 'Guest' or display == 'Guest'

        for t in [tenant, 'total']:
            row_category = None

            if is_guest:
                row_category = 'guests'
            else:
                if user in users_by_tenant[t]:
                    row_category = 'devices'  # Existing user installing a new device
                else:
                    row_category = 'users'  # Completely new user
                    users_by_tenant[t].add(user)

            for c in [row_category, 'total']:
                tenant_totals[t][c] += 1
                dates[date][t][c]['today'] += 1

            for c in categories:
                dates[date][t][c]['total'] = tenant_totals[t][c]

    return dates


if __name__ == '__main__':
    run_task()
