"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
Fetch connections data (users, conferences etc.) of the given day.
"""

import rethinkdb as r
from collections import defaultdict
from datetime import timedelta
import sys
sys.path.append('..')
import pytz

import app.plugins.cdr_connector as cdr
from vidyomonit.app.tools.rdb_manager import get_rdb_conn
from vidyomonit.app.utils import task_wrapper, utc_today

@task_wrapper('daily connections')
def run_task(dt=None):
    dt = dt or utc_today()

    conns = cdr.fetch_conns_in_range(dt, dt + timedelta(days=1))
    tenants = _aggregate_conns_data(conns, dt)

    conn = get_rdb_conn()
    res = r.table('connections_per_day').insert({
        'tenants': tenants,
        'id': dt
    }, conflict='replace').run(conn)

    conn.close()

    return res


def restore_data(dt_from, dt_to):
    day_count = (dt_to - dt_from).days + 1
    for single_date in (dt_from + timedelta(n) for n in range(day_count)):
        run_task(single_date)
        print 'Added missing daily connections for: ' + single_date.strftime('%Y-%m-%d')
    return


def _aggregate_conns_data(conns, start_dt, end_dt=None):
    """
    Aggregate accumulating connections data.
    Note, connections must be sorted by ascending JoinTime!
    """
    end_dt = end_dt or start_dt + timedelta(days=1)
    tenants = defaultdict(lambda: {
        # Minutes from connections with just 1 participant are ignored;
        # hence, we need to keep track of the number of participants
        'minutes': defaultdict(lambda: {'participants': 0, 'seconds': 0}),
        'connections': {
            'total': 0,
            'connection_type': {'conference': 0, 'p2p': 0},
            'endpoint_type': defaultdict(int),
            'app_os': defaultdict(int),
            'app_version': defaultdict(int),
        },
        'max_sim_connections': 0
    })

    active_conns = defaultdict(list)

    for c in conns:
        # Minutes in connections:
        dur = min(c['LeaveTime'], end_dt) - max(c['JoinTime'], start_dt)
        seconds = dur.seconds + dur.days * 24 * 3600
        call_id = c['UniqueCallID']
        conn_tenant = c['TenantName']

        for tenant in [conn_tenant, 'total']:
            tenants[tenant]['minutes'][call_id]['participants'] += 1
            tenants[tenant]['minutes'][call_id]['seconds'] += seconds

        # Number of connections (Ignore connections shorter than 60s):
        if seconds >= 60:
            conn_type = 'p2p' if c['ConferenceType'] == 'D' and c['Direction'] == 'O' else 'conference'
            categories = [
                ('endpoint_type', 'EndpointType'),
                ('app_os', 'ApplicationOs'),
                ('app_version', 'ApplicationVersion'),
            ]

            for tenant in [conn_tenant, 'total']:
                tenants[tenant]['connections']['total'] += 1
                tenants[tenant]['connections']['connection_type'][conn_type] += 1

                for category_name, column_name in categories:
                    category_val = c[column_name]
                    if category_val:
                        tenants[tenant]['connections'][category_name][category_val] += 1

        # Max. simultaneous connections:
        floor = max(c['JoinTime'], start_dt)
        for tenant in [conn_tenant, 'total']:
            active_conns[tenant] = [ac for ac in active_conns[tenant] if ac['LeaveTime'] >= floor]
            active_conns[tenant].append(c)
            tenants[tenant]['max_sim_connections'] = max(
                tenants[tenant]['max_sim_connections'],
                len(active_conns[tenant])
            )

    for k, v in tenants.iteritems():
        seconds = sum(m['seconds'] for m in v['minutes'].itervalues() if m['participants'] > 1)
        tenants[k]['minutes'] = int(seconds / 60.0)

    return tenants


if __name__ == '__main__':
    run_task()
