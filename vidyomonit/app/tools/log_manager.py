"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

# Logging configuration

import logging
from logging.handlers import RotatingFileHandler

log_format = "%(asctime)s | %(levelname)s | %(name)s | %(message)s | %(module)s/%(filename)s | %(funcName)s():%(lineno)d"
# logging.basicConfig()
formatter = logging.Formatter(log_format)
logger = logging.getLogger('app')
logger.setLevel(logging.DEBUG)


def setup_log_webapp(configuration):
    """
    Setup log handlers.
    """

    if getattr(configuration, 'SENTRY_ENABLED', False):
        from raven.handlers.logging import SentryHandler
        sentry_handler = SentryHandler(configuration.SENTRY_DSN)
        sentry_handler.setLevel(logging.ERROR)
        sentry_handler.setFormatter(formatter)
        logger.addHandler(sentry_handler)

    if configuration.DEBUG:
        file_handler = RotatingFileHandler(configuration.LOG_DEV_WEBAPP, maxBytes=5*1024*1024, backupCount=5)
        file_handler.setLevel(logging.DEBUG)
    else:
        file_handler = RotatingFileHandler(configuration.LOG_PROD_WEBAPP, maxBytes=5*1024*1024, backupCount=5)
        file_handler.setLevel(logging.INFO)

    file_handler.setFormatter(formatter)

    logger.addHandler(file_handler)


def setup_log_tasks():
    from vidyomonit import conf
    if getattr(conf, 'SENTRY_ENABLED', False):
        from raven.handlers.logging import SentryHandler
        sentry_handler = SentryHandler(conf.SENTRY_DSN)
        sentry_handler.setLevel(logging.ERROR)
        sentry_handler.setFormatter(formatter)
        logger.addHandler(sentry_handler)

    if conf.DEBUG:
        file_handler = logging.handlers.RotatingFileHandler(conf.LOG_DEV_TASKS, maxBytes=5*1024*1024, backupCount=5)
        file_handler.setLevel(logging.DEBUG)
    else:
        file_handler = logging.handlers.RotatingFileHandler(conf.LOG_PROD_TASKS, maxBytes=5*1024*1024, backupCount=5)
        file_handler.setLevel(logging.INFO)

    file_handler.setFormatter(formatter)

    logger.addHandler(file_handler)
