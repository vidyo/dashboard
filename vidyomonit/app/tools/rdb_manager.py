"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

# RethinkDB manager

import rethinkdb as r
from rethinkdb import RqlRuntimeError


def db_setup(config, silent):
    """
    Init database and tables.
    """
    connection = r.connect(host=config['RDB_CONF']['host'], port=config['RDB_CONF']['port'])
    db = config['RDB_CONF']['db']
    try:
        r.db_create(db).run(connection)
    except RqlRuntimeError:
        if not silent:
            print 'Database already exists'

    tables = (
        'snapshot',                  # High detail snapshot of CDR's current state, stored in high intervals but for a short range (e.g. every 5min for past 24h)
        'active_connections',        # Currently active connections
        'connections_per_day',       # Connection stats per day
        'meetings_per_day',          # Meeting stats per day
        'installs_per_day',          # Client installation stats per day
        'registrations_per_day',     # Member registration stats per day
        'devices',                   # Vidyo portals, router and gateways by ID
        'unique_users_per_month',    # Unique user counts per month
        'tenants',                   # Vidyo tenant names by ID
        'connections_groups',        # Used for grouping connections in the dashboard evolution tab
        'connections_groups_devices',# Used for storing devuces that are groupped in the dashboard evolution tab
        'gateways'
    )

    for table in tables:
        try:
            r.db(db).table_create(table).run(connection)
            if not silent:
                print 'Created table:', table
        except RqlRuntimeError:
            if not silent:
                print 'Already exists, skipping:', table

    if not silent:
        print 'Database setup completed. Now run the app without --setup.'

    connection.close()


def get_rdb_conn(config=None):
    rdb_conf = None
    if not config:
        from vidyomonit import conf
        rdb_conf = conf.RDB_CONF
    else:
        rdb_conf = config
    return r.connect(**rdb_conf)
