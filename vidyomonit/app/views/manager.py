"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from flask import Blueprint, render_template, url_for, redirect, abort
from jinja2 import TemplateNotFound

module = Blueprint('manager', __name__)

@module.route('/', methods=['GET'])
def index():
    return redirect(url_for('manager.home'))


@module.route('/home')
def home():
    try:
        # if "user" in session:
        #     pass
        #     #current_app.logger.debug("Welcome {name}".format(name=session["user"]["nickname"]))
        return render_template('home.html')
    except TemplateNotFound:
        abort(404)

@module.route('/network-devices')
def network_devices():
    return render_template('network_devices.html')