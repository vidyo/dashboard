"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from flask import Blueprint, g, request, jsonify
import rethinkdb as r

from vidyomonit.app.utils import dt_parse, utc, utc_today

module = Blueprint('module_api_users', __name__)


@module.route('/monthly')
def monthly_unique_users():
    today = utc_today()
    from_dt = dt_parse(request.args.get('from', None), utc(today.year - 1, today.month, 1))
    until_dt = dt_parse(request.args.get('until', None), today)
    tenant = request.args.get('tenant', None)

    selection = r.table('unique_users_per_month').between(from_dt, until_dt).with_fields(
        'id',
        {'tenants': tenant} if tenant else 'tenants'
    ).order_by('id').run(g.rdb_conn)

    return jsonify({'data': list(selection)})
