"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from flask import Blueprint, jsonify, g

import socket
import rethinkdb as r
import logging
logger = logging.getLogger('app.webapp')

# from vidyomonit import conf
from vidyomonit.app.utils import json_error
from vidyomonit.app.tools.mysql_manager import MySQL_Manager
from flask import current_app

module = Blueprint('module_api_devices', __name__)


@module.route('/')
def get_devices():
    """
    Fetch a list of devices.
    """
    selection = r.table('devices').run(g.rdb_conn)
    return jsonify({'data': list(selection)})


@module.route('/info/<hostname>')
def get_device_info(hostname):
    """
    Fetch device info for the given hostname.
    """
    try:
        hostname = hostname.split(':')[-1].strip()
        ip = socket.gethostbyname(hostname)
    except Exception as e:
        logger.error("Couldn't match hostname %s to IP: %s" % (hostname, e))
        return json_error(404, "Couldn't match hostname to IP")

    query = """SELECT Identifier, DisplayName, ComponentType
        FROM NetworkElementConfiguration
        WHERE IpAddress=%s
        AND Status='ACTIVE'
        AND ComponentType != 'VidyoProxy';
    """

    conf = current_app.config
    row, _ = MySQL_Manager(conf['VIDYO_DBCDR']).execute(query, (ip,))
    if not row:
        return json_error(404, 'No device found in CDR')

    device_type = dict(
        VidyoRouter='router',
        VidyoGateway='gateway',
        VidyoManager='portal'
    ).get(row['ComponentType'])

    return jsonify(dict(
        id=row['Identifier'],
        label=row['DisplayName'],
        type=device_type,
        ip_address=ip
    ))
