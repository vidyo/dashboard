"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from flask import Blueprint, jsonify, g

import rethinkdb as r
import logging
logger = logging.getLogger('app.webapp')

module = Blueprint('module_api_tenants', __name__)


@module.route('/')
def get_tenants():
    """
    Fetch tenants by ID.
    """
    selection = r.table('tenants').run(g.rdb_conn)
    tenants = [s['name'] for s in selection] + ['total']
    return jsonify({'data': tenants})
