"""
CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from flask import Blueprint, jsonify, g

import rethinkdb as r
import logging
logger = logging.getLogger('app.webapp')


module = Blueprint('module_api_gateways', __name__)


@module.route('/')
def get_gateways():
    """
    Fetch a list of devices.
    """
    selection = r.table('gateways').run(g.rdb_conn)

    result = {}
    for element in selection:
        print element['gwid']
        result.update({element['gwid']: element['name']})

    return jsonify(result)