"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from flask import Blueprint, g, request, jsonify

import rethinkdb as r
from datetime import timedelta

from vidyomonit.app.utils import dt_parse, utc_now

module = Blueprint('module_api_installs', __name__)


@module.route('/daily')
def snapshot():
    now = utc_now()
    from_dt = dt_parse(request.args.get('from', None), now - timedelta(days=30))
    until_dt = dt_parse(request.args.get('until', None), now)
    tenant = request.args.get('tenant', None)

    selection = r.table('installs_per_day').between(from_dt, until_dt).with_fields(
        'id',
        {'tenants': tenant} if tenant else 'tenants'
    ).order_by('id').run(g.rdb_conn)

    return jsonify({'data': list(selection)})
