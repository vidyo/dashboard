"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from flask import Blueprint, g, request, jsonify

import rethinkdb as r
from datetime import timedelta, time

from vidyomonit.app.utils import dt_parse, utc_now, json_error

module = Blueprint('module_api_connections', __name__)

@module.route('/snapshot')
def snapshot():
    now = utc_now()
    from_dt = dt_parse(request.args.get('from', None), now - timedelta(days=1))
    until_dt = dt_parse(request.args.get('until', None), now)
    tenant = request.args.get('tenant', None)

    selection = list(r.table('snapshot').between(from_dt, until_dt).with_fields(
        'id',
        {'tenants': tenant} if tenant else 'tenants'
    ).order_by('id').run(g.rdb_conn))

    if not selection:
        return json_error(404, 'No snapshots found')

    # Map device IDs into labels:
    devices = dict([(d['id'], d) for d in list(r.table('devices').run(g.rdb_conn))])

    for row in selection:
        for data in row['tenants'].itervalues():
            routers, gateways = data.get('RouterID'), data.get('GWID')
            if routers:
                data['routers'] = dict((devices.get(key, {}).get('label') or key, val) for key, val in routers.iteritems())
                data.pop('RouterID', None)
            if gateways:
                data['gateways'] = dict((devices.get(key, {}).get('label') or key, val) for key, val in gateways.iteritems())
                data.pop('GWID', None)

    return jsonify({'data': selection})


@module.route('/daily')
def daily():
    now = utc_now()
    from_dt = dt_parse(request.args.get('from', None), now - timedelta(days=30))
    until_dt = dt_parse(request.args.get('until', None), now)
    tenant = request.args.get('tenant', None)

    selection = r.table('connections_per_day').between(from_dt, until_dt).with_fields(
        'id',
        {'tenants': tenant} if tenant else 'tenants'
    ).order_by('id').run(g.rdb_conn)

    app_os_groups = get_connections_groups_by_type('app_os')
    app_version_groups = get_connections_groups_by_type('app_version')
    endpoint_type_groups = get_connections_groups_by_type('endpoint_type')

    for s in selection:
        for t in s['tenants']:
            connections = s['tenants'][t]['connections']
            connections['app_os'] = group_connections(connections['app_os'], app_os_groups)
            connections['app_version'] = group_connections(connections['app_version'], app_version_groups)
            connections['endpoint_type'] = group_connections(connections['endpoint_type'], endpoint_type_groups)
            s['tenants'][t]['connections'] = connections

    return jsonify({'data': list(selection)})


@module.route('/active')
def active_conns():
    tenant = request.args.get('tenant', None)

    selection = list(r.table('active_connections').order_by(r.desc('updated')).with_fields(
        'id',
        'updated',
        {'tenants': tenant} if tenant else 'tenants'
    ).limit(1).run(g.rdb_conn))

    if not selection:
        return json_error(404, 'No active conns found')
    selection = selection[0]

    # Map devices into connections
    devices = dict([(d['id'], d) for d in list(r.table('devices').run(g.rdb_conn))])

    for conns in selection['tenants'].itervalues():
        for conn in conns:
            router, gateway = devices.get(conn.get('RouterID')), devices.get(conn.get('GWID'))
            if router:
                conn['RouterLabel'] = router['label']
                conn['RouterHostname'] = router['hostname']
                conn['RouterIP'] = router['ip_address']
            if gateway:
                conn['GatewayLabel'] = gateway['label']
                conn['GatewayHostname'] = gateway['hostname']
                conn['GatewayIP'] = gateway['ip_address']

    return jsonify(selection)

@module.route('/maxSimConnections')
def max_simultaneous_connections():
    tenant = request.args.get('tenant', None)
    selection = r.table('connections_per_day').with_fields(
        'id',
        {'tenants': tenant} if tenant else 'tenants'
    ).order_by('id').run(g.rdb_conn)
    max_sim_conns = 0
    max_sim_conns_date = None
    for stats_by_day in selection:
        for tenant_name in stats_by_day['tenants']:
            stats_by_day_by_tenant = stats_by_day['tenants'][tenant_name]
            if 'max_sim_connections' in stats_by_day_by_tenant:
                max_sim_con_tenant_day = stats_by_day_by_tenant['max_sim_connections']
                if max_sim_con_tenant_day > max_sim_conns:
                    max_sim_conns = max_sim_con_tenant_day
                    max_sim_conns_date = stats_by_day['id']

    return jsonify({'data': {
        'maximum_simultaneous_connections_amount': max_sim_conns,
        'maximum_simultaneous_connections_date': max_sim_conns_date,
    }})


def group_connections(connections, connections_groups):
    keys_to_delete = []
    keys_to_add = {}
    for c in connections:
        if c in connections_groups and connections_groups[c]['group_family']:
            c_value = connections[c]
            family = connections_groups[c]['group_family']['name']
            if family in keys_to_add:
                keys_to_add[family] += c_value
            else:
                keys_to_add[family] = c_value
            keys_to_delete.append(c)
    for k in keys_to_delete:
        connections.pop(k)
    connections.update(keys_to_add)
    return connections


def get_connections_groups_by_type(connections_group_type):
    groups = list(r.table('connections_groups_devices').filter({'type': connections_group_type}).merge(lambda device: {
        'group_family': r.table('connections_groups').get(device['connections_group'])
    }).run(g.rdb_conn))
    groups_by_name = {}
    for device in groups:
        groups_by_name[device['name']] = device
    return groups_by_name