"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from flask import Blueprint, jsonify, g, make_response
from flask.ext import restful
from flask_restful import reqparse
from IPy import IP

import rethinkdb as r
module = Blueprint('module_api_admin_devices', __name__)
api = restful.Api()
api.init_app(module)

def ip_address(value):
    try:
        IP(value)
        return value
    except ValueError:
        raise ValueError("Invalid IP address")


def device_type(value):
    valid_device_types = ['router', 'gateway', 'portal']
    if value.lower() not in valid_device_types:
        raise ValueError("Invalid device type")
    return value.lower()


def non_empty_str(value):
    if value == '':
        raise ValueError("Empty value")
    return value


class Devices(restful.Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('id', type=non_empty_str, ignore=True)
        self.parser.add_argument('label', type=non_empty_str, ignore=True)
        self.parser.add_argument('type', type=device_type, ignore=True)

    def get(self):
        selection = sorted(list(r.table('devices').run(g.rdb_conn)), key=lambda x: (x['label']))
        return jsonify({'data': selection})

    def post(self):
        self.parser.add_argument('hostname', type=str, ignore=True, help='help')
        self.parser.add_argument('ip_address', type=ip_address, ignore=True)

        args = self.parser.parse_args()
        invalid_fields = self.make_invalid_fields_reponse(args)

        if len(invalid_fields) > 0:
            return make_response(jsonify({
                'messages': {
                    'invalidFields': invalid_fields
                }
            }), 200)

        device = list(r.table('devices').filter(r.row['id'] == args['id']).run(g.rdb_conn))
        if len(device) > 0:
            return make_response(jsonify({
                'messages': {
                    'generalError': 'Device with this id already exist'
                }
            }), 200)
        res = r.table('devices').insert(args).run(g.rdb_conn)
        if res['inserted'] == 1:
            return make_response(jsonify({
                'messages': {
                    'generalFeedback': 'Device added'
                }
            }), 201)
        else:
            return make_response(jsonify({
                'messages': {
                    'generalError': res['errors']
                }
            }), 200)

    def put(self):
        args = self.parser.parse_args()
        invalid_fields = self.make_invalid_fields_reponse(args)

        if len(invalid_fields) > 0:
            return make_response(jsonify({
                'messages': {
                    'invalidFields': invalid_fields
                }
            }), 200)

        device = list(r.table('devices').filter(r.row['id'] == args['id']).run(g.rdb_conn))

        if len(device) < 1:
            return make_response(jsonify({
                'messages': {
                    'generalFeedback': 'Device with id {} does not exist'.format(args['id'])
                }
            }), 200)

        res = r.table('devices').filter(r.row['id'] == args['id']).update(args).run(g.rdb_conn)

        if res['replaced'] == 1:
            return make_response(jsonify({}), 204)
        else:
            return make_response(jsonify({
                'messages': {
                    'generalError': res['errors']
                }
            }), 200)

    def delete(self):
        self.parser.remove_argument('label')
        self.parser.remove_argument('type')
        args = self.parser.parse_args()
        res = None
        if args['id']:
            res = r.table('devices').get(args['id']).delete().run(g.rdb_conn)
        else:
            return make_response(jsonify({
                'messages': {
                    'generalError': 'Invalid parameters'
                }
            }), 200)
        if res['deleted'] == 1:
            return make_response(jsonify({
                'messages': {
                    'generalFeedback': 'Device with id %s was removed'.format(args['id'])
                }
            }), 200)
        else:
            return make_response(jsonify({
                'messages': {
                    'generalError': 'Failed to remove %s: %s' % (args['id'], res)
                }
            }), 200)

    def make_invalid_fields_reponse(self, args):
        invalid_fields = {}
        for arg in args:
            if args[arg] is None:
                invalid_fields[arg] = "Invalid value"
        return invalid_fields

api.add_resource(Devices, '/devices')
