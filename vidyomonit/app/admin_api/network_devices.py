"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from flask.ext import restful
from flask import Blueprint, jsonify, make_response
import vidyomonit.app.plugins.read_only_root_connector as rorc

module = Blueprint('module_api_admin_network_devices', __name__)
api = restful.Api()
api.init_app(module)
import logging
class NetworkDevices(restful.Resource):

    def get(self):
        logger = logging.getLogger('app.webapp')
        logger.error('Test error loging')
        devices = rorc.fetch_devices_data()
        return make_response(jsonify({'data': devices}), 200)

api.add_resource(NetworkDevices, '/networkDevices')