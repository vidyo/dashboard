"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from flask import Flask, g, abort
from flask.ext.cors import CORS

import logging
from rethinkdb.errors import RqlDriverError

import sys, os
# sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
sys.path.insert(0, '../')

from vidyomonit.app.tools import log_manager, rdb_manager


def create_app(config):
#TODO module of filename

    tpl_dir = 'templates'
    static_dir = 'static'
    app = Flask(__name__, template_folder=tpl_dir, static_folder=static_dir)
    app.config.from_object(config)
    cors = CORS(app)

    initialize_api_blueprints(app)
    initialize_admin_blueprints(app)

    if not app.config['TESTING']:
        log_manager.setup_log_webapp(config)
        logger = logging.getLogger('app.webapp')
        app.logger.addHandler(logger)


    @app.before_request
    def before_request():
        """
        Open a connection to db for each request.
        """
        try:
            g.rdb_conn = rdb_manager.get_rdb_conn(app.config['RDB_CONF'])
        except RqlDriverError:
            abort(503, "No database connection could be established.")


    @app.teardown_request
    def teardown_request(exception):
        """
        Close db connection after request.
        """
        try:
            g.rdb_conn.close()
        except AttributeError:
            pass

    return app


def initialize_api_blueprints(app):
    from vidyomonit.app.api.connections import module as module_api_connections
    from vidyomonit.app.api.meetings import module as module_api_meetings
    from vidyomonit.app.api.devices import module as module_api_devices
    from vidyomonit.app.api.installs import module as module_api_installs
    from vidyomonit.app.api.registrations import module as module_api_registrations
    from vidyomonit.app.api.users import module as module_api_users
    from vidyomonit.app.api.tenants import module as module_api_tenants
    from vidyomonit.app.api.gateways import module as module_api_gateways

    from vidyomonit.app.admin_api.devices import module as module_api_admin_devices
    from vidyomonit.app.admin_api.network_devices import module as module_api_admin_network_devices

    app.register_blueprint(module_api_connections, url_prefix='/api/connections')
    app.register_blueprint(module_api_meetings, url_prefix='/api/meetings')
    app.register_blueprint(module_api_devices, url_prefix='/api/devices')
    app.register_blueprint(module_api_installs, url_prefix='/api/installs')
    app.register_blueprint(module_api_registrations, url_prefix='/api/registrations')
    app.register_blueprint(module_api_users, url_prefix='/api/users')
    app.register_blueprint(module_api_tenants, url_prefix='/api/tenants')
    app.register_blueprint(module_api_gateways, url_prefix='/api/gateways')

    app.register_blueprint(module_api_admin_devices, url_prefix='/api/admin')
    app.register_blueprint(module_api_admin_network_devices, url_prefix='/api/admin')


def initialize_admin_blueprints(app):
    from vidyomonit.app.views.manager import module as module_manger_panel
    from vidyomonit.app.connections_manager.views import module as module_connections_manager

    app.register_blueprint(module_manger_panel, url_prefix='')
    app.register_blueprint(module_connections_manager, url_prefix='/connections')
