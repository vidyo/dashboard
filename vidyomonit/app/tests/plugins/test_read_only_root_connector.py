"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'pkwiatkowski'
from vidyomonit.app.tests.BaseVidyomonitTestCase import BaseVidyomonitTestCase
from vidyomonit.app.plugins.read_only_root_connector import fetch_devices_data

class PluginReadOnlyRootConnectorTestCase(BaseVidyomonitTestCase):


    def test_fetching_devices_data_with_proper_data(self):
        data = fetch_devices_data()
        self.assertEqual(len(data), 4)

        device1 = data[0]
        device2 = data[1]
        device3 = data[2]
        device4 = data[3]

        self.assertEqual(device1['label'], 'DisplayName1')
        self.assertEqual(device1['ip_address'], '137.138.248.228')
        self.assertEqual(device1['type'], 'gateway')
        self.assertEqual(device1['hostname'], 'vidyogw1.cern.ch')

        self.assertEqual(device2['label'], 'DisplayName2')
        self.assertEqual(device2['ip_address'], '137.138.248.230')
        self.assertEqual(device2['type'], '')
        self.assertEqual(device2['hostname'], 'vidyoportal.cern.ch')

        self.assertEqual(device3['label'], 'DisplayName3')
        self.assertEqual(device3['ip_address'], '137.138.248.229')
        self.assertEqual(device3['type'], 'router')
        self.assertEqual(device3['hostname'], 'vidyorouter1.cern.ch')

        self.assertEqual(device4['label'], 'DisplayName4')
        self.assertEqual(device4['ip_address'], '192.91.244.171')
        self.assertEqual(device4['type'], 'portal')
        self.assertEqual(device4['hostname'], 'vidyorouter2.cern.ch')

    def test_fetching_devices_data_with_empty_databse(self):
        #TODO
        None

    def test_fetching_devices_data_with_invalid_data(self):
        #TODO
        None