"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'pkwiatkowski'
from vidyomonit.app.tests.BaseVidyomonitTestCase import BaseVidyomonitTestCase
from vidyomonit.app.plugins.cdr_connector import fetch_conns_in_range
import datetime

class PluginCDRConnectorFetchConnsInRangeTestCase(BaseVidyomonitTestCase):

    def test_fetching_conns_in_range_proper_data(self):
        date_two_days_ago_ago = self.localize_date(datetime.datetime.utcnow() - datetime.timedelta(days=2))
        date_day_ago = self.localize_date(datetime.datetime.utcnow() - datetime.timedelta(days=1))
        date_now = self.localize_date(datetime.datetime.utcnow())

        data = fetch_conns_in_range(start_dt=date_two_days_ago_ago, end_dt=date_now, configuration=self.config)
        data_listed = list()
        for d in data:
            data_listed.append(d)

        self.assertEqual(len(data_listed), 6)
        is_start_date_change_tested = False
        for d in data_listed:
            self.analyze_response_structure(d)
            if str(d['CallID']) == '111112':
                is_start_date_change_tested = True
                self.assertEqual(d['JoinTime'].date(), date_day_ago.date())

        self.assertTrue(is_start_date_change_tested)

        self.assertEqual('111114', str(data_listed[0]['CallID']))
        self.assertEqual('1111141', str(data_listed[1]['CallID']))
        self.assertEqual('111112', str(data_listed[2]['CallID']))
        self.assertEqual('111111', str(data_listed[3]['CallID']))
        self.assertEqual('111113', str(data_listed[4]['CallID']))
        self.assertTrue(data_listed[4]['LeaveTime'] is not None)
        self.assertEqual('111116', str(data_listed[5]['CallID']))



    def test_fetching_conns_in_range_with_empty_table(self):
        self.clear_mysql_database()

        date_two_days_ago_ago = self.localize_date(datetime.datetime.utcnow() - datetime.timedelta(days=2))
        date_now = self.localize_date(datetime.datetime.utcnow())

        data = fetch_conns_in_range(start_dt=date_two_days_ago_ago, end_dt=date_now, configuration=self.config)

        data_listed = list()
        for d in data:
            data_listed.append(d)

        self.assertEqual(len(data_listed), 0)

    def test_fetching_conns_in_range_proper_without_end_time(self):
        date_two_days_ago = self.localize_date(datetime.datetime.utcnow() - datetime.timedelta(days=2))

        data = fetch_conns_in_range(start_dt=date_two_days_ago, configuration=self.config)

        data_listed = list()
        for d in data:
            data_listed.append(d)

        self.assertEqual(len(data_listed), 3)
        self.assertEqual('111114', str(data_listed[0]['CallID']))
        self.assertEqual('1111141', str(data_listed[1]['CallID']))
        return

    def test_fetching_conns_in_range_without_data(self):
        date_20_days = self.localize_date(datetime.datetime.utcnow() - datetime.timedelta(days=20))
        date_18_days = self.localize_date(datetime.datetime.utcnow() - datetime.timedelta(days=18))
        data = fetch_conns_in_range(start_dt=date_20_days, end_dt=date_18_days, configuration=self.config)

        data_listed = list()
        for d in data:
            data_listed.append(d)

        self.assertEqual(len(data_listed), 0)

    def analyze_response_structure(self, data):
        self.assertTrue('ApplicationOs' in data)
        self.assertTrue('ApplicationVersion' in data)
        self.assertTrue('CallID' in data)
        self.assertTrue('CallState' in data)
        self.assertTrue('CallerID' in data)
        self.assertTrue('CallerName' in data)
        self.assertTrue('ConferenceName' in data)
        self.assertTrue('ConferenceType' in data)
        self.assertTrue('Direction' in data)
        self.assertTrue('EndpointType' in data)
        self.assertTrue('GWID' in data)
        self.assertTrue('JoinTime' in data)
        self.assertTrue('LeaveTime' in data)
        self.assertTrue('RouterID' in data)
        self.assertTrue('TenantName' in data)
        self.assertTrue('UniqueCallID' in data)




