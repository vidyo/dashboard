"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'pkwiatkowski'
from vidyomonit.app.tests.BaseVidyomonitTestCase import BaseVidyomonitTestCase
from vidyomonit.app.plugins.cdr_connector import fetch_tenants


class PluginCDRConnectorFetchTenantsTestCase(BaseVidyomonitTestCase):
    def test_fetching_tenants(self):
        data = fetch_tenants(self.config)

        data_listed = list()
        for d in data:
            data_listed.append(d)

        self.assertEqual(len(data_listed), 3)
        self.assertEqual('1', str(data_listed[0]['tenantID']))

    def test_fetching_tenants_with_empty_table(self):
        self.clear_mysql_database()
        data = fetch_tenants(self.config)

        data_listed = list()
        for d in data:
            data_listed.append(d)

        self.assertEqual(len(data_listed), 0)



