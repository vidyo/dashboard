"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'pkwiatkowski'
from vidyomonit.app.tests.BaseVidyomonitTestCase import BaseVidyomonitTestCase
from vidyomonit.app.plugins.cdr_connector import fetch_installs


class PluginCDRConnectorFetchInstallsTestCase(BaseVidyomonitTestCase):

    def test_fetching_installs_with_proper_data(self):
        data = fetch_installs(self.config)

        self.assertEqual(len(data), 5)
        for d in data:
            self.analyze_response_row_structure(d)

        self.assertEqual('someUser2', data[0]['userName'])
        self.assertEqual('someUser1', data[1]['userName'])
        self.assertEqual('someUser', data[2]['userName'])
        self.assertEqual('someUser4', data[3]['userName'])
        self.assertEqual('someUser4', data[4]['userName'])

    def test_fetching_snapshot_data_with_empty_table(self):
        self.clear_mysql_database()
        data = fetch_installs(self.config)
        self.assertEqual(len(data), 0)

    def analyze_response_row_structure(self, data):
        self.assertTrue('dt' in data)
        self.assertTrue('tenantName' in data)
        self.assertTrue('userName' in data)
        self.assertTrue('displayName' in data)



