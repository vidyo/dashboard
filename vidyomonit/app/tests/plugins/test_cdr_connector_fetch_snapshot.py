"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'pkwiatkowski'
from vidyomonit.app.tests.BaseVidyomonitTestCase import BaseVidyomonitTestCase
from vidyomonit.app.plugins.cdr_connector import fetch_snapshot_data


class PluginCDRConnectorFetchSnapshotTestCase(BaseVidyomonitTestCase):

    def test_fetching_snapshot_data_with_proper_data(self):
        data = fetch_snapshot_data(self.config)
        self.assertEqual(len(data), 3)
        self.assertTrue('total' in data)
        self.assertTrue('CERN' in data)
        self.assertTrue('GATEWAY' in data)

        self.assertTrue('ConferenceType' in data['total'])
        self.assertTrue('EndpointType' in data['total'])
        self.assertTrue('GWID' in data['total'])
        self.assertTrue('RouterID' in data['total'])
        self.assertTrue('total' in data['total'])

        self.assertEqual(len(data['CERN']), 5)
        self.assertEqual(data['CERN']['ConferenceType']['C'], 3)
        self.assertEqual(data['CERN']['ConferenceType']['D'], 1)
        self.assertEqual(data['CERN']['EndpointType']['G'], 3)
        self.assertEqual(data['CERN']['EndpointType']['D'], 1)
        self.assertEqual(len(data['CERN']['RouterID']), 2)
        self.assertTrue('Router1ID' in data['CERN']['RouterID'])
        self.assertTrue('Router3ID' in data['CERN']['RouterID'])
        self.assertEqual(data['CERN']['RouterID']['Router1ID'], 3)
        self.assertEqual(data['CERN']['RouterID']['Router3ID'], 1)

        self.assertEqual(len(data['GATEWAY']), 5)
        self.assertEqual(data['GATEWAY']['ConferenceType']['C'], 1)
        self.assertEqual(data['GATEWAY']['EndpointType']['D'], 1)

    def test_fetching_snapshot_data_with_empty_table(self):
        self.clear_mysql_database()
        data = fetch_snapshot_data(self.config)
        self.assertEqual(len(data), 0)


