"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'pkwiatkowski'
from vidyomonit.app.tests.BaseVidyomonitTestCase import BaseVidyomonitTestCase
from vidyomonit.app.plugins.cdr_connector import fetch_active_conns_data


class PluginCDRConnectorFetchActiveConnsTestCase(BaseVidyomonitTestCase):

    def test_fetching_active_conns_data_with_proper_data(self):

        data = fetch_active_conns_data(self.config)
        self.assertEqual(len(data), 3)
        self.assertTrue('total' in data)
        self.assertTrue('CERN' in data)
        self.assertTrue('GATEWAY' in data)

        self.assertEqual(len(data['CERN']), 4)
        self.assertEqual(len(data['GATEWAY']), 1)
        self.assertEqual(len(data['total']), 6)

        self.assertTrue('CallID' in data['total'][0])
        self.assertTrue('CallerID' in data['total'][0])
        self.assertTrue('CallerName' in data['total'][0])
        self.assertTrue('ConferenceName' in data['total'][0])
        self.assertTrue('EndpointType' in data['total'][0])
        self.assertTrue('GWID' in data['total'][0])
        self.assertTrue('JoinTime' in data['total'][0])
        self.assertTrue('RouterID' in data['total'][0])
        self.assertTrue('TenantName' in data['total'][0])
        self.assertTrue('UniqueCallID' in data['total'][0])

        self.assertEqual('111111', str(data['GATEWAY'][0]['CallID']))
        self.assertEqual('111111111', data['GATEWAY'][0]['UniqueCallID'])
        self.assertEqual('SomeName', data['GATEWAY'][0]['ConferenceName'])
        self.assertEqual('SomeCallerName', data['GATEWAY'][0]['CallerName'])

    def test_fetching_active_conns_data_with_empty_table(self):
        self.clear_mysql_database()
        data = fetch_active_conns_data(self.config)
        self.assertEqual(len(data), 0)


