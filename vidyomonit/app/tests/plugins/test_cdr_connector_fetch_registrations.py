"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'pkwiatkowski'
from vidyomonit.app.tests.BaseVidyomonitTestCase import BaseVidyomonitTestCase
from vidyomonit.app.plugins.cdr_connector import fetch_registrations


class PluginCDRConnectorFetchRegistrationsTestCase(BaseVidyomonitTestCase):

    def test_fetching_registrations(self):
        data = fetch_registrations(self.config)

        data_listed = list()
        for d in data:
            data_listed.append(d)

        self.assertEqual(len(data_listed), 2)
        for d in data_listed:
            self.analyze_response_structure(d)

        self.assertEqual('memberName3', data_listed[0]['memberName'])
        self.assertEqual('someRole3', data_listed[0]['roleName'])
        self.assertEqual('tenant1', data_listed[0]['tenantName'])
        self.assertEqual('groupName1', data_listed[0]['groupName'])

        self.assertEqual('memberName4', data_listed[1]['memberName'])
        self.assertEqual('someRole2', data_listed[1]['roleName'])
        self.assertEqual('tenant2', data_listed[1]['tenantName'])
        self.assertEqual('groupName2', data_listed[1]['groupName'])

    def test_fetching_registrations_with_empty_table(self):
        self.clear_mysql_database()
        data = fetch_registrations(self.config)

        data_listed = list()
        for d in data:
            data_listed.append(d)

        self.assertEqual(len(data_listed), 0)

    def analyze_response_structure(self, data):
        self.assertTrue('memberName' in data)
        self.assertTrue('memberCreated' in data)
        self.assertTrue('roleName' in data)
        self.assertTrue('tenantName' in data)
        self.assertTrue('groupName' in data)

