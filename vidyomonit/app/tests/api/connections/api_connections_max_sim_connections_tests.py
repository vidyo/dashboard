"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from vidyomonit.app.tests.api.VidyomonitApiTestCase import VidyomonitApiTestCase
import json

class ApiConnectionsMaxSimConnectionsTestCase(VidyomonitApiTestCase):

    def get_get_response(self, tenant=''):
        return self.get_api_get_response('/api/connections/maxSimConnections', tenant_param=tenant)

    def testMaxSimConnectionsWithoutAnyParameters(self):
        res = self.get_get_response()

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']
        self.assertEqual(len(jsoned_res), 2)

        self.checkResponseStructure(jsoned_res)
        self.assertEqual(jsoned_res['maximum_simultaneous_connections_amount'], 20)
        self.assertNotEqual(jsoned_res['maximum_simultaneous_connections_date'], '')

    def testMaxSimConnectionsWithCERNTenantParameters(self):
        res = self.get_get_response(tenant='CERN')

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']
        self.assertEqual(len(jsoned_res), 2)

        self.checkResponseStructure(jsoned_res)
        self.assertEqual(jsoned_res['maximum_simultaneous_connections_amount'], 11)
        self.assertNotEqual(jsoned_res['maximum_simultaneous_connections_date'], '')

    def testMaxSimConnectionsInvalidTenantParameters(self):
        res = self.get_get_response(tenant='invalid')

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']
        self.assertEqual(len(jsoned_res), 2)

        self.checkResponseStructure(jsoned_res)
        self.assertEqual(jsoned_res['maximum_simultaneous_connections_amount'], 0)
        self.assertEqual(jsoned_res['maximum_simultaneous_connections_date'], None)

    def checkResponseStructure(self, res):
        self.assertTrue('maximum_simultaneous_connections_amount' in res)
        self.assertTrue('maximum_simultaneous_connections_date' in res)