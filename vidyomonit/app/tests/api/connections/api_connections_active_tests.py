"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from vidyomonit.app.tests.api.VidyomonitApiTestCase import VidyomonitApiTestCase
from vidyomonit.app.tests.tools.test_data_generator import TestDataGenerator as TDG
import json


class ApiConnectionsActiveTestCase(VidyomonitApiTestCase):

    tenant_data_template = {
        "CallID": {},
        "CallerID": {},
        "CallerName": {},
        "ConferenceName": {},
        "EndpointType": {},
        "GWID": {},
        "JoinTime": {},
        "RouterHostname": {},
        "RouterID": {},
        "RouterIP": {},
        "RouterLabel": {},
        "TenantName": {},
        "UniqueCallID": {}
    }

    def get_get_response(self, tenant=''):
        return self.get_api_get_response('/api/connections/active', tenant_param=tenant)

    def testActiveWithNoParameters(self):
        res = self.get_get_response()

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)

        self.assertEqual(len(jsoned_res), 3)
        self.assertTrue('updated' in jsoned_res)
        self.assertTrue('id' in jsoned_res)
        self.assertTrue('tenants' in jsoned_res)
        self.doTestResponse(jsoned_res)

    def testActiveWithTenantCERNParameter(self):
        res = self.get_get_response(tenant='CERN')

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)

        self.assertEqual(len(jsoned_res), 3)
        self.assertTrue('updated' in jsoned_res)
        self.assertTrue('id' in jsoned_res)
        self.assertTrue('tenants' in jsoned_res)
        self.doTestResponse(jsoned_res, tenant_constrain_name='CERN')

    def testActiveWithTenantGATEWAYParameter(self):
        res = self.get_get_response(tenant='GATEWAY')

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)

        self.assertEqual(len(jsoned_res), 3)
        self.assertTrue('updated' in jsoned_res)
        self.assertTrue('id' in jsoned_res)
        self.assertTrue('tenants' in jsoned_res)
        self.doTestResponse(jsoned_res, tenant_constrain_name='GATEWAY')

    def testWithInvalidTenantParameter(self):
        res = self.get_get_response(tenant='invalid')

        self.assertEqual(res._status_code, 404)

    def doTestResponse(self, data, tenant_constrain_name=''):

        if tenant_constrain_name:
            self.assertTrue(tenant_constrain_name in data['tenants'])
            self.assertEqual(len(data['tenants']), 1)

        if not tenant_constrain_name or tenant_constrain_name == 'CERN':
            #CERN TENANT
            self.validateDataStructureForTenantData(data['tenants']['CERN'][0])
            self.assertEqual(data['tenants']['CERN'][0]['CallID'], 1)

            self.validateDataStructureForTenantData(data['tenants']['CERN'][1])
            self.assertEqual(data['tenants']['CERN'][1]['CallID'], 2)

            self.assertEqual(data['tenants']['CERN'][2]['CallID'], 3)
            self.assertTrue('RouterHostname' not in data['tenants']['CERN'][2])
            self.assertTrue('RouterLabel' not in data['tenants']['CERN'][2])
            self.assertTrue('RouterIP' not in data['tenants']['CERN'][2])

        if not tenant_constrain_name or tenant_constrain_name == 'GATEWAY':
            #GATEWAY TENANT
            self.validateDataStructureForTenantData(data['tenants']['GATEWAY'][0])
            self.assertEqual(data['tenants']['GATEWAY'][0]['CallID'], 4)

            self.validateDataStructureForTenantData(data['tenants']['GATEWAY'][1])
            self.assertEqual(data['tenants']['GATEWAY'][1]['CallID'], 5)

            self.assertTrue('GatewayHostname' not in data['tenants']['GATEWAY'][2])
            self.assertTrue('GatewayLabel'    not in data['tenants']['GATEWAY'][2])
            self.assertTrue('GatewayIP'       not in data['tenants']['GATEWAY'][2])
            self.assertTrue('RouterHostname'  not in data['tenants']['GATEWAY'][2])
            self.assertTrue('RouterLabel'     not in data['tenants']['GATEWAY'][2])
            self.assertTrue('RouterIP'        not in data['tenants']['GATEWAY'][2])
            self.assertEqual(data['tenants']['GATEWAY'][2]['CallID'], 6)

        if not tenant_constrain_name or tenant_constrain_name == 'total':
            #total TENANT
            self.validateDataStructureForTenantData(data['tenants']['total'][0])
            self.validateDataStructureForTenantData(data['tenants']['total'][1])
            self.validateDataStructureForTenantData(data['tenants']['total'][3])
            self.validateDataStructureForTenantData(data['tenants']['total'][4])

            self.assertEqual(data['tenants']['total'][0]['CallID'], 7)
            self.assertEqual(data['tenants']['total'][1]['CallID'], 8)
            self.assertEqual(data['tenants']['total'][2]['CallID'], 9)

            self.assertTrue('RouterHostname' not in data['tenants']['total'][2])
            self.assertTrue('RouterLabel'    not in data['tenants']['total'][2])
            self.assertTrue('RouterIP'       not in data['tenants']['total'][2])

            self.assertEqual(data['tenants']['total'][3]['CallID'], 10)

            self.assertEqual(data['tenants']['total'][4]['CallID'], 11)

            self.assertEqual(data['tenants']['total'][5]['CallID'], 12)

            self.assertTrue('GatewayHostname'   not in data['tenants']['total'][5])
            self.assertTrue('GatewayLabel'      not in data['tenants']['total'][5])
            self.assertTrue('GatewayIP'         not in data['tenants']['total'][5])
            self.assertTrue('RouterHostname'    not in data['tenants']['total'][5])
            self.assertTrue('RouterLabel'       not in data['tenants']['total'][5])
            self.assertTrue('RouterIP'          not in data['tenants']['total'][5])

    def validateDataStructureForTenantData(self, tenant_data):
        self.doCompareDictionariesStructure(self.tenant_data_template, tenant_data)
