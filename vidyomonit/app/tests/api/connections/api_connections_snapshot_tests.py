"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from vidyomonit.app.tests.api.VidyomonitApiTestCase import VidyomonitApiTestCase
from vidyomonit.app.tests.tools.test_data_generator import TestDataGenerator as TDG
import json
import datetime


class ApiConnectionsSnapshotTestCase(VidyomonitApiTestCase):

    tenant_data_template = {
        "ConferenceType": {},
          "EndpointType": {},
          "routers": {},
          "total": {}
    }
    def get_get_response(self, from_param='', until_param='', tenant_param=''):
        return self.get_api_get_response('/api/connections/snapshot', from_param, until_param, tenant_param)

    def testSnapshotWithoutAnyParameters(self):
        res = self.get_get_response()
        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 1)
        self.doTestResponseFrom1HourAgo(jsoned_res[0])

    def testSnapshotWithFromParameter(self):
        date_two_days_and_hour_ago = datetime.datetime.utcnow() - datetime.timedelta(days=2, hours=2)
        from_param_encoded = self.localize_and_encdode_date(date_two_days_and_hour_ago)

        res = self.get_get_response(from_param=from_param_encoded)

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 2)
        self.doTestingOnResponseFromTwoDaysAgo(jsoned_res[0])
        self.doTestResponseFrom1HourAgo(jsoned_res[1])

    def testSnapshotWithInvalidFromParameter(self):
        res = self.get_get_response(from_param='invalid')

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 1)
        self.doTestResponseFrom1HourAgo(jsoned_res[0])

    def testSnapshotWithUntilParameter(self):
        date_two_days_and_hour_ago = datetime.datetime.utcnow() - datetime.timedelta(days=2, hours=2)
        from_param_encoded = self.localize_and_encdode_date(date_two_days_and_hour_ago)

        date_two_hours_ago = datetime.datetime.utcnow() - datetime.timedelta(hours=2)
        to_param_encoded = self.localize_and_encdode_date(date_two_hours_ago)

        res = self.get_get_response(from_param=from_param_encoded, until_param=to_param_encoded)

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 1)
        self.doTestingOnResponseFromTwoDaysAgo(jsoned_res[0])

    def testSnapshotWithInvalidUntilParameter(self):
        date_two_days_and_hour_ago = datetime.datetime.utcnow() - datetime.timedelta(days=2, hours=2)
        from_param_encoded = self.localize_and_encdode_date(date_two_days_and_hour_ago)

        res = self.get_get_response(until_param='invalid')

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 1)
        self.doTestResponseFrom1HourAgo(jsoned_res[0])

    def testSnapshotWithCERNTenantParameter(self):
        res = self.get_get_response(tenant_param='CERN')

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']
        self.assertEqual(len(jsoned_res), 1)
        self.doTestResponseFrom1HourAgo(jsoned_res[0], tenant_constrain_name='CERN')

    def testSnapshotWithGATEWAYTenantParameter(self):
        res = self.get_get_response(tenant_param='GATEWAY')

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']
        self.assertEqual(len(jsoned_res), 1)
        self.doTestResponseFrom1HourAgo(jsoned_res[0], tenant_constrain_name='GATEWAY')

    def testSnapshotWithTotalTenantParameter(self):
        res = self.get_get_response(tenant_param='total')

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']
        self.assertEqual(len(jsoned_res), 1)
        self.doTestResponseFrom1HourAgo(jsoned_res[0], tenant_constrain_name='total')

    def testSnapshotWithInvalidTenantParameter(self):
        res = self.get_get_response(tenant_param='invalid')

        self.assertEqual(res._status_code, 404)

    def testSnapshotUsingTimeRangeWithNoSnapshot(self):
        date_three_days_and_hour_ago = datetime.datetime.utcnow() - datetime.timedelta(days=3)
        from_param_encoded = self.localize_and_encdode_date(date_three_days_and_hour_ago)

        date_two_days_and_hour_ago = datetime.datetime.utcnow() - datetime.timedelta(days=2, hours=2)
        to_param_encoded = self.localize_and_encdode_date(date_two_days_and_hour_ago)

        res = self.get_get_response(from_param=from_param_encoded, until_param=to_param_encoded)

        self.assertEqual(res._status_code, 404)

    def testSnapshotUsingFutureFromParameter(self):
        date_tommorow = datetime.datetime.utcnow() + datetime.timedelta(days=1)
        from_param_encoded = self.localize_and_encdode_date(date_tommorow)

        res = self.get_get_response(from_param=from_param_encoded)

        self.assertEqual(res._status_code, 404)

    def doTestResponseFrom1HourAgo(self, data, tenant_constrain_name=''):
        self.assertTrue('tenants' in data)
        if tenant_constrain_name:
            self.assertEqual(len(data['tenants']), 1)
            self.assertTrue(tenant_constrain_name in data['tenants'])

        if not tenant_constrain_name or tenant_constrain_name == 'CERN':
            #CERN TENANT
            self.assertTrue('CERN' in data['tenants'])
            self.validateDataStructureForTenantData(data['tenants']['CERN'])
            self.assertEqual(data['tenants']['CERN']['routers'][TDG.ROUTER_1_LABEL], 1)
            self.assertFalse('gateways' in data['tenants']['CERN'])
            self.assertEqual(data['tenants']['CERN']['GWID'], {})

        if not tenant_constrain_name or tenant_constrain_name == 'GATEWAY':
            #GATEWAY TENANT
            self.assertTrue('GATEWAY' in data['tenants'])
            self.validateDataStructureForTenantData(data['tenants']['GATEWAY'])
            self.assertEqual(data['tenants']['GATEWAY']['routers'][TDG.ROUTER_1_LABEL], 4)
            self.assertTrue('gateways' in data['tenants']['GATEWAY'])
            self.assertEqual(data['tenants']['GATEWAY']['gateways'][TDG.GATEWAY_1_LABEL], 4)
            self.assertFalse('GWID' in data['tenants']['GATEWAY'])


        if not tenant_constrain_name or tenant_constrain_name == 'total':
            self.assertTrue('total' in data['tenants'])
            self.validateDataStructureForTenantData(data['tenants']['total'])
            self.assertEqual(data['tenants']['total']['routers'][TDG.ROUTER_1_LABEL], 10)


            self.assertTrue('gateways' in data['tenants']['total'])
            self.assertFalse('GWID' in data['tenants']['total'])

            self.assertEqual(data['tenants']['total']['gateways'][TDG.GATEWAY_1_LABEL], 7)
            self.assertEqual(data['tenants']['total']['gateways'][TDG.GATEWAY_3_ID], 9)

    def doTestingOnResponseFromTwoDaysAgo(self, data):
        #CERN TENANT
        self.assertEqual(data['tenants']['CERN']['routers'][TDG.ROUTER_1_LABEL], 111)
        self.assertEqual(data['tenants']['CERN']['routers'][TDG.ROUTER_3_ID], 333)

        self.assertFalse('gateways' in data['tenants']['CERN'])
        self.assertTrue('GWID' in data['tenants']['CERN'])
        self.assertEqual(data['tenants']['CERN']['GWID'], {})

        #GATEWAY TENANT
        self.assertEqual(data['tenants']['GATEWAY']['routers'][TDG.ROUTER_1_LABEL], 777)
        self.assertEqual(data['tenants']['GATEWAY']['routers'][TDG.ROUTER_3_ID], 999)

        self.assertTrue('gateways' in data['tenants']['GATEWAY'])
        self.assertFalse('GWID' in data['tenants']['GATEWAY'])

        self.assertEqual(data['tenants']['GATEWAY']['gateways'][TDG.GATEWAY_1_LABEL], 444)
        self.assertEqual(data['tenants']['GATEWAY']['gateways'][TDG.GATEWAY_3_ID], 666)

        #TOTAL TENANT
        self.assertEqual(data['tenants']['total']['routers'][TDG.ROUTER_1_LABEL], 1313)
        self.assertEqual(data['tenants']['total']['routers'][TDG.ROUTER_3_ID], 1515)

        self.assertTrue('gateways' in data['tenants']['total'])
        self.assertFalse('GWID' in data['tenants']['total'])

        self.assertEqual(data['tenants']['total']['gateways'][TDG.GATEWAY_1_LABEL], 1010)
        self.assertEqual(data['tenants']['total']['gateways'][TDG.GATEWAY_3_ID], 1212)
    
    def validateDataStructureForTenantData(self, tenant_data):
            self.doCompareDictionariesStructure(self.tenant_data_template, tenant_data)
