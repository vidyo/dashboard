"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from vidyomonit.app.tests.api.VidyomonitApiTestCase import VidyomonitApiTestCase
from vidyomonit.app.tests.tools.test_data_generator import TestDataGenerator as TDG
import json
import datetime


class ApiConnectionsDailyTestCase(VidyomonitApiTestCase):

    tenant_data_template = {
         "connections": {
            "app_os": {},
            "app_version": {},
            "connection_type": {
              "conference": {},
              "p2p": {}
            },
            "endpoint_type": {},
            "total": {}
          },
          "max_sim_connections": {},
          "minutes": {}
    }

    def get_get_response(self, from_param='', until_param='', tenant_param=''):
        return self.get_api_get_response('/api/connections/daily', from_param, until_param, tenant_param)

    def testDailyWithoutAnyParameter(self):
        res = self.get_get_response()
        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 2)
        self.doTestResponseFrom2DaysAgo(jsoned_res[0])
        self.doTestResponseFrom1HourAgo(jsoned_res[1])

    def testDailyWithFromParameter(self):
        date_two_days_and_hour_ago = datetime.datetime.utcnow() - datetime.timedelta(days=2, hours=2)
        from_param_encoded = self.localize_and_encdode_date(date_two_days_and_hour_ago)

        res = self.get_get_response(from_param=from_param_encoded)

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 2)
        self.doTestResponseFrom2DaysAgo(jsoned_res[0])
        self.doTestResponseFrom1HourAgo(jsoned_res[1])

    def testDailyWithInvalidFromParameter(self):
        res = self.get_get_response(from_param='invalid')

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 2)
        self.doTestResponseFrom2DaysAgo(jsoned_res[0])
        self.doTestResponseFrom1HourAgo(jsoned_res[1])

    def testDailyWithUntilParameter(self):
        date_two_days_and_hour_ago = datetime.datetime.utcnow() - datetime.timedelta(days=2, hours=2)
        from_param_encoded = self.localize_and_encdode_date(date_two_days_and_hour_ago)

        date_two_hours_ago = datetime.datetime.utcnow() - datetime.timedelta(hours=2)
        to_param_encoded = self.localize_and_encdode_date(date_two_hours_ago)

        res = self.get_get_response(from_param=from_param_encoded, until_param=to_param_encoded)

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 1)
        self.doTestResponseFrom2DaysAgo(jsoned_res[0])

    def testDailyWithInvalidUntilParameter(self):
        date_two_days_and_hour_ago = datetime.datetime.utcnow() - datetime.timedelta(days=2, hours=2)
        from_param_encoded = self.localize_and_encdode_date(date_two_days_and_hour_ago)

        res = self.get_get_response(from_param=from_param_encoded, until_param='invalid')

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 2)
        self.doTestResponseFrom2DaysAgo(jsoned_res[0])
        self.doTestResponseFrom1HourAgo(jsoned_res[1])

    def testDailyWithCERNTenantParameter(self):
        res = self.get_get_response(tenant_param='CERN')

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']
        self.assertEqual(len(jsoned_res), 2)
        self.doTestResponseFrom2DaysAgo(jsoned_res[0], tenant_constrain_name='CERN')
        self.doTestResponseFrom1HourAgo(jsoned_res[1], tenant_constrain_name='CERN')
        self.doTestTenantExistence(jsoned_res[0], tenant='CERN')

    def testDailyWithInvalidTenantParameter(self):
        res = self.get_get_response(tenant_param='invalid')

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']
        self.assertEqual(len(jsoned_res), 0)

    def testDailyWithGATEWAYTenantParameter(self):
        res = self.get_get_response(tenant_param='GATEWAY')

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']
        self.assertEqual(len(jsoned_res), 2)
        self.doTestResponseFrom2DaysAgo(jsoned_res[0], tenant_constrain_name='GATEWAY')
        self.doTestResponseFrom1HourAgo(jsoned_res[1], tenant_constrain_name='GATEWAY')
        self.doTestTenantExistence(jsoned_res[0], tenant='GATEWAY')

    def testDailyUsingTimeRangeWithNoSnapshot(self):
        date_three_days_and_hour_ago = datetime.datetime.utcnow() - datetime.timedelta(days=3)
        from_param_encoded = self.localize_and_encdode_date(date_three_days_and_hour_ago)

        date_two_days_and_hour_ago = datetime.datetime.utcnow() - datetime.timedelta(days=2, hours=2)
        to_param_encoded = self.localize_and_encdode_date(date_two_days_and_hour_ago)

        res = self.get_get_response(from_param=from_param_encoded, until_param=to_param_encoded)

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']
        self.assertEqual(len(jsoned_res), 0)

    def testDailyUsingFutureFromParameter(self):
        date_tommorow = datetime.datetime.utcnow() + datetime.timedelta(days=1)
        from_param_encoded = self.localize_and_encdode_date(date_tommorow)

        res = self.get_get_response(from_param=from_param_encoded)

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']
        self.assertEqual(len(jsoned_res), 0)

    def testDailyUsingConnectionsGrouping(self):
        date_three_hours_ago = datetime.datetime.utcnow() - datetime.timedelta(hours=3)
        from_param_encoded = self.localize_and_encdode_date(date_three_hours_ago)
        res = self.get_get_response(from_param=from_param_encoded)
        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']
        self.assertEqual(len(jsoned_res), 1)
        jsoned_res_str = json.dumps(jsoned_res)

        self.assertTrue('Operating System 3' in jsoned_res_str)
        self.assertEqual(jsoned_res[0]['tenants']['CERN']['connections']['app_os']['Operating System 3'], 4)
        self.assertTrue('Operating System Group 1' in jsoned_res_str)
        self.assertEqual(jsoned_res[0]['tenants']['CERN']['connections']['app_os']['Operating System Group 1'], 3)
        self.assertFalse('Operating System 2' in jsoned_res_str)
        self.assertFalse('Operating System 1' in jsoned_res_str)

        self.assertTrue('endpoint3' in jsoned_res_str)
        self.assertEqual(jsoned_res[0]['tenants']['CERN']['connections']['endpoint_type']['endpoint3'], 6)
        self.assertTrue('Endpoint Group 1' in jsoned_res_str)
        self.assertEqual(jsoned_res[0]['tenants']['CERN']['connections']['endpoint_type']['Endpoint Group 1'], 3)
        self.assertFalse('endpoint1' in jsoned_res_str)
        self.assertFalse('endpoint2' in jsoned_res_str)

        self.assertTrue('appversion3' in jsoned_res_str)
        self.assertEqual(jsoned_res[0]['tenants']['CERN']['connections']['app_version']['appversion3'], 5)
        self.assertTrue('App Version Group 1' in jsoned_res_str)
        self.assertEqual(jsoned_res[0]['tenants']['CERN']['connections']['app_version']['App Version Group 1'], 3)
        self.assertFalse('appversion2' in jsoned_res_str)
        self.assertFalse('appversion1' in jsoned_res_str)


    def doTestResponseFrom1HourAgo(self, data, tenant_constrain_name=''):
        if not tenant_constrain_name or tenant_constrain_name == 'CERN':
            #CERN TENANT
            self.assertEqual(data['tenants']['CERN']['connections']['total'], 1)
            self.assertEqual(data['tenants']['CERN']['max_sim_connections'], 2)
            self.assertEqual(data['tenants']['CERN']['minutes'], 3)

        if not tenant_constrain_name or tenant_constrain_name == 'GATEWAY':
            #GATEWAY TENANT
            self.assertEqual(data['tenants']['GATEWAY']['connections']['total'], 4)
            self.assertEqual(data['tenants']['GATEWAY']['max_sim_connections'], 5)
            self.assertEqual(data['tenants']['GATEWAY']['minutes'], 6)

        if not tenant_constrain_name or tenant_constrain_name == 'total':
            #total TENANT
            self.assertEqual(data['tenants']['total']['connections']['total'], 7)
            self.assertEqual(data['tenants']['total']['max_sim_connections'], 8)
            self.assertEqual(data['tenants']['total']['minutes'], 9)

    def doTestResponseFrom2DaysAgo(self, data, tenant_constrain_name=''):
        if not tenant_constrain_name or tenant_constrain_name == 'CERN':
            #CERN TENANT
            self.assertEqual(data['tenants']['CERN']['connections']['total'], 10)
            self.assertEqual(data['tenants']['CERN']['max_sim_connections'], 11)
            self.assertEqual(data['tenants']['CERN']['minutes'], 12)

        if not tenant_constrain_name or tenant_constrain_name == 'GATEWAY':
            #GATEWAY TENANT
            self.assertEqual(data['tenants']['GATEWAY']['connections']['total'], 13)
            self.assertEqual(data['tenants']['GATEWAY']['max_sim_connections'], 14)
            self.assertEqual(data['tenants']['GATEWAY']['minutes'], 15)

        if not tenant_constrain_name or tenant_constrain_name == 'total':
            #total TENANT
            self.assertEqual(data['tenants']['total']['connections']['total'], 19)
            self.assertEqual(data['tenants']['total']['max_sim_connections'], 20)
            self.assertEqual(data['tenants']['total']['minutes'], 21)

    def validateDataStructureForTenantData(self, tenant_data):
            self.doCompareDictionariesStructure(self.tenant_data_template, tenant_data)

    def doTestTenantExistence(self, data, tenant=''):
        if tenant:
            self.assertTrue(tenant in data['tenants'])
            self.assertEqual(len(data['tenants']), 1)