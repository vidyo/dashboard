"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from vidyomonit.app.tests.api.VidyomonitApiTestCase import VidyomonitApiTestCase
import json

class ApiDevicesGetDeviceInfoTestCase(VidyomonitApiTestCase):

    def get_get_response(self, hostname=''):
        return self.get_api_get_response('/api/devices/info/'+hostname)

    def testGetDeviceInfoWithNoParameter(self):
        res = self.get_get_response()
        self.assertEqual(res._status_code, 404)

    def testGetDeviceInfoWithValidHostnameAndExistingRouterDeviceParameter(self):
        res = self.get_get_response('vidyorouter1.cern.ch')
        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)
        self.assertEqual(jsoned_res['id'], 'Device3')
        self.assertEqual(jsoned_res['type'], 'router')
        self.assertEqual(jsoned_res['ip_address'], '137.138.248.229')

    def testGetDeviceInfoWithValidHostnameAndExistingGatewayDeviceParameter(self):
        res = self.get_get_response('vidyogw1.cern.ch')
        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)
        self.assertEqual(jsoned_res['id'], 'Device1')
        self.assertEqual(jsoned_res['type'], 'gateway')
        self.assertEqual(jsoned_res['ip_address'], '137.138.248.228')

    def testGetDeviceInfoWithValidHostnameAndExistingPortalDeviceParameter(self):
        res = self.get_get_response('vidyoportal.cern.ch')
        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)
        self.assertEqual(jsoned_res['id'], 'Device2')
        self.assertEqual(jsoned_res['type'], 'portal')
        self.assertEqual(jsoned_res['ip_address'], '137.138.248.230')

    def testGetDeviceInfoWithValidHostnameAndNotExistingDeviceParameter(self):
        res = self.get_get_response('cern.ch')
        self.assertEqual(res._status_code, 404)
        jsoned_res = json.loads(res.data)
        self.assertEqual(jsoned_res['error'], 'No device found in CDR')

    def testGetDeviceInfoWithInvalidParameter(self):
        res = self.get_get_response('invalid')
        self.assertEqual(res._status_code, 404)
        jsoned_res = json.loads(res.data)
        self.assertEqual(jsoned_res['error'], "Couldn't match hostname to IP")



