"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from vidyomonit.app.tests.api.VidyomonitApiTestCase import VidyomonitApiTestCase
import json
import datetime


class ApiInstallsSnapshotTestCase(VidyomonitApiTestCase):

    tenant_data_template= {
         "devices": {
            "today": {},
            "total": {}
          },
          "guests": {
            "today": {},
            "total": {}
          },
          "total": {
            "today": {},
            "total": {}
          },
          "users": {
            "today": {},
            "total": {}
          }
    }

    def get_get_response(self, from_param='', until_param='', tenant_param=''):
        return self.get_api_get_response('/api/installs/daily', from_param, until_param, tenant_param)

    def testSnapshotWithNoParameters(self):
        res = self.get_get_response()
        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 1)
        self.doTestingOnResponseFromTwoDaysAgo(jsoned_res[0])

    def testSnapshotWithValidFromParameterParameters(self):
        date_thirty_three_days_ago = datetime.datetime.utcnow() - datetime.timedelta(days=33)
        from_param_encoded = self.localize_and_encdode_date(date_thirty_three_days_ago)

        res = self.get_get_response(from_param=from_param_encoded)

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 2)
        self.doTestingOnResponseFrom32DaysAgo(jsoned_res[0])
        self.doTestingOnResponseFromTwoDaysAgo(jsoned_res[1])


    def testSnapshotWithValidUntilParameterParameters(self):
        date_33_days_ago = datetime.datetime.utcnow() - datetime.timedelta(days=33)
        from_param_encoded = self.localize_and_encdode_date(date_33_days_ago)

        date_29_days_ago = datetime.datetime.utcnow() - datetime.timedelta(days=29)
        until_param_encoded = self.localize_and_encdode_date(date_29_days_ago)

        res = self.get_get_response(from_param=from_param_encoded, until_param=until_param_encoded)

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 1)
        self.doTestingOnResponseFrom32DaysAgo(jsoned_res[0])

    def testSnapshotWithInvalidFromParameterParameters(self):
        res = self.get_get_response(from_param='invalid')

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 1)
        self.doTestingOnResponseFromTwoDaysAgo(jsoned_res[0])


    def testSnapshotWithInvalidToParameterParameters(self):
        res = self.get_get_response(until_param='invalid')

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 1)
        self.doTestingOnResponseFromTwoDaysAgo(jsoned_res[0])

    def testSnapshotWithCERNTenantParameter(self):
        res = self.get_get_response(tenant_param='CERN')
        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 1)
        self.doTestingOnResponseFromTwoDaysAgo(jsoned_res[0], tenant='CERN')

    def testSnapshotWithInvalidTenantParameter(self):
        res = self.get_get_response(tenant_param='invalid')
        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 0)

    def testSnapshotWithRangeWithNotInstalls(self):
        date_29_days_ago = datetime.datetime.utcnow() - datetime.timedelta(days=29)
        from_param_encoded = self.localize_and_encdode_date(date_29_days_ago)

        date_20_days_ago = datetime.datetime.utcnow() - datetime.timedelta(days=20)
        until_param_encoded = self.localize_and_encdode_date(date_20_days_ago)

        res = self.get_get_response(from_param=from_param_encoded, until_param=until_param_encoded)

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 0)

    def doTestingOnResponseFromTwoDaysAgo(self, data, tenant=''):
        self.assertTrue('tenants' in data)
        if tenant:
            self.assertEqual(len(data['tenants']), 1)
            self.assertTrue(tenant in data['tenants'])

        if not tenant or tenant == 'CERN':
            self.assertTrue('CERN' in data['tenants'])

            self.validateDataStructureForTenantData(data['tenants']['CERN'])

            self.assertEqual(data['tenants']['CERN']['devices']['today'], 1)
            self.assertEqual(data['tenants']['CERN']['devices']['total'], 2)

        if not tenant or tenant == 'total':
            self.assertTrue('total' in data['tenants'])

            self.validateDataStructureForTenantData(data['tenants']['total'])

            self.assertEqual(data['tenants']['total']['devices']['today'], 9)
            self.assertEqual(data['tenants']['total']['devices']['total'], 10)


    def doTestingOnResponseFrom32DaysAgo(self, data, tenant=''):
        self.assertTrue('tenants' in data)
        if not tenant or tenant == 'CERN':
            self.assertTrue('CERN' in data['tenants'])

            self.validateDataStructureForTenantData(data['tenants']['CERN'])

            self.assertEqual(data['tenants']['CERN']['devices']['today'], 17)
            self.assertEqual(data['tenants']['CERN']['devices']['total'], 18)

        if not tenant or tenant == 'total':
            self.assertTrue('total' in data['tenants'])

            self.validateDataStructureForTenantData(data['tenants']['total'])

            self.assertEqual(data['tenants']['total']['devices']['today'], 25)
            self.assertEqual(data['tenants']['total']['devices']['total'], 26)

    def validateDataStructureForTenantData(self, tenant_data):
        self.doCompareDictionariesStructure(self.tenant_data_template, tenant_data)


