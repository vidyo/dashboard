"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from vidyomonit.app.tests.api.VidyomonitApiTestCase import VidyomonitApiTestCase
import json
import datetime


class ApiRegistrationsDailyTestCase(VidyomonitApiTestCase):

    tenant_data_template = {
        'today': {
            'total': {}
        },
        'total': {
            'total': {}
        }
    }

    def get_get_response(self, from_param='', until_param='', tenant_param=''):
        return self.get_api_get_response('/api/registrations/daily', from_param, until_param, tenant_param)

    def testWithNoParameters(self):
        res = self.get_get_response()
        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 1)
        self.doTestResponseFrom2DaysAgo(jsoned_res[0])

    def testWithValidFromParameterParameters(self):
        date_thirty_three_days_ago = datetime.datetime.utcnow() - datetime.timedelta(days=33)
        from_param_encoded = self.localize_and_encdode_date(date_thirty_three_days_ago)

        res = self.get_get_response(from_param=from_param_encoded)

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 2)
        self.doTestResponseFrom32DaysAgo(jsoned_res[0])
        self.doTestResponseFrom2DaysAgo(jsoned_res[1])
    #
    def testWithValidUntilParameterParameters(self):
        date_33_days_ago = datetime.datetime.utcnow() - datetime.timedelta(days=33)
        from_param_encoded = self.localize_and_encdode_date(date_33_days_ago)

        date_29_days_ago = datetime.datetime.utcnow() - datetime.timedelta(days=29)
        until_param_encoded = self.localize_and_encdode_date(date_29_days_ago)

        res = self.get_get_response(from_param=from_param_encoded, until_param=until_param_encoded)

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 1)
        self.doTestResponseFrom32DaysAgo(jsoned_res[0])
    #
    def testWithInvalidFromParameterParameters(self):
        res = self.get_get_response(from_param='invalid')

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 1)
        self.doTestResponseFrom2DaysAgo(jsoned_res[0])
    #
    def testWithInvalidUntilParameterParameters(self):
        date_33_days_ago = datetime.datetime.utcnow() - datetime.timedelta(days=33)
        from_param_encoded = self.localize_and_encdode_date(date_33_days_ago)

        res = self.get_get_response(from_param=from_param_encoded, until_param='invalid')

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 2)
        self.doTestResponseFrom32DaysAgo(jsoned_res[0])
        self.doTestResponseFrom2DaysAgo(jsoned_res[1])

    def testWithCERNTenantParameter(self):
        res = self.get_get_response(tenant_param='CERN')
        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 1)
        self.doTestResponseFrom2DaysAgo(jsoned_res[0], tenant='CERN')

    def testWithInvalidTenantParameter(self):
        res = self.get_get_response(tenant_param='invalid')
        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']

        self.assertEqual(len(jsoned_res), 0)

    def testWithRangeWithNotInstalls(self):
        date_29_days_ago = datetime.datetime.utcnow() - datetime.timedelta(days=29)
        from_param_encoded = self.localize_and_encdode_date(date_29_days_ago)

        date_20_days_ago = datetime.datetime.utcnow() - datetime.timedelta(days=20)
        until_param_encoded = self.localize_and_encdode_date(date_20_days_ago)

        res = self.get_get_response(from_param=from_param_encoded, until_param=until_param_encoded)

        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']
        self.assertEqual(len(jsoned_res), 0)

    def doTestResponseFrom2DaysAgo(self, data, tenant=''):
        self.assertTrue('tenants' in data)
        if tenant:
            self.assertEqual(len(data['tenants']), 1)
            self.assertTrue(tenant in data['tenants'])

        if not tenant or tenant == 'CERN':
            self.assertTrue('CERN' in data['tenants'])
            self.validateDataStructureForTenantData(data['tenants']['CERN'])
            self.assertEqual(data['tenants']['CERN']['today']['normal']['IT'], 1)
            self.assertEqual(data['tenants']['CERN']['total']['admin']['Support'], 5)

        if not tenant or tenant == 'total':
            self.assertTrue('total' in data['tenants'])
            self.validateDataStructureForTenantData(data['tenants']['total'])
            self.assertEqual(data['tenants']['total']['today']['total']['IT'], 14)
    #
    def doTestResponseFrom32DaysAgo(self, data, tenant=''):
        self.assertTrue('tenants' in data)
        if tenant:
            self.assertEqual(len(data['tenants']), 1)
            self.assertTrue(tenant in data['tenants'])

        if not tenant or tenant == 'CERN':
            self.assertTrue('CERN' in data['tenants'])
            self.validateDataStructureForTenantData(data['tenants']['CERN'])
            self.assertEqual(data['tenants']['CERN']['today']['admin']['Support'], 11)
            self.assertEqual(data['tenants']['CERN']['total']['admin']['Support'], 15)

        if not tenant or tenant == 'total':
            self.assertTrue('total' in data['tenants'])
            self.validateDataStructureForTenantData(data['tenants']['total'])
            self.assertEqual(data['tenants']['total']['today']['total']['Support'], 21)

    def validateDataStructureForTenantData(self, tenant_data):
            self.doCompareDictionariesStructure(self.tenant_data_template, tenant_data)
