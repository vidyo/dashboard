"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from vidyomonit.app.utils import utc, utc_today
import datetime
import pytz
import json


class TestDataGenerator():
    ROUTER_1_ID = 'Router1Id'
    ROUTER_1_LABEL = 'Router1Label'
    ROUTER_1_HOSTNAME = 'Router1Hostname'
    ROUTER_1_IP = '111.111.111.111'
    ROUTER_2_ID = 'Router2Id'
    ROUTER_2_LABEL = 'Router2Label'
    ROUTER_2_HOSTNAME = 'Router2Hostname'
    ROUTER_3_ID = 'Router3Id'
    ROUTER_3_LABEL = 'Router3Label'
    ROUTER_3_HOSTNAME = 'Router3Hostname'

    GATEWAY_1_ID = 'Gateway1Id'
    GATEWAY_1_LABEL = 'Gateway1Label'
    GATEWAY_1_HOSTNAME = 'Gateway1Hostname'
    GATEWAY_2_ID = 'Gateway2Id'
    GATEWAY_2_HOSTNAME = 'Gateway2Hostname'
    GATEWAY_2_LABEL = 'Gateway2Label'
    GATEWAY_3_ID = 'Gateway3Id'

    TENANT_CERN = 'CERN'
    TENANT_GATEWAY = 'GATEWAY'
    TENANT_TOTAL = 'total'
    TENANT_SWITCH = 'SWITCH'

    @staticmethod
    def getDateHourAgo():
        date_hour_ago = datetime.datetime.utcnow() - datetime.timedelta(hours=1)
        return pytz.utc.localize(date_hour_ago)

    @staticmethod
    def getDateTwoDaysAgo():
        date_two_days_ago = datetime.datetime.utcnow() - datetime.timedelta(hours=47)
        return pytz.utc.localize(date_two_days_ago)

    @staticmethod
    def getDate32DaysAgo():
        date_two_days_ago = datetime.datetime.utcnow() - datetime.timedelta(days=32)
        return pytz.utc.localize(date_two_days_ago)

    @staticmethod
    def getDateFirstOfCurrentMonthTwoYearsAgoAgo():
        today = utc_today()
        return utc(today.year - 2, today.month, 1)

    @staticmethod
    def generateDataForSnapshotTable():
        data = [
            {
                "id": TestDataGenerator.getDateHourAgo(),
                "tenants": {
                    TestDataGenerator.TENANT_CERN: {
                        "ConferenceType": {
                            "C": 1
                        },
                        "EndpointType": {
                            "D": 1,
                            "G": 1,
                            "R": 1
                        },
                        "GWID": {},
                        "RouterID": {
                            TestDataGenerator.ROUTER_1_ID: 1,
                            TestDataGenerator.ROUTER_2_ID: 2,
                            TestDataGenerator.ROUTER_3_ID: 3,
                        },
                        "total": 10
                    },
                    TestDataGenerator.TENANT_GATEWAY: {
                        "ConferenceType": {
                            "C": 2
                        },
                        "EndpointType": {
                            "L": 3
                        },
                        "GWID": {
                            TestDataGenerator.GATEWAY_1_ID: 4,
                            TestDataGenerator.GATEWAY_2_ID: 5,
                            TestDataGenerator.GATEWAY_3_ID: 6
                        },
                        "RouterID": {
                            TestDataGenerator.ROUTER_1_ID: 4,
                            TestDataGenerator.ROUTER_2_ID: 5,
                            TestDataGenerator.ROUTER_3_ID: 6,
                        },
                        "total": 26
                    },
                    TestDataGenerator.TENANT_TOTAL: {
                        "ConferenceType": {
                            "C": 465
                        },
                        "EndpointType": {
                            "D": 215,
                            "G": 214,
                            "L": 34,
                            "R": 2
                        },
                        "GWID": {
                            TestDataGenerator.GATEWAY_1_ID: 7,
                            TestDataGenerator.GATEWAY_2_ID: 8,
                            TestDataGenerator.GATEWAY_3_ID: 9
                        },
                        "RouterID": {
                            TestDataGenerator.ROUTER_1_ID: 10,
                            TestDataGenerator.ROUTER_2_ID: 11,
                            TestDataGenerator.ROUTER_3_ID: 12,
                        },
                        "total": 465
                    }
                }
            },

            {
                "id": TestDataGenerator.getDateTwoDaysAgo(),
                "tenants": {
                    TestDataGenerator.TENANT_CERN: {
                        "ConferenceType": {
                            "C": 1
                        },
                        "EndpointType": {
                            "D": 1,
                            "G": 1,
                            "R": 1
                        },
                        "GWID": {},
                        "RouterID": {
                            TestDataGenerator.ROUTER_1_ID: 111,
                            TestDataGenerator.ROUTER_2_ID: 222,
                            TestDataGenerator.ROUTER_3_ID: 333,
                        },
                        "total": 10
                    },
                    TestDataGenerator.TENANT_GATEWAY: {
                        "ConferenceType": {
                            "C": 2
                        },
                        "EndpointType": {
                            "L": 3
                        },
                        "GWID": {
                            TestDataGenerator.GATEWAY_1_ID: 444,
                            TestDataGenerator.GATEWAY_2_ID: 555,
                            TestDataGenerator.GATEWAY_3_ID: 666
                        },
                        "RouterID": {
                            TestDataGenerator.ROUTER_1_ID: 777,
                            TestDataGenerator.ROUTER_2_ID: 888,
                            TestDataGenerator.ROUTER_3_ID: 999,
                        },
                        "total": 26
                    },
                    TestDataGenerator.TENANT_TOTAL: {
                        "ConferenceType": {
                            "C": 465
                        },
                        "EndpointType": {
                            "D": 215,
                            "G": 214,
                            "L": 34,
                            "R": 2
                        },
                        "GWID": {
                            TestDataGenerator.GATEWAY_1_ID: 1010,
                            TestDataGenerator.GATEWAY_2_ID: 1111,
                            TestDataGenerator.GATEWAY_3_ID: 1212
                        },
                        "RouterID": {
                            TestDataGenerator.ROUTER_1_ID: 1313,
                            TestDataGenerator.ROUTER_2_ID: 1414,
                            TestDataGenerator.ROUTER_3_ID: 1515,
                        },
                        "total": 465
                    }
                }
            },
            {
                "id": TestDataGenerator.getDate32DaysAgo(),
                "tenants": {}
            }
        ]

        return data

    @staticmethod
    def generateDataForDevicesTable():
        data = [
            {
                'id': TestDataGenerator.ROUTER_1_ID,
                'label': TestDataGenerator.ROUTER_1_LABEL,
                'ip_address': TestDataGenerator.ROUTER_1_IP,
                'type': 'router',
                'hostname': TestDataGenerator.ROUTER_1_HOSTNAME
            },
            {
                'id': TestDataGenerator.ROUTER_2_ID,
                'label': TestDataGenerator.ROUTER_2_LABEL,
                'ip_address': '222.222.222.222',
                'type': 'router',
                'hostname': TestDataGenerator.ROUTER_2_HOSTNAME
            },
            {
                'id': TestDataGenerator.GATEWAY_1_ID,
                'label': TestDataGenerator.GATEWAY_1_LABEL,
                'ip_address': '121.121.121.121',
                'type': 'gateway',
                'hostname': TestDataGenerator.GATEWAY_1_HOSTNAME
            },
            {
                'id': TestDataGenerator.GATEWAY_2_ID,
                'label': TestDataGenerator.GATEWAY_2_LABEL,
                'ip_address': '212.212.212.212',
                'type': 'router',
                'hostname': TestDataGenerator.GATEWAY_2_HOSTNAME
            }
        ]
        return data

    @staticmethod
    def generateDataForConnectionsPerDay():
        data = [
            {
                "id": TestDataGenerator.getDateHourAgo(),
                "tenants": {
                    TestDataGenerator.TENANT_CERN: {
                        "connections": {
                            "app_os": {
                                'Operating System 1': 1,
                                'Operating System 2': 2,
                                'Operating System 3': 4,
                            },
                            "app_version": {
                                'appversion1': 1,
                                'appversion2': 2,
                                'appversion3': 5
                            },
                            "connection_type": {},
                            "endpoint_type": {
                                'endpoint1': 1,
                                'endpoint2': 2,
                                'endpoint3': 6
                            },
                            "total": 1
                        },
                        "max_sim_connections": 2,
                        "minutes": 3
                    },
                    TestDataGenerator.TENANT_GATEWAY: {
                        "connections": {
                            "app_os": {},
                            "app_version": {},
                            "connection_type": {},
                            "endpoint_type": {
                            },
                            "total": 4
                        },
                        "max_sim_connections": 5,
                        "minutes": 6
                    },
                    TestDataGenerator.TENANT_TOTAL: {
                        "connections": {
                            "app_os": {},
                            "app_version": {},
                            "connection_type": {},
                            "endpoint_type": {},
                            "total": 7
                        },
                        "max_sim_connections": 8,
                        "minutes": 9
                    }
                }
            },
            {
                "id": TestDataGenerator.getDateTwoDaysAgo(),
                "tenants": {
                    TestDataGenerator.TENANT_CERN: {
                        "connections": {
                            "app_os": {},
                            "app_version": {},
                            "connection_type": {},
                            "endpoint_type": {},
                            "total": 10
                        },
                        "max_sim_connections": 11,
                        "minutes": 12
                    },
                    TestDataGenerator.TENANT_GATEWAY: {
                        "connections": {
                            "app_os": {},
                            "app_version": {},
                            "connection_type": {},
                            "endpoint_type": {},
                            "total": 13
                        },
                        "max_sim_connections": 14,
                        "minutes": 15
                    },
                    TestDataGenerator.TENANT_SWITCH: {
                        "connections": {
                            "app_os": {},
                            "app_version": {},
                            "connection_type": {},
                            "endpoint_type": {},
                            "total": 16
                        },
                        "max_sim_connections": 17,
                        "minutes": 18
                    },
                    TestDataGenerator.TENANT_TOTAL: {
                        "connections": {
                            "app_os": {},
                            "app_version": {},
                            "connection_type": {},
                            "endpoint_type": {},
                            "total": 19
                        },
                        "max_sim_connections": 20,
                        "minutes": 21
                    }
                }
            }
        ]
        return data

    @staticmethod
    def generateDataForActiveConnections():
        data = [
            {
                'id': '1',
                'updated': TestDataGenerator.getDateTwoDaysAgo(),
                'tenants': {

                }
            },
            {
                "id": 0,
                'updated': TestDataGenerator.getDateHourAgo(),
                "tenants": {
                    TestDataGenerator.TENANT_CERN: [
                        {
                            "CallID": 1,
                            "CallerID": "caller1Id",
                            "CallerName": "caller1Name",
                            "ConferenceName": "conference1name",
                            "EndpointType": "D",
                            "GWID": None,
                            "JoinTime": "Mon, 23 Nov 2015 09:39:31 GMT",
                            "RouterID": TestDataGenerator.ROUTER_1_ID,
                            "TenantName": "CERN",
                            "UniqueCallID": "6297828041266738"
                        },
                        {
                            "CallID": 2,
                            "CallerID": "caller2Id",
                            "CallerName": "caller2Name",
                            "ConferenceName": "conference2name",
                            "EndpointType": "D",
                            "GWID": None,
                            "JoinTime": "Mon, 23 Nov 2015 09:39:31 GMT",
                            "RouterID": TestDataGenerator.ROUTER_2_ID,
                            "TenantName": "CERN",
                            "UniqueCallID": "6297828041266738"
                        },
                        {
                            "CallID": 3,
                            "CallerID": "caller3Id",
                            "CallerName": "caller3Name",
                            "ConferenceName": "conference3name",
                            "EndpointType": "D",
                            "GWID": None,
                            "JoinTime": "Mon, 23 Nov 2015 09:39:31 GMT",
                            "RouterID": TestDataGenerator.ROUTER_3_ID,
                            "TenantName": "CERN",
                            "UniqueCallID": "6297828041266738"
                        },
                    ],
                    TestDataGenerator.TENANT_GATEWAY: [
                        {
                            "CallID": 4,
                            "CallerID": "caller4Id",
                            "CallerName": "caller4Name",
                            "ConferenceName": "conference4name",
                            "EndpointType": "D",
                            "GWID": TestDataGenerator.GATEWAY_1_ID,
                            "JoinTime": "Mon, 23 Nov 2015 09:39:31 GMT",
                            "RouterID": TestDataGenerator.ROUTER_1_ID,
                            "TenantName": "GATEWAY",
                            "UniqueCallID": "6297828041266738"
                        },
                        {
                            "CallID": 5,
                            "CallerID": "caller5Id",
                            "CallerName": "caller5Name",
                            "ConferenceName": "conference4name",
                            "EndpointType": "D",
                            "GWID": TestDataGenerator.GATEWAY_2_ID,
                            "JoinTime": "Mon, 23 Nov 2015 09:39:31 GMT",
                            "RouterID": TestDataGenerator.ROUTER_2_ID,
                            "TenantName": "GATEWAY",
                            "UniqueCallID": "6297828041266738"
                        },
                        {
                            "CallID": 6,
                            "CallerID": "caller4Id",
                            "CallerName": "caller4Name",
                            "ConferenceName": "conference4name",
                            "EndpointType": "D",
                            "GWID": TestDataGenerator.GATEWAY_3_ID,
                            "JoinTime": "Mon, 23 Nov 2015 09:39:31 GMT",
                            "RouterID": TestDataGenerator.ROUTER_3_ID,
                            "TenantName": "GATEWAY",
                            "UniqueCallID": "6297828041266738"
                        }
                    ],
                    TestDataGenerator.TENANT_TOTAL: [
                        {
                            "CallID": 7,
                            "CallerID": "caller4Id",
                            "CallerName": "caller4Name",
                            "ConferenceName": "conference4name",
                            "EndpointType": "D",
                            "GWID": None,
                            "JoinTime": "Mon, 23 Nov 2015 09:39:31 GMT",
                            "RouterID": TestDataGenerator.ROUTER_1_ID,
                            "TenantName": "CERN",
                            "UniqueCallID": "6297828041266738"
                        },
                        {
                            "CallID": 8,
                            "CallerID": "caller5Id",
                            "CallerName": "caller5Name",
                            "ConferenceName": "conference4name",
                            "EndpointType": "D",
                            "GWID": None,
                            "JoinTime": "Mon, 23 Nov 2015 09:39:31 GMT",
                            "RouterID": TestDataGenerator.ROUTER_2_ID,
                            "TenantName": "CERN",
                            "UniqueCallID": "6297828041266738"
                        },
                        {
                            "CallID": 9,
                            "CallerID": "caller4Id",
                            "CallerName": "caller4Name",
                            "ConferenceName": "conference4name",
                            "EndpointType": "D",
                            "GWID": None,
                            "JoinTime": "Mon, 23 Nov 2015 09:39:31 GMT",
                            "RouterID": TestDataGenerator.ROUTER_3_ID,
                            "TenantName": "CERN",
                            "UniqueCallID": "6297828041266738"
                        },
                        {
                            "CallID": 10,
                            "CallerID": "caller4Id",
                            "CallerName": "caller4Name",
                            "ConferenceName": "conference4name",
                            "EndpointType": "D",
                            "GWID": TestDataGenerator.GATEWAY_1_ID,
                            "JoinTime": "Mon, 23 Nov 2015 09:39:31 GMT",
                            "RouterID": TestDataGenerator.ROUTER_1_ID,
                            "TenantName": "GATEWAY",
                            "UniqueCallID": "6297828041266738"
                        },
                        {
                            "CallID": 11,
                            "CallerID": "caller5Id",
                            "CallerName": "caller5Name",
                            "ConferenceName": "conference4name",
                            "EndpointType": "D",
                            "GWID": TestDataGenerator.GATEWAY_2_ID,
                            "JoinTime": "Mon, 23 Nov 2015 09:39:31 GMT",
                            "RouterID": TestDataGenerator.ROUTER_2_ID,
                            "TenantName": "GATEWAY",
                            "UniqueCallID": "6297828041266738"
                        },
                        {
                            "CallID": 12,
                            "CallerID": "caller4Id",
                            "CallerName": "caller4Name",
                            "ConferenceName": "conference4name",
                            "EndpointType": "D",
                            "GWID": TestDataGenerator.GATEWAY_3_ID,
                            "JoinTime": "Mon, 23 Nov 2015 09:39:31 GMT",
                            "RouterID": TestDataGenerator.ROUTER_3_ID,
                            "TenantName": "GATEWAY",
                            "UniqueCallID": "6297828041266738"
                        }
                    ]
                }
            }
        ]
        return data

    @staticmethod
    def generateDataForInstallsPerDay():
        data = [
            {
                "id": TestDataGenerator.getDateTwoDaysAgo(),
                "tenants": {
                    "CERN": {
                        "devices": {
                            "today": 1,
                            "total": 2
                        },
                        "guests": {
                            "today": 3,
                            "total": 4
                        },
                        "total": {
                            "today": 5,
                            "total": 6
                        },
                        "users": {
                            "today": 7,
                            "total": 8
                        }
                    },
                    "total": {
                        "devices": {
                            "today": 9,
                            "total": 10,
                        },
                        "guests": {
                            "today": 11,
                            "total": 12
                        },
                        "total": {
                            "today": 13,
                            "total": 14
                        },
                        "users": {
                            "today": 15,
                            "total": 16
                        }
                    }
                }
            },
            {
                "id": TestDataGenerator.getDate32DaysAgo(),
                "tenants": {
                    "CERN": {
                        "devices": {
                            "today": 17,
                            "total": 18
                        },
                        "guests": {
                            "today": 19,
                            "total": 20
                        },
                        "total": {
                            "today": 21,
                            "total": 22
                        },
                        "users": {
                            "today": 23,
                            "total": 24
                        }
                    },
                    "total": {
                        "devices": {
                            "today": 25,
                            "total": 26
                        },
                        "guests": {
                            "today": 27,
                            "total": 28
                        },
                        "total": {
                            "today": 29,
                            "total": 30
                        },
                        "users": {
                            "today": 31,
                            "total": 32
                        }
                    }
                }
            }
        ]
        return data

    @staticmethod
    def generate_data_for_meetings_per_day():
        data = [
            {
                "id": TestDataGenerator.getDate32DaysAgo(),
                "tenants": {
                    TestDataGenerator.TENANT_CERN: {
                        "category": {
                            "Meetings": 1
                        },
                        "total": 2
                    },
                    TestDataGenerator.TENANT_TOTAL: {
                        "category": {
                            "Meetings": 3
                        },
                        "total": 4
                    }
                }
            },
            {
                "id": TestDataGenerator.getDateTwoDaysAgo(),
                "tenants": {
                    TestDataGenerator.TENANT_CERN: {
                        "category": {
                            "Meetings": 5
                        },
                        "total": 6
                    },
                    TestDataGenerator.TENANT_TOTAL: {
                        "category": {
                            "Meetings": 7
                        },
                        "total": 8
                    }
                }
            }
        ]
        return data

    @staticmethod
    def generate_data_for_registration_per_day():
        return [
            {
                "id": TestDataGenerator.getDate32DaysAgo(),
                "tenants": {
                    TestDataGenerator.TENANT_CERN: {
                        "today": {
                            "admin": {
                                "Support": 11,
                                "total": 12
                            },
                            "total": {
                                "Support": 13,
                                "total": 14
                            }
                        },
                        "total": {
                            "admin": {
                                "Support": 15,
                                "total": 16
                            },
                            "total": {
                                "Support": 17,
                                "total": 18
                            }
                        }
                    },
                    TestDataGenerator.TENANT_TOTAL: {
                        "today": {
                            "admin": {
                                "Support": 19,
                                "total": 20
                            },
                            "total": {
                                "Support": 21,
                                "total": 22
                            }
                        },
                        "total": {
                            "admin": {
                                "Support": 22,
                                "total": 23
                            },
                            "total": {
                                "Support": 24,
                                "total": 25
                            }
                        }
                    }
                }
            },
            {
                "id": TestDataGenerator.getDateTwoDaysAgo(),
                "tenants": {
                    TestDataGenerator.TENANT_CERN: {
                        "today": {
                            "normal": {
                                "IT": 1,
                                "total": 2
                            },
                            "total": {
                                "IT": 3,
                                "total": 4
                            }
                        },
                        "total": {
                            "admin": {
                                "Support": 5,
                                "total": 6
                            },
                            "normal": {
                                "IT": 7,
                                "total": 8
                            },
                            "total": {
                                "IT": 9,
                                "Support": 10,
                                "total": 11
                            }
                        }
                    },
                    TestDataGenerator.TENANT_TOTAL: {
                        "today": {
                            "normal": {
                                "IT": 12,
                                "total": 13
                            },
                            "total": {
                                "IT": 14,
                                "total": 15
                            }
                        },
                        "total": {
                            "admin": {
                                "Support": 15,
                                "total": 16
                            },
                            "normal": {
                                "IT": 17,
                                "total": 18
                            },
                            "total": {
                                "IT": 19,
                                "Support": 20,
                                "total": 21
                            }
                        }
                    }
                }
            }
        ]

    @staticmethod
    def generate_data_for_unique_users_per_month():
        return [
            {
                "id": TestDataGenerator.getDateFirstOfCurrentMonthTwoYearsAgoAgo(),
                "tenants": {
                    TestDataGenerator.TENANT_CERN: {
                        "guest": 1,
                        "h323": 2,
                        "phone": 3,
                        "user": 4
                    } ,
                    TestDataGenerator.TENANT_TOTAL: {
                        "guest": 5,
                        "h323": 6,
                        "phone": 7,
                        "user": 8
                    }
                }
            },
            {
                "id": TestDataGenerator.getDate32DaysAgo(),
                "tenants": {
                    TestDataGenerator.TENANT_CERN: {
                        "guest": 9,
                        "h323": 10,
                        "phone": 11,
                        "user": 12
                    },
                    TestDataGenerator.TENANT_TOTAL: {
                        "guest": 13,
                        "h323": 14,
                        "phone": 15,
                        "user": 16
                    }
                }
            }
        ]

    @staticmethod
    def generate_data_for_tenants():
        return [
            {
                "id": 1,
                "name":  TestDataGenerator.TENANT_CERN
            },
            {
                "id": 2,
                "name":  TestDataGenerator.TENANT_GATEWAY
            }
        ]

    @staticmethod
    def generate_data_for_connection_groups():
        return [
            {
                'id': 'connection_group_1',
                'name': 'Operating System Group 1',
                'type': 'app_os'
            },
            {
                'id': 'connection_group_2',
                'name': 'testName2',
                'type': 'app_os'
            },
            {
                'id': 'endpoint_group1',
                'name': 'Endpoint Group 1',
                'type': 'endpoint_type'
            },
            {
                'id': 'appversion_group1',
                'name': 'App Version Group 1',
                'type': 'app_version'
            }
        ]

    @staticmethod
    def generate_data_for_connection_groups_devices():
        return [
            {
                'connections_group': 'connection_group_1',
                'name': 'Operating System 1',
                'type': 'app_os'
            }, {
                'connections_group': 'connection_group_1',
                'name': 'Operating System 2',
                'type': 'app_os'
            },

            {
                'connections_group': 'endpoint_group1',
                'name': 'endpoint1',
                'type': 'endpoint_type'
            }, {
                'connections_group': 'endpoint_group1',
                'name': 'endpoint2',
                'type': 'endpoint_type'
            },

            {
                'connections_group': 'appversion_group1',
                'name': 'appversion1',
                'type': 'app_version'
            }, {
                'connections_group': 'appversion_group1',
                'name': 'appversion2',
                'type': 'app_version'
            }
        ]