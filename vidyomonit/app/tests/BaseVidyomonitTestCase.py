"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from flask.ext.testing import TestCase
from vidyomonit.app.app_factory import create_app
import rethinkdb as r
from vidyomonit import conf_test
from vidyomonit.app.tests.tools.test_data_generator import TestDataGenerator as TDG
import MySQLdb
import pytz
import urllib

#TODO standarize methods names camelcase or undercase
class BaseVidyomonitTestCase(TestCase):
    app = None
    connection = None
    config = None
    tables_and_method_for_data_generation = {
        'snapshot': TDG.generateDataForSnapshotTable,
        'devices': TDG.generateDataForDevicesTable,
        'connections_per_day': TDG.generateDataForConnectionsPerDay,
        'active_connections': TDG.generateDataForActiveConnections,
        'installs_per_day': TDG.generateDataForInstallsPerDay,
        'meetings_per_day': TDG.generate_data_for_meetings_per_day,
        'registrations_per_day': TDG.generate_data_for_registration_per_day,
        'unique_users_per_month': TDG.generate_data_for_unique_users_per_month,
        'tenants': TDG.generate_data_for_tenants,
        'connections_groups': TDG.generate_data_for_connection_groups,
        'connections_groups_devices': TDG.generate_data_for_connection_groups_devices
    }

    def __init__(self, *args, **kwargs):
        self.config = conf_test
        super(TestCase, self).__init__(*args, **kwargs)


    def create_app(self):
        application = create_app(self.config)
        self.app = application.test_client()
        return application

    def setUp(self):
        self.createDBIfMissing()
        self.clearDatabase()
        self.loadTestData()

    def tearDown(self):
        self.clearDatabase()

    def loadTestData(self):
        self.loadMySQLData()
        self.loadRDBData()

    def loadMySQLData(self):
        mysql_config = self.config.VIDYO_DBCDR.copy()
        mysql_config.pop('db', None)
        connection = MySQLdb.connect(**mysql_config)
        cursor = connection.cursor(MySQLdb.cursors.Cursor)
        try:
            for line in open(self.config.TEST_SQL_FILE_PATH, 'r'):
                stripped_line = line.strip()
                if stripped_line == '':
                    continue
                cursor.execute(stripped_line)
                connection.commit()
        except MySQLdb.Error, e:
            print e
        finally:
            cursor.close()

    def clear_mysql_database(self):
        tables_to_truncate = ['ConferenceCall2', 'NetworkElementConfiguration', 'ClientInstallations',
                              'ClientInstallations2', 'Tenant', 'Groups', 'MemberRole', 'RoomType', 'Room', 'Member']
        connection = MySQLdb.connect(**self.config.VIDYO_DBCDR)
        cursor = connection.cursor(MySQLdb.cursors.Cursor)
        try:
            for tableName in tables_to_truncate:
                cursor.execute('TRUNCATE '+tableName)
            connection.commit()
        except MySQLdb.Error, e:
            print e
        finally:
            cursor.close()

    def loadRDBData(self):
        connection = self.get_rdb_connection()
        for table_name, generate_method in self.tables_and_method_for_data_generation.iteritems():
            res = r.db(self.app.config['RDB_CONF']['db']).table(table_name).insert(generate_method()).run(connection)
            if res['inserted'] == 0:
                raise EnvironmentError('No data added to: ' + table_name)

    def get_rdb_connection(self):
        return r.connect(host=self.config.RDB_CONF['host'], port=self.config.RDB_CONF['port'])

    def createDBIfMissing(self):
        connection = self.get_rdb_connection()
        databases = r.db_list().run(connection)
        if self.app.config['RDB_CONF']['db'] not in databases:
            r.db_create(self.app.config['RDB_CONF']['db']).run(connection)
        tables = r.db(self.app.config['RDB_CONF']['db']).table_list().run(connection)
        for table_name, _ in self.tables_and_method_for_data_generation.iteritems():
            if table_name not in tables:
                r.db(self.app.config['RDB_CONF']['db']).table_create(table_name).run(connection)

    def clearDatabase(self):
        connection = self.get_rdb_connection()
        tables = r.db(self.app.config['RDB_CONF']['db']).table_list().run(connection)
        for table_name, _ in self.tables_and_method_for_data_generation.iteritems():
            if table_name in tables:
                r.db(self.app.config['RDB_CONF']['db']).table(table_name).delete().run(connection)

    def get_api_get_response(self, url, from_param='', until_param='', tenant_param=''):
        paramConnector=''

        if from_param != '':
            from_param = 'from='+from_param
        if until_param != '':
            until_param = 'until=' + until_param
        if until_param and from_param:
            paramConnector='&'
        if tenant_param:
            tenant_param = 'tenant=' + tenant_param
            if until_param or from_param:
                tenant_param = '&'+tenant_param

        return self.client.get(url + '?' + from_param + paramConnector + until_param + tenant_param)

    def localize_and_encdode_date(self, date):
        return self.encode_date(self.localize_date(date))

    def localize_date(self, date):
        return pytz.utc.localize(date)

    def encode_date(self, date):
        return urllib.quote(date.isoformat())

    def doCompareDictionariesStructure(self, template_dict, target_dict):
        if not template_dict:
            return
        for attr in template_dict:
            self.assertTrue(attr in target_dict)
            self.doCompareDictionariesStructure(template_dict[attr], target_dict[attr])
