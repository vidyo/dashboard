"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from vidyomonit.app.tests.admin_api.VidyomonitAdminApiTestCase import VidyomonitAdminApiTestCase


class ApiAdminNetworkDevicesTestCase(VidyomonitAdminApiTestCase):

    def test_getting_network_devices(self):
        res = self.client.get('/api/admin/networkDevices', follow_redirects=True)
        self.assertEqual(res._status_code, 200)
        self.assertTrue('hostname' in res.data)
        self.assertTrue('id' in res.data)
        self.assertTrue('ip' in res.data)
        self.assertTrue('type' in res.data)
        self.assertTrue('label' in res.data)

