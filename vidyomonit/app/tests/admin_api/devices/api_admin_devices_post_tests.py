"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from vidyomonit.app.tests.admin_api.VidyomonitAdminApiTestCase import VidyomonitAdminApiTestCase


class ApiAdminPostTestCase(VidyomonitAdminApiTestCase):
    valid_ipv6_address = 'FE80:0000:0000:0000:0202:B3FF:FE1E:8329'
    invalid_ipv6_address = '1200::AB00:1234::2552:7777:1313'
    invalid_ipv4_address = '999.999.5.6'

    def get_post_response(self, data):
        return self.client.post('/api/admin/devices', data=data, follow_redirects=True)


    ### Testing full valid data
    def test_posting_device_valid_data_with_ipv4(self):
        data = self.get_valid_device_data()

        res = self.get_post_response(data)
        self.assertEqual(res._status_code, 201)
        self.assertTrue('Device added' in res.data)

    def test_posting_device_valid_data_with_ipv6(self):
        data = self.get_valid_device_data()
        data.update({'ip_address': self.valid_ipv6_address})

        res = self.get_post_response(data)
        self.assertEqual(res._status_code, 201)
        self.assertTrue('Device added' in res.data)


    ### Testing IP parameter
    def test_posting_device_with_empty_ip(self):
        data = self.get_valid_device_data()
        data.pop('ip', None)

        res = self.get_post_response(data)
        self.assertEqual(res._status_code, 201)
        self.assertTrue('Device added' in res.data)

    def test_posting_device_with_invalid_ipv4_address(self):
        data = dict(
            ip_address=self.invalid_ipv4_address,
            id='NQBJGVTKRGDRMM6EV577NUXUHFNMKM3625Y62CGT8659Z00VR0001',
            label='ValidLabel',
            hostname='validHostname',
            type='router'
        )

        res = self.get_post_response(data)
        self.assertEqual(res._status_code, 200)
        self.assertTrue('invalidFields' in res.data)
        self.assertTrue('ip_address' in res.data)

    def test_posting_device_with_invalid_ipv6_address(self):
        data = dict(
            ip_address=self.invalid_ipv6_address,
            id='NQBJGVTKRGDRMM6EV577NUXUHFNMKM3625Y62CGT8659Z00VR0001',
            label='ValidLabel',
            hostname='validHostname',
            type='router'
        )

        res = self.get_post_response(data)
        self.assertEqual(res._status_code, 200)
        self.assertTrue('invalidFields' in res.data)
        self.assertTrue('ip_address' in res.data)


    ### Testing id parameter
    def test_posting_device_with_valid_data_and_empty_id(self):
        data = self.get_valid_device_data()
        data.update({'id': ''})

        res = self.get_post_response(data)
        self.assertEqual(res._status_code, 200)
        self.assertTrue('invalidFields' in res.data)
        self.assertTrue('id' in res.data)

    def test_posting_device_when_id_already_exist(self):
        data = self.get_valid_device_data()
        data.update({'id': self.existingDeviceId})

        res = self.get_post_response(data)
        self.assertEqual(res._status_code, 200)
        self.assertTrue('Device with this id already exist' in res.data)



    ### Testing hostname parameter
    def test_posting_device_with_valid_data_and_empty_hostname(self):
        data = self.get_valid_device_data()
        data.update({'hostname': ''})
    
        res = self.get_post_response(data)
        self.assertEqual(res._status_code, 201)
        self.assertTrue('Device added' in res.data)


    ### Testing label parameter
    def test_posting_device_with_empty_label(self):
        data = self.get_valid_device_data()
        data.pop('label', None)

        res = self.get_post_response(data)
        self.assertEqual(res._status_code, 200)
        self.assertTrue('invalidFields' in res.data)
        self.assertTrue('label' in res.data)


    ### Testing type parameter
    def test_posting_device_with_empty_type(self):
        data = self.get_valid_device_data()
        data.pop('type', None)

        res = self.get_post_response(data)
        self.assertEqual(res._status_code, 200)
        self.assertTrue('invalidFields' in res.data)
        self.assertTrue('type' in res.data)

    def test_posting_device_with_invalid_type(self):
        data = self.get_valid_device_data()
        data.update({'type': 'invalidType'})

        res = self.get_post_response(data)
        self.assertEqual(res._status_code, 200)
        self.assertTrue('invalidFields' in res.data)
        self.assertTrue('type' in res.data)


##test when device is exsisting