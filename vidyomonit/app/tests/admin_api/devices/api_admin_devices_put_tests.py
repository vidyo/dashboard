"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from vidyomonit.app.tests.admin_api.VidyomonitAdminApiTestCase import VidyomonitAdminApiTestCase


class ApiAdminPutTestCase(VidyomonitAdminApiTestCase):

    def get_put_response(self, data):
        return self.client.put('/api/admin/devices', data=data, follow_redirects=True)

    def test_updating_device_using_existing_id(self):
        data = self.get_valid_device_data()
        data.update({
            'id': self.existingDeviceId,
            'type': self.deviceTypePortal,
            'label': self.validLabel
        })

        res = self.get_put_response(data)
        device_to_compare = self.get_device_from_db_by_id(self.existingDeviceId)[0]

        self.assertEqual(res._status_code, 204)
        self.assertEqual('', res.data)
        self.assertEqual(device_to_compare['label'], data['label'])
        self.assertEqual(device_to_compare['type'], data['type'])

    def test_updating_device_using_not_existing_id(self):
        data = self.get_valid_device_data()
        data.update({'id': 'someInvalidId'})

        res = self.get_put_response(data)

        self.assertEqual(res._status_code, 200)
        self.assertTrue('Device with id someInvalidId does not exist' in res.data)

    def test_updating_device_using_existing_and_empty_ip(self):
        data = self.get_valid_device_data()
        data.update({
            'id': self.existingDeviceId,
            'ip_address': ''
        })

        res = self.get_put_response(data)
        device_to_compare = self.get_device_from_db_by_id(self.existingDeviceId)[0]

        self.assertEqual(res._status_code, 204)
        self.assertEqual(device_to_compare['ip_address'], self.existingDeviceIp)

    def test_updating_device_using_existing_and_invalid_ipv4(self):
        data = self.get_valid_device_data()
        data.update({
            'id': self.existingDeviceId,
            'ip_address': '999.999.99.99'
        })

        res = self.get_put_response(data)
        device_to_compare = self.get_device_from_db_by_id(self.existingDeviceId)[0]

        self.assertEqual(res._status_code, 204)
        self.assertEqual(device_to_compare['ip_address'], self.existingDeviceIp)

    def test_updating_device_using_existing_id_and_hostname_parameter(self):
        data = self.get_valid_device_data()
        data.update({
            'id': self.existingDeviceId,
            'hostname': 'newHostname'
        })

        res = self.get_put_response(data)
        device_to_compare = self.get_device_from_db_by_id(self.existingDeviceId)[0]

        self.assertEqual(res._status_code, 204)
        self.assertEqual(device_to_compare['hostname'], self.existingDeviceHostname)

    def test_updating_device_using_empty_label(self):
        data=dict({
            'id': self.existingDeviceId,
            'label': ''
        })

        res = self.get_put_response(data)

        self.assertEqual(res._status_code, 200)
        self.assertTrue('invalidFields' in res.data)
        self.assertTrue('label' in res.data)

    def test_updating_device_using_without_label_and_typeparameter(self):
        data = dict({
            'id': self.existingDeviceId
        })

        res = self.get_put_response(data)

        self.assertEqual(res._status_code, 200)
        self.assertTrue('invalidFields' in res.data)
        self.assertTrue('label' in res.data)
        self.assertTrue('type' in res.data)

    def test_updating_device_using_empty_type(self):
        data = dict({
            'type': ''
        })

        res = self.get_put_response(data)

        self.assertEqual(res._status_code, 200)
        self.assertTrue('invalidFields' in res.data)
        self.assertTrue('type' in res.data)

    def test_updatimg_device_using_invalid_type_parameter(self):
        data = dict({
            'type': 'invalid_type'
        })

        res = self.get_put_response(data)

        self.assertEqual(res._status_code, 200)
        self.assertTrue('invalidFields' in res.data)
        self.assertTrue('type' in res.data)