"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from vidyomonit.app.tests.admin_api.VidyomonitAdminApiTestCase import VidyomonitAdminApiTestCase
from vidyomonit.app.tests.tools.test_data_generator import TestDataGenerator as TDG


class ApiAdminDeleteTestCase(VidyomonitAdminApiTestCase):

    def get_delete_response(self, data={}):
        return self.client.delete('/api/admin/devices', data=data, headers={'Content-Type': 'application/x-www-form-urlencoded'})

    def test_deleting_device_using_valid_id(self):
        data = dict({
            'id': TDG.ROUTER_1_ID,
        })

        res = self.get_delete_response(data)

        self.assertEqual(res._status_code, 200)
        self.assertTrue('Device with id %s was removed'.format(TDG.ROUTER_1_ID) in res.data)

    def test_deleting_device_using_invalid_id(self):
        data = dict({
            'id': 'invalidId'
        })

        res = self.get_delete_response(data)

        self.assertEqual(res._status_code, 200)
        self.assertTrue('Failed to remove' in res.data)

    def test_deleting_device_without_id(self):
        data = dict({})

        res = self.get_delete_response(data)

        self.assertEqual(res._status_code, 200)
        self.assertTrue('Invalid parameters' in res.data)

