"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

from vidyomonit.app.tests.admin_api.VidyomonitAdminApiTestCase import VidyomonitAdminApiTestCase
from vidyomonit.app.tests.tools.test_data_generator import TestDataGenerator as TDG
import json


class ApiAdminGetTestCase(VidyomonitAdminApiTestCase):

    response_template = {
        'hostname': None,
        'id': None,
        'ip_address': None,
        'label': None,
        'type': None,
    }

    def test_getting_device_using_valid_id(self):
        res = self.client.get('/api/admin/devices', follow_redirects=True)
        self.assertEqual(res._status_code, 200)
        jsoned_res = json.loads(res.data)['data']
        for device_response in jsoned_res:
            self.validateResponseStructure(device_response)
        self.assertTrue(TDG.ROUTER_1_ID in res.data)

    def validateResponseStructure(self, response):
        self.doCompareDictionariesStructure(self.response_template, response)