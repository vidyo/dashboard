"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
Background tasks to fetch data from the Vidyo CDR.
Note: all time ranges are [inclusive, exclusive] if not otherwise mentioned.
"""

from collections import defaultdict
from datetime import timedelta

from vidyomonit.app.utils import utc_from_timestamp, utc, utc_now, timestamp
from vidyomonit.app.tools.mysql_manager import MySQL_Manager


def fetch_snapshot_data(configuration=None):
    if not configuration:
        import conf
        configuration = conf
    query = """SELECT TenantName, ConferenceType, EndpointType, RouterID, GWID
        FROM ConferenceCall2
        WHERE CallState='IN PROGRESS'
        AND JoinTime > date_sub(now(), INTERVAL 2 DAY );
    """
    rows, _ = MySQL_Manager(configuration.VIDYO_DBCDR).execute(query, fetch_all=True)

    keys = ('ConferenceType', 'EndpointType', 'RouterID', 'GWID')
    tenants = defaultdict(lambda: dict((k, defaultdict(int)) for k in keys))

    for row in rows:
        tenant = row['TenantName']
        if not tenant:
            tenant = 'total'
        row_tenants = (tenant, 'total')
        for tenant_name in row_tenants:
            for k in [k for k in keys if row[k]]:
                tenants[tenant_name][k][row[k]] += 1

            if not 'total' in tenants[tenant_name]:
                tenants[tenant_name]['total'] = 0

            tenants[tenant_name]['total'] += 1

    return tenants


def fetch_active_conns_data(configuration=None):
    if not configuration:
        import conf
        configuration = conf

    process_conference_name = getattr(configuration, 'PROCESS_CONFERENCE_NAME', None)
    if not process_conference_name:
        raise Exception('conf.PROCESS_CONFERENCE_NAME function is undefined!')

    query = """SELECT TenantName, CallID, UniqueCallID, ConferenceName, EndpointType, UNIX_TIMESTAMP(JoinTime) as JoinTime, RouterID, GWID, CallerID, CallerName
        FROM ConferenceCall2
        WHERE CallState='IN PROGRESS'
        AND JoinTime > date_sub(now(), INTERVAL 2 DAY );
    """
    rows, _ = MySQL_Manager(configuration.VIDYO_DBCDR).execute(query, fetch_all=True)

    tenants = defaultdict(list)

    for row in rows:
        row['JoinTime'] = utc_from_timestamp(row['JoinTime'])
        if row['ConferenceName']:
            row['ConferenceName'] = process_conference_name(row['ConferenceName'])

        if row['TenantName']:
            tenants[row['TenantName']].append(row)

        tenants['total'].append(row)

    return tenants


def fetch_conns_in_range(start_dt, end_dt=None, configuration=None):
    """
    Fetch connections between an inclusive start and an exclusive end date.

    Note! JoinTime of returned connections is always equal or greater than start_dt.
    If start_dt is 20/1/2014, connections that start the day before but end sometime in
    20/1/2014 are also included: however, connection's JoinTime is changed to 20/1/2014.
    """

    if not configuration:
        import conf
        configuration = conf

    end_dt = end_dt or start_dt + timedelta(days=1)
    start_ts, end_ts = [timestamp(dt) for dt in [start_dt, end_dt]]
    db = MySQL_Manager(configuration.VIDYO_DBCDR)

    query = """SELECT TenantName, CallID, UniqueCallID, ConferenceName, ConferenceType, EndpointType, Direction, UNIX_TIMESTAMP(JoinTime) as JoinTime, UNIX_TIMESTAMP(LeaveTime) as LeaveTime, RouterID, GWID, CallerID, CallerName, CallState, ApplicationVersion, ApplicationOs
        FROM ConferenceCall2
        WHERE (
            (CallState='COMPLETED' AND UNIX_TIMESTAMP(LeaveTime) > %s) OR
            (CallState='IN PROGRESS')
        )
        AND UNIX_TIMESTAMP(JoinTime) >= %s
        AND UNIX_TIMESTAMP(JoinTime) < %s
        AND TenantName IS NOT NULL
        ORDER BY JoinTime ASC;
    """
    params = (start_ts, start_ts - 1*24*3600, end_ts)

    for row in db.iterate_results(query, params):
        row['JoinTime'] = max(utc_from_timestamp(row['JoinTime']), start_dt)
        row['LeaveTime'] = utc_from_timestamp(row['LeaveTime']) if row['LeaveTime'] else utc_now()
        yield row


def fetch_installs(configuration=None):
    """
    Fetch client installations by date. Installations are grouped by tenant,
    these values are in turn grouped by users/devices/guests/total, which are in
    turn grouped by total/today.

    E.g. _fetch_installs()[date][tenant]['guests']['today'] returns the daily
    guests of the given tenant in the given date.
    """

    if not configuration:
        import conf
        configuration = conf

    query = """SELECT dt, tenantName, userName, displayName FROM (
        SELECT UNIX_TIMESTAMP(timeInstalled) as dt, tenantName, userName, userName AS displayName
        FROM ClientInstallations
        UNION ALL
        SELECT UNIX_TIMESTAMP(timeInstalled) as dt, tenantName, userName, displayName
        FROM ClientInstallations2
    ) AS s
    WHERE userName != '' AND userName IS NOT NULL
    ORDER BY dt ASC
    """
    # TODO separate into aggretate func!
    rows, _ = MySQL_Manager(configuration.VIDYO_DBCDR).execute(query, fetch_all=True)
    return rows


def fetch_registrations(configuration=None):
    """
    Fetch all member registrations in chronological order.
    """
    if not configuration:
        import conf
        configuration = conf
    db = MySQL_Manager(configuration.VIDYO_DBCDR)

    query = """SELECT m.memberName, m.memberCreated, mr.roleName, t.tenantName, g.groupName
        FROM
            Member m
                INNER JOIN
            MemberRole mr ON mr.roleID = m.roleID
                INNER JOIN
            Room r ON r.memberID = m.memberID
                INNER JOIN
            Tenant t ON t.tenantID = m.tenantID
                INNER JOIN
            Groups g ON g.groupID = r.groupID
                INNER JOIN
            RoomType rt ON rt.roomTypeID = r.roomTypeID
        WHERE
            m.memberCreated IS NOT NULL
                AND rt.roomType IN ('Personal' , 'Legacy')
                AND m.active=1
        ORDER BY m.memberCreated ASC;
    """

    for row in db.iterate_results(query):
        row['memberCreated'] = utc_from_timestamp(row['memberCreated'])
        yield row


def fetch_tenants(configuration=None):
    """
    Fetch all tenant names.
    """
    if not configuration:
        import conf
        configuration = conf

    db = MySQL_Manager(configuration.VIDYO_DBCDR)
    query = 'SELECT tenantID, tenantName FROM Tenant'
    return db.iterate_results(query)
