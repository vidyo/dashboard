"""
This file is part of the CERN Dashboards for Vidyo
Copyright (C) 2014 European Organization for Nuclear Research (CERN)

CERN Dashboards for Vidyo is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
Background tasks to fetch data from the Vidyo CDR.
Note: all time ranges are [inclusive, exclusive[ if not otherwise mentioned.
"""

import socket
from vidyomonit.app.tools.mysql_manager import MySQL_Manager
from flask import current_app as app


def fetch_devices_data():
    """Fetch information about all devices in the database. """
    query = "SELECT DISTINCT Identifier, displayName, componentType, ipAddress FROM NetworkElementConfiguration"
    rows, _ = MySQL_Manager(app.config['VIDYO_DBCDR']).execute(query, fetch_all=True)
    devices = []

    def get_device_type_from_component(component):
        return {
            'VidyoGateway': 'gateway',
            'VidyoRouter': 'router',
            'VidyoProxy': 'portal'
        }.get(component, '')

    for row in rows:
        device = {}
        device['id'] = row['Identifier']
        device['label'] = row['displayName']
        device['ip_address'] = row['ipAddress']
        device['type'] = get_device_type_from_component(row['componentType'])
        try:
            host_info = socket.gethostbyaddr(device['ip_address'])
            if len(host_info) == 3:
                device['hostname'] = host_info[0]
        except socket.herror:
            device['hostname'] = ''

        devices.append(device)

    return devices

