/*!
 * This file is part of the CERN Dashboards for Vidyo
 * Copyright (C) 2014 European Organization for Nuclear Research (CERN)
 *
 * CERN Dashboards for Vidyo is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var React = require('react');
var Application = require('./components/application');

/**
 * Build and start refreshing the dashboard. Config properties include
 * - targetId: container element id,
 * - url: API location, either a string like 'https://vidyoportal.com/api/' or a function
 *        that receives the requested API path (e.g. 'connections/daily') and query parameters
 *        as an object (e.g. {tenant: 'CERN'}), returning the URL string.
 *        The latter option can be used to request monitor API data through a proxy server with
 *        a different routing mechanism.
 * - defaultTenant: the tenant whose data is initially fetched, defaults to 'total'.
 *
 * Returns an object containing an unmount method, that can be used to delete
 * dashboard from the containing element.
 */
var VidyoAdminDashboard = function(config) {
    if (!config) throw new Error('config object undefined');
    if (!config.url) throw new Error('config.url undefined');
    if (!config.targetId) throw new Error('config.targetId undefined');

    var elem = document.getElementById(config.targetId);

    React.render(
        <Application url={config.url} />,
        elem
    );

    return {
        unmount: function() {
            return React.unmountComponentAtNode(document.getElementById(config.targetId));
        }
    };
};

module.exports = VidyoAdminDashboard;
