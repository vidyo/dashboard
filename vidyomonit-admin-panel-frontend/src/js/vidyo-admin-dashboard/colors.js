/*!
 * This file is part of the CERN Dashboards for Vidyo
 * Copyright (C) 2014 European Organization for Nuclear Research (CERN)
 *
 * CERN Dashboards for Vidyo is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var colors = {
    background: '#FAFAFA', 
    chartBackground: '#FFF',
    chartBorder: '#EBEBEB',
    chartHeader: '#4F4F4F',
    headerAccent: '#A95353',
    subHeader: '#A5ADAB',
    chartColors: ['#0B76B6', '#FF8000', '#60BD68', '#F17CB0', '#B2912F', '#B276B2', '#DECF3F', '#F15854', '#307D99'],
};


module.exports = colors;
