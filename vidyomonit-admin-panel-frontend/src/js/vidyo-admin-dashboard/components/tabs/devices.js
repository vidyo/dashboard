/*!
 * This file is part of the CERN Dashboards for Vidyo
 * Copyright (C) 2014 European Organization for Nuclear Research (CERN)
 *
 * CERN Dashboards for Vidyo is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';
/* global google */

var React = require('react');
var _ = require('underscore');

var API = require('../../api');
var Row = require('../utils').Row;
var Col = require('../utils').Col;

var DEVICE_TYPES = ['router', 'gateway', 'portal'];
var DEFAULT_DEVICE_TYPE = DEVICE_TYPES[0];

var Devices = React.createClass({
    propTypes: {
        url: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.func]).isRequired
    },

    getInitialState: function () {
        return {
            api: API.getClient(this.props.url)
        };
    },

    render: function () {
        return (
            <div>
                <Row>
                    <Col sm={12}>
                        <DeviceList api={this.state.api}/>
                    </Col>
                </Row>
            </div>
        );
    }
});


var DeviceList = React.createClass({
    propTypes: {
        api: React.PropTypes.object.isRequired
    },

    getInitialState: function () {
        return {
            devicesList: {},
            successFeedback: {},
            failureFeedback: {},
            fieldsFeedback: {},
            typeaheadDevices: []
        };
    },

    componentWillMount: function () {
        this.props.api.getDevices().done(function (data) {
            this.setState({
                devicesList: data.data
            });
        }.bind(this));

        this.props.api.getAllAvaibleDevices().done(function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                this.setState({
                    typeaheadDevices: data.data
                });
            }
        }.bind(this));
    },

    updateList: function (list) {
        this.setState({
            devicesList: list
        });
    },

    resetFeedback: function() {
        this.setState({
            successFeedback: {},
            failureFeedback: {},
            fieldsFeedback: {}
        });
    },

    setFailureFeedback: function(data) {
        if (!(data['messages'])) {
            this.setState({
                failureFeedback: {
                    message: 'An unknown error occurred. Please try again.'
                }
            });
            return;
        }

        if (data.messages.invalidFields) {
            var invalidFields = {};
            for (var field in data.messages.invalidFields) {
                invalidFields[field] = data.messages.invalidFields[field];
            }
            this.setState({
                fieldsFeedback: invalidFields
            });
        } else if(data.messages.generalError) {
            this.setState({
                failureFeedback: {
                    message: data.messages.generalError
                }
            });
        }
    },

    setSuccessFeedback: function() {
        this.setState({
            successFeedback: {
                message: 'Device added successfully'
            }
        });

        setTimeout(function(){
            $('#addDeviceFormCancel').click();
            this.resetFeedback();
        }.bind(this), 2000);
    },

    addListItem: function (item) {
        this.props.api.addDevice(item)
            .done(function(data, textStatus, jqXHR) {
                this.resetFeedback();

                if (jqXHR.status == 201) {
                    var devicesList = this.state.devicesList;
                    devicesList.push(item);

                    this.updateList(devicesList);
                    this.setSuccessFeedback();
                } else {
                    this.setFailureFeedback(data);
                }
            }.bind(this))
    },

    removeListItem: function (itemId) {
        this.props.api.remove({id:itemId}).done(function(data, textStatus, jqXHR){
            if (jqXHR.status == 200) {
                var devicesList = this.state.devicesList.filter(function(device) {
                    return device.id != itemId;
                });
                this.updateList(devicesList);
            } else {

            }
        }.bind(this));
    },

    removeAllListItems: function () {
        this.state.devicesList.forEach(function(device) {
            this.removeListItem(device.id)
        }.bind(this));
    },

    updateListItem: function (item) {
        this.props.api.updateDevice(item)
            .done(function(data, textStatus, jqXHR) {
                if (jqXHR.status == 204) {
                    var devicesList = this.state.devicesList;
                    var index = _.findIndex(devicesList, function(obj){
                        return obj.id === item.id
                    });

                    devicesList[index].label = item.label;
                    devicesList[index].type = item.type;

                    this.setState({
                        devicesListInvalidFields: {}
                    });
                    this.updateList(devicesList);
                } else {
                    if (data.messages.invalidFields) {
                        var invalidFields = {};
                        for (var field in data.messages.invalidFields) {
                            invalidFields[field] = data.messages.invalidFields[field];
                        }
                        this.setState({
                            devicesListInvalidFields: invalidFields
                        });
                    } else
                    if(data.messages.generalError) {
                        this.setState({
                            generalError: ''+data.messages.generalError,
                            invalidFields: {}
                        });
                    }
                }
            }.bind(this))
    },

    removeSelectedItems: function(IDsList) {
        IDsList.forEach(function(deviceId) {
            this.removeListItem(deviceId)
        }.bind(this));
    },

    render: function () {
        var items = this.state.devicesList;

        return (
            <div>
                <FeedbackBox errorMessage={this.state.failureFeedback} successMessage={this.state.successFeedback}/>
                <AddDeviceForm handleAddListItem={this.addListItem} fieldsFeedback={this.state.fieldsFeedback} typeaheadDevices={this.state.typeaheadDevices}/>

                <div>
                    <List items={items}
                        removeListItem={this.removeListItem}
                        removeAllListItems={this.removeAllListItems}
                        updateListItem={this.updateListItem}
                        listInvalidFields={this.state.devicesListInvalidFields}
                        removeSelectedItems={this.removeSelectedItems}
                    />
                </div>
            </div>
        );
    }
});

var FeedbackBox = React.createClass({

    getInitialState: function() {
        return {
            className: '',
            display: {
                display: 'none'
            },
            message: ''
        }
    },

    componentWillReceiveProps: function(nextProps) {
        if (nextProps.errorMessage.message) {
            this.setState({
                className: 'alert alert-danger',
                display: {
                    display: 'block'
                },
                message: nextProps.errorMessage.message
            });
        } else if (nextProps.successMessage.message) {
            this.setState({
                className: 'alert alert-success',
                display: {
                    display: 'block'
                },
                message: nextProps.successMessage.message
            });
        } else {
            this.setState(this.getInitialState());
        }
    },

    render: function() {
        return (
            <div className={this.state.className} role="alert" display={this.state.display}>
                {this.state.message}
            </div>
        )
    }
});

var AddDeviceForm = React.createClass({
    propTypes: {
        handleAddListItem: React.PropTypes.func,
        fieldsFeedback: React.PropTypes.object,
        typeaheadDevices: React.PropTypes.array
    },

    getInitialState: function() {
        return ({
            ip_address: '',
            id: '',
            label: '',
            hostname: '',
            type: 'router'
        });
    },

    handleCloseResetForm: function() {
        this.collapseForm();
        this.resetForm();
    },

    collapseForm: function() {
        $('#addDeviceFormCollapsible').collapse('hide');
    },

    resetForm: function() {
        document.getElementById("addDeviceForm").reset();
        this.setState(this.getInitialState());
    },

    componentWillReceiveProps: function(nextProps) {
        var typeaheadFields = ['id', 'ip_address', 'hostname'];
        if (nextProps['typeaheadDevices'].length != 0) {
            typeaheadFields.forEach(function(field) {
                $('#'+field).typeahead({
                        hint: true,
                        highlight: true,
                        minLength: 2
                    },
                    {
                        name: 'devices',
                        displayKey: 'identifier',
                        source: this.fieldMatcher(nextProps.typeaheadDevices, field)
                    });

                $('#'+field).bind('typeahead:selected', function(obj, datum, name) {
                    this.setState({
                        ip_address: datum.ip_address,
                        id: datum.id,
                        label: datum.label,
                        type: (datum.type != '') ? datum.type : DEFAULT_DEVICE_TYPE,
                        hostname: datum.hostname,
                    });
                }.bind(this));

                //fix for bug: inputs values disappear  on blur events
                $('#'+field).bind('blur', function(event){
                    this.forceUpdate();
                }.bind(this));
            }.bind(this));
        }
    },

    componentDidMount: function() {
        $('#addDeviceFormCollapsible').on('hidden.bs.collapse', function () {
            this.resetForm();
        }.bind(this));

    },


    fieldMatcher: function(devices, objField) {
        return function findMatches(q, cb) {
            var matches, substrRegex;

            matches = [];
            substrRegex = new RegExp(q, 'i');

            $.each(devices, function(i, obj) {
                if (substrRegex.test(obj[objField])) {
                    var identifier = null;
                    if (objField == 'ip_address') {
                        identifier = obj.ip_address+' (' + obj.type + ')';
                    } else {
                        identifier = obj[objField];
                    }
                    matches.push({
                        id: obj.id,
                        ip_address: obj.ip_address,
                        type: obj.type,
                        label: obj.label,
                        hostname: obj.hostname,
                        identifier: identifier
                    });
                }
            });
            cb(matches);
        };
    },

    handleFormSubmit: function(e) {
        var device = this.state;

        e.preventDefault();
        this.props.handleAddListItem(device);
    },

    handleInputChange: function(fieldName, fieldValue) {
        var fieldObj = {};
        fieldObj[fieldName] = fieldValue;
        this.setState(fieldObj);
    },

    getInputsFeedback: function() {
        return {
            ip_address: this.props.fieldsFeedback.ip_address,
            id: this.props.fieldsFeedback.id,
            label: this.props.fieldsFeedback.label,
            hostname: this.props.fieldsFeedback.hostname,
            type: this.props.fieldsFeedback.type
        };
    },



    render: function() {
        var feedback = this.getInputsFeedback();
        return (
            <div>
                <a className="btn btn-primary"
                    data-toggle="collapse"
                    href="#addDeviceFormCollapsible"
                    aria-expanded="false"
                    aria-controls="collapseExample"
                    onClick={this.handleCloseResetForm}
                >
                    Add New Device
                </a>
                <div className="collapse" id="addDeviceFormCollapsible">
                    <div className="addDeviceFormWrapper">
                        <form id="addDeviceForm" className="form-horizontal" onSubmit={this.handleSubmit} >
                            <AddDeviceFormInput name="ip_address" type="text" handleChange={this.handleInputChange} feedback={feedback.ip_address} value={this.state.ip_address}/>
                            <AddDeviceFormInput name="hostname" type="text" handleChange={this.handleInputChange} feedback={feedback.hostname} value={this.state.hostname}/>
                            <AddDeviceFormInput name="id" type="text" handleChange={this.handleInputChange} feedback={feedback.id} value={this.state.id}/>
                            <AddDeviceFormInput name="type" type="select" handleChange={this.handleInputChange} value={this.state.type} feedback={feedback.type}/>
                            <AddDeviceFormInput name="label" type="text" handleChange={this.handleInputChange} feedback={feedback.label} value={this.state.label}/>

                            <button type="submit" className="btn btn-primary" onClick={this.handleFormSubmit}>Save device</button>
                            <button type="button" className="btn btn-warning" id="addDeviceFormCancel" onClick={this.handleCloseResetForm}>Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
});


var AddDeviceFormInput = React.createClass({

    propTypes: {
        name: React.PropTypes.string,
        type: React.PropTypes.string,
        handleChange: React.PropTypes.func,
        value: React.PropTypes.string
    },

    getInitialState: function() {
        return ({
            className: 'form-group',
            feedbackMessage: '',
            displayFeedback: {display: 'none'},
            fieldName: ''
        });
    },

    handleChange: function(event) {
        this.props.handleChange(this.props.name, event.target.value);
    },

    componentWillMount: function() {
        this.setState({
            fieldName: this.props.name
        });
    },

    componentWillReceiveProps: function(nextProps) {
        if (nextProps.feedback) {
            this.setState({
                className: 'form-group has-error has-feedback',
                feedbackMessage: nextProps.feedback,
                displayFeedback: {
                    display: 'block'
                }
            });
        } else {
            this.setState({
                className: 'form-group',
                feedbackMessage: '',
                displayFeedback: {display: 'none'}
            });
        }
    },

    renderInputElement: function() {
        if (this.props.type==='text') {
            return (<AddDeviceFormInputText name={this.props.name} handleChange={this.handleChange} value={this.props.value}/>)
        } else if (this.props.type==='select') {
            return (<AddDeviceFormInputSelect name={this.props.name} handleChange={this.handleChange} value={this.props.value}/>)
        }
    },

    render: function() {
        var inputElement = this.renderInputElement();

        return (
            <div className={this.state.className}>
                <label htmlFor={this.state.fieldName} className="control-label col-xs-2">{this.state.fieldName}:</label>
                <div className="col-xs-10">
                    {inputElement}
                    <small className="help-block" style={this.state.displayFeedback}>{this.state.feedbackMessage}</small>
                </div>

            </div>
        );
    }
});

var AddDeviceFormInputText = React.createClass({
    propTypes: {
        name: React.PropTypes.string,
        handleChange: React.PropTypes.func,
        value: React.PropTypes.string
    },

    render: function() {
        return (
            <input onChange={this.props.handleChange} type="text" className="form-control" id={this.props.name} required value={this.props.value}/>
        )
    }
});

var AddDeviceFormInputSelect = React.createClass({
    propTypes: {
        name: React.PropTypes.string,
        defaultValue: React.PropTypes.string
    },

    render: function() {
        return (
            <select onChange={this.props.handleChange} className="form-control" id={this.props.name} value={this.props.value}>
                <option value="router">router</option>
                <option value="gateway">gateway</option>
                <option value="portal">portal</option>
            </select>
        )
    }
});


var ListHeader = React.createClass({

    propTypes: {
        totalNumberOfListItems: React.PropTypes.number,
        removeAllListItems: React.PropTypes.func,
        removeSelectedItems: React.PropTypes.func
    },

    handleRemoveAll: function (event) {
        event.preventDefault();

        if (confirm('Are you sure you want to remove all devices?')){
            this.props.removeAllListItems();
        }
    },

    handleRemoveSelected: function (event) {
        event.preventDefault();

        if (confirm('Are you sure you want to remove selected devices?')){
            this.props.removeSelectedItems();
        }
    },

    render: function () {
        var totalNumberOfListItems = this.props.totalNumberOfListItems;

        if (totalNumberOfListItems > 0) {
            return (
                <form onSubmit={this.handleSubmit} className="form-inline">
                    {this.props.totalNumberOfListItems} {totalNumberOfListItems === 1 ? 'item' : 'items'}
                    {' '}
                    <button className="btn btn-xs btn-danger" onClick={this.handleRemoveAll} type="submit">Remove all</button>
                    &nbsp;
                    <button className="btn btn-xs btn-danger" onClick={this.handleRemoveSelected} type="submit">Remove selected</button>
                </form>

            );
        }

        return (<span>Devices List</span>);
    }
});

var EmptyList = React.createClass({
    render: function () {
        return (
            <tr>
                <td colSpan="7">There are no devices</td>
            </tr>
        );
    }
});

var List = React.createClass({

    propTypes: {
        items: React.PropTypes.oneOfType([React.PropTypes.object, React.PropTypes.array]),
        removeListItem: React.PropTypes.func,
        removeAllListItems: React.PropTypes.func,
        updateListItem: React.PropTypes.func,
        listInvalidFields: React.PropTypes.object,
        removeSelectedItems: React.PropTypes.func
    },

    getInitialState: function () {
        return {
            editableItemId: null,
            listInvalidFields: {},
            itemsRemoveList: []
        };
    },

    componentWillReceiveProps: function(nextProps) {
        if (_.isEmpty(nextProps.listInvalidFields)) {
            this.setState({
                editableItemId: '',
                listInvalidFields: {}
            });
        } else {
            this.setState({
                listInvalidFields: nextProps.listInvalidFields
            })
        }
    },

    getListOfItemIds: function (items) {
        return Object.keys(items);
    },

    getTotalNumberOfListItems: function (items) {
        return items.length;
    },

    makeListItemEditable: function(itemId) {
        this.setState({
            editableItemId: itemId
        });
    },

    makeListItemUnEditable: function() {
        this.setState({
            editableItemId: '',
            listInvalidFields: {}
        });
    },

    toggleItemRemoveList: function (itemId, isChecked) {
        var itemIndex = this.state.itemsRemoveList.indexOf(itemId);
        if (isChecked && (itemIndex == -1)) {
            this.state.itemsRemoveList.push(itemId);
            this.setState({
                itemsRemoveList: this.state.itemsRemoveList
            });
        } else if ((isChecked == false) && (itemIndex != -1)) {
            this.state.itemsRemoveList.splice(itemIndex,1);
            this.setState({
                itemsRemoveList: this.state.itemsRemoveList
            });
        }
    },

    removeSelectedItems: function() {
        this.props.removeSelectedItems(this.state.itemsRemoveList);
    },

    createListItemElements: function (items) {
        var item;

        return (
            this
                .getListOfItemIds(items)
                .map(function (itemId) {
                    item = items[itemId];
                    var checked = this.state.itemsRemoveList.indexOf(item.id) != -1;
                    if (this.state.editableItemId === item.id) {
                        return (
                            <EditableListItem
                                item={item}
                                handleUpdateListItem={this.props.updateListItem}
                                handleCancelUpdateListItem={this.makeListItemUnEditable}
                                key={item.id}
                                invalidFields={this.state.listInvalidFields}
                                handleClickRemoveCheckbox={this.toggleItemRemoveList}
                                isChecked={checked}
                            />);
                    } else {
                        return (
                            <NonEditableListItem
                                item={item}
                                handleRemoveListItem={this.props.removeListItem}
                                handleMakeItemEditable={this.makeListItemEditable}
                                key={item.id}
                                handleClickRemoveCheckbox={this.toggleItemRemoveList}
                                isChecked={checked}
                            />);
                    }
                }.bind(this))
                .reverse()
        );
    },

    render: function () {
        var items = this.props.items;
        var listItemElements = this.createListItemElements(items);

        return (
            <div>
                <h3 className="page-header">
                    <ListHeader
                        totalNumberOfListItems={this.getTotalNumberOfListItems(items)}
                        removeAllListItems={this.props.removeAllListItems}
                        removeSelectedItems={this.removeSelectedItems}
                    />
                </h3>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Id</th>
                            <th style={{'width': '120px'}}>Type</th>
                            <th style={{'width': '190px'}}>Label</th>
                            <th>Hostaname</th>
                            <th>IP Address</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {listItemElements.length > 0 ? listItemElements : <EmptyList />}
                    </tbody>
                </table>
            </div>
        );
    }
});

var NonEditableListItem = React.createClass({

    propTypes: {
        item: React.PropTypes.object,
        handleRemoveListItem: React.PropTypes.func,
        handleMakeItemEditable: React.PropTypes.func,
        handleClickRemoveCheckbox: React.PropTypes.func,
        isChecked: React.PropTypes.bool
    },

    handleRemove: function (event) {
        event.preventDefault();
        if (confirm('Are you sure you want to delee the decvice?')) {
            this.props.handleRemoveListItem(this.props.item.id);
        }
    },

    handleEdit: function(event) {
        event.preventDefault();
        this.props.handleMakeItemEditable(this.props.item.id);
    },

    handleCheckboxClick: function(event) {
        this.props.handleClickRemoveCheckbox(this.props.item.id, event.target.checked);
    },

    render: function () {
        var item = this.props.item;
        return (
            <tr>
                <td><input type="checkbox" onChange={this.handleCheckboxClick} defaultChecked={this.props.isChecked}/></td>
                <td>{item.id}</td>
                <td>{item.type}</td>
                <td>{item.label}</td>
                <td>{item.hostname}</td>
                <td>{item.ip_address}</td>
                <td>
                    <form className="form-inline" onSubmit={this.handleEdit}>
                        <button type="submit" className="btn btn-success btn-xs">Edit</button>
                    </form>
                </td><td>
                    <form className="form-inline" onSubmit={this.handleRemove}>
                        <button type="submit" className="btn btn-danger btn-xs">Remove</button>
                    </form>
                </td>
            </tr>
        );
    }
});

var EditableListItem = React.createClass({


    propTypes: {
        item: React.PropTypes.object,
        handleUpdateListItem: React.PropTypes.func,
        handleCancelUpdateListItem: React.PropTypes.func,
        invalidFields: React.PropTypes.object
    },

    getInitialState: function() {
        return ({
            label: '',
            type: '',
            invalidFields: {}
        });
    },

    componentDidMount: function() {
        this.setState({
            label: this.props.item.label,
            type: this.props.item.type
        })
    },

    handleLabelChange: function(event) {
        this.setState({label: event.target.value});
    },

    handleTypeChange: function(event) {
        this.setState({type: event.target.value});
    },

    handleUpdate: function(event) {
        event.preventDefault();
        this.setState({
            invalidFields: {}
        });
        var item = {
            id: this.props.item.id,
            label: this.state.label,
            type: this.state.type
        };

        this.props.handleUpdateListItem(item);
    },

    handleCancelUpdate: function(event) {
        event.preventDefault();
        this.setState(this.getInitialState());

        this.props.handleCancelUpdateListItem();
    },

    render: function () {
        var item = this.props.item;
        var formFields = ['label', 'type'];
        var fields = {};
        formFields.forEach(function(field) {
            var fieldName = (field === 'type') ? 'device_type' : field;
            fields[fieldName] = {
                className: 'form-group',
                feedback: {
                    display: {
                        display: 'none'
                    },
                    message: ''
                }
            };
            if (this.props.invalidFields[field]) {
                fields[fieldName].className += ' has-error has-feedback';
                fields[fieldName].feedback.display = {display: 'block'};
                fields[fieldName].feedback.message = this.props.invalidFields[field];
            }
        }.bind(this));

        return (
            <tr>
                <td><input type="checkbox" onChange={this.handleCheckboxClick} defaultChecked={this.props.isChecked}/></td>
                <td>{item.id}</td>
                <td>
                        <div className={fields.device_type.className}>
                        <select
                            className="form-control"
                            id="listDeviceTypeEdit"
                            ref="deviceType"
                            value={this.state.type}
                            selected={this.state.type}
                            onChange={this.handleTypeChange}>

                            <option value="router">router</option>
                            <option value="gateway">gateway</option>
                            <option value="portal">portal</option>
                        </select>
                        <small className="help-block" style={fields.device_type.feedback.display}>{fields.device_type.feedback.message}</small>
                    </div>
                </td>
                <td>
                    <div className={fields.label.className}>
                        <input
                            type="text"
                            className="form-control"
                            id="deviceLabelEdit"
                            value={this.state.label}
                            onChange={this.handleLabelChange}
                            ref="label"
                            required/>
                        <small className="help-block" style={fields.label.feedback.display}>{fields.label.feedback.message}</small>
                    </div>
                </td>
                <td>{item.hostname}</td>
                <td>{item.ip_address}</td>
                <td>
                    <form className="form-inline" onSubmit={this.handleUpdate}>
                        <button type="submit" className="btn btn-success btn-xs">Save</button>
                    </form>
                </td>
                <td>
                    <form className="form-inline" onSubmit={this.handleCancelUpdate}>
                        <button type="submit" className="btn btn-warning btn-xs">Cancel</button>
                    </form>
                </td>
            </tr>
        );
    }
});

module.exports = Devices;