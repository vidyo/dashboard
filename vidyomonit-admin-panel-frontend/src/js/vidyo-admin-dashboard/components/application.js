/*!
 * This file is part of the CERN Dashboards for Vidyo
 * Copyright (C) 2014 European Organization for Nuclear Research (CERN)
 *
 * CERN Dashboards for Vidyo is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var React = require('react');
var _ = require('underscore');

var API = require('../api');
var colors = require('../colors');
var Row = require('./utils').Row;
var Col = require('./utils').Col;

var Tabs = require('./tabs/tabs');


var Application = React.createClass({
    propTypes: {
        url: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.func]).isRequired
    },

    handleTabChange: function(newTab) {
        this.setState({tabs: this.state.tabs.map(function(d) {
            d.selected = d.name === newTab;
            return d;
        })});
    },


    getInitialState: function() {
        return {
            tabs: [{name: 'Devices', selected: true}]
        };
    },

    render: function() {
        var s = this.state;
        var tabName = _.find(s.tabs, function(d) { return d.selected; }).name;
        var tab;

        if (tabName === 'Devices') {
            tab = <Tabs.Devices url={this.props.url}/>;
        }

        return (
            <div className='vidyo-dashboard-container' style={{background: colors.background}}>
                <div className='container-fluid'>
                    <Row>
                        <Col sm={8} lg={10}>
                            <div className='pull-left'>
                                <TabSelect onTabChange={this.handleTabChange} tabs={this.state.tabs} />
                            </div>
                        </Col>
                    </Row>
                    {tab}
                </div>
                <div style={{clear: 'both', height: 0}}></div>
            </div>
        );
    }
});


var TabSelect = React.createClass({
    propTypes: {
        tabs: React.PropTypes.array.isRequired,
        onTabChange: React.PropTypes.func.isRequired
    },

    handleTabChange: function(e) {
        this.props.onTabChange($(e.target).text());
    },

    render: function() {
        var ulStyle = {
            paddingLeft: 0,
            listStyle: 'none'
        };

        var tabs = this.props.tabs.map(function(d, i) {
            var liStyle = {
                float: 'left',
                padding: i > 0 ? '10px 15px' : '10px 15px 10px 0px'
            };

            var aStyle = {
                color: d.selected ? '#22529E' : '#777',
                cursor: d.selected ? 'text' : 'pointer',
                fontSize: 15,
                fontWeight: 700,
                textDecoration: 'none'
            };
            var clickHandler = d.selected ? null : this.handleTabChange;

            return (
                <li key={d.name} style={liStyle}>
                    <a style={aStyle} onClick={clickHandler}>{d.name}</a>
                </li>
            );
        }.bind(this));

        return (
            <ul style={ulStyle}>{tabs}</ul>
        );
    }
});


module.exports = Application;
