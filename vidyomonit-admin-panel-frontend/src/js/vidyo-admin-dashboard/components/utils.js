/*!
 * This file is part of the CERN Dashboards for Vidyo
 * Copyright (C) 2014 European Organization for Nuclear Research (CERN)
 *
 * CERN Dashboards for Vidyo is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var React = require('react');

var Row = React.createClass({
    render: function() {
        return (
            <div {...this.props} className='row'>
                {this.props.children}
            </div>
        );
    }
});


var Col = React.createClass({
    propTypes: {
        sm: React.PropTypes.number,
        md: React.PropTypes.number,
        lg: React.PropTypes.number,
        smOffset: React.PropTypes.number,
        mdOffset: React.PropTypes.number,
        lgOffset: React.PropTypes.number,
        hiddenXs: React.PropTypes.bool,
    },

    render: function() {
        var p = this.props;
        var classes = [];
        if (p.sm) classes.push('col-sm-' + p.sm);
        if (p.md) classes.push('col-md-' + p.md);
        if (p.lg) classes.push('col-lg-' + p.lg);
        if (p.smOffset) classes.push('col-sm-offset-' + p.smOffset);
        if (p.mdOffset) classes.push('col-md-offset-' + p.mdOffset);
        if (p.lgOffset) classes.push('col-lg-offset-' + p.lgOffset);
        if (p.hiddenXs) classes.push('hidden-xs');
        var className = classes.join(' ');

        return (
            <div className={className}>
                {p.children}
            </div>
        );
    }
});



module.exports = {
    Row: Row,
    Col: Col
};
