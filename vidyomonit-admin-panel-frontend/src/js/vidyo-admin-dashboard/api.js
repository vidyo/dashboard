/*!
 * This file is part of the CERN Dashboards for Vidyo
 * Copyright (C) 2014 European Organization for Nuclear Research (CERN)
 *
 * CERN Dashboards for Vidyo is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var _ = require('underscore');



/**
 * Admin  API client.
 */
var getClient = function(apiLocation) {
    var _getAPIURL = function(path, data) {
        if (_.isString(apiLocation)) {
            return apiLocation + path + (_.isEmpty(data) ? '' : '?' + $.param(data));
        } else if (_.isFunction(apiLocation)) {
            return apiLocation(path, data);
        }
    };

    var _ajax = function(path, data, method) {
        data = data || {};
        method = method || 'GET';

        return $.ajax({
            url: _getAPIURL(path),
            data: data,
            crossDomain: true,
            method: method,
            dataType: 'json'
        });
    };

    return {
        addDevice: function(data) {
            return _ajax('devices', data, 'POST');
        },
        getDevices: function(data) {
            return _ajax('devices', {}, 'GET');
        },
        remove: function(data) {
            return _ajax('devices', data, 'DELETE');
        },
        updateDevice: function(data) {
            return _ajax('devices', data, 'PUT');
        },
        getAllAvaibleDevices: function(data) {
            return _ajax('networkDevices', data, 'GET');
        }
    };
};

module.exports = {
    getClient: getClient
};
