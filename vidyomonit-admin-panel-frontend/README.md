    This file is part of the CERN Dashboards for Vidyo
    Copyright (C) 2014 European Organization for Nuclear Research (CERN)
    
    CERN Dashboards for Vidyo is free software: you can redistribute it and/or
    modify it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.
    
    CERN Dashboards for Vidyo is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU Affero General Public License
    along with the CERN Dashboards for Vidyo software.  If not, see <http://www.gnu.org/licenses/>.

This application is based on Aleksi Pekkala's vidyomonit-dashboard-frontend

# Vidyo admin dashboard

A Javascript front-end for the Vidyomonit Admin API.

## Deploy

You can either build the Javascript bundle yourself (see below), or just use the readily built vidyo-admin-dashboard-bundle.min.js. Also you'll need the vidyo-admin-dashboard-style.css file, which is basically a stripped down version of Bootstrap, encapsulated under a namespace to prevent it from messing with the outer scope. Both files are can be found in the src/ folder.

To make application work properly you also need to add following scripts to the <head> tag:

      <script src="js/jquery.min.js"></script>
      <script src="js/typeahead.jquery.min.js"></script>
      <script src="js/typeahead.bundle.min.js"></script>
      <script src="js/bootstrap.min.js"></script>

    var vidyoAdminDashboard;


    // Config options are explained in src/js/vidyo-admin-dashboard/vidyo-admin-dashboard.js
    var config = {
        targetId: 'vidyo-admin-dashboard-container',
        url: 'address_to_vidyomonit_backend/api/admin'
    };

    vidyoAdminDashboard = new VidyoAdminDashboard(config);

    // vidyoAdminDashboard.unmount(); <-- can be used to delete dashboard from DOM

For a more complete deployment example, see src/index.html.


## Develop

1. Install NPM
2. Navigate to the folder with package.json file
3. Run
> npm install
4. 2. Navigate to the folder with bower.json file
5. Run
> bower install bower.json
Now you can run the following build scripts:

> npm run watch

to watch for changes in files and continuously build the bundle, and

> npm run build

to build a minified bundle ready for deployment.


## TODO
- include bower components to the application bundle during building
